package com.posma.sckola.app.persistence.dao.impl;

import com.posma.sckola.app.persistence.dao.AbstractDao;
import com.posma.sckola.app.persistence.dao.UserDao;
import com.posma.sckola.app.persistence.entity.StatusUser;
import com.posma.sckola.app.persistence.entity.UserEntity;
import org.springframework.stereotype.Repository;

import javax.persistence.Query;
import java.util.List;

/**
 * Created by Francis on 27/03/2017.
 */

@Repository("UserDao")
public class UserDaoImpl extends AbstractDao<UserEntity> implements UserDao {

    public UserDaoImpl(){
        super ();
        setClazz(UserEntity.class);
    }

    /**
     * description
     * @since 27/03/2017
     * @param status
     * @return List<UserEntity>
     * @author PosmaGroup
     * @version 1.0
     */
    public List<UserEntity> findAllByFieldStatus(StatusUser status){
        Query query = this.getEntityManager().createQuery("SELECT m FROM UserEntity as m WHERE m.status = :status");
        query.setParameter("status", status);
        return query.getResultList();
    }

    /**
     *
     * @param fieldName
     * @param id
     * @return
     */
    public List<UserEntity> findAllByField(String fieldName, Object id) {
        return getEntityManager().createQuery("from UserEntity where " + fieldName + "='" + id + "'").getResultList();
    }

    /**
     * buscar un usuario por mail
     * @param mail
     * @return
     */
    public UserEntity findByMail(String mail) {
        List<UserEntity> userEntityList = this.getEntityManager().createQuery("SELECT m FROM UserEntity as m where m.mail ='" + mail + "'", UserEntity.class).getResultList();
        return  userEntityList.size() == 1 ? userEntityList.get(0):null;
    }

    public List<UserEntity> findByMailRepresentative(String mail){
        List<UserEntity> userEntityList = this.getEntityManager().createQuery("SELECT m FROM UserEntity as m where m.mailRepresentative ='" + mail + "'", UserEntity.class).getResultList();
        return  userEntityList;
    }

}
