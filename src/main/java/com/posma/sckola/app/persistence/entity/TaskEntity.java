package com.posma.sckola.app.persistence.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "TASK")
@EntityListeners({TaskEntity.class})
public class TaskEntity implements Serializable {

    @Id
    @Basic(optional = false)
    @Column(name="ID")
    @SequenceGenerator(name = "TASK_SEQ", sequenceName = "S_ID_TASK", initialValue = 1, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TASK_SEQ")
    private Long id;

    @Column(name="NAME", nullable = false)
    private String name;

    @Column(name="COMPLETION_DATE", nullable = false)
    private Date completionDate;

    @Column(name="CREATION_DATE", nullable = false)
    private Date creationDate;

    @Column(name="adjunct", nullable = true)
    private String adjunt;

    @Column(name="weight", nullable = true)
    private Integer weight;

    @Column(name = "DESCRIPTION", nullable=true, length = 1024)
    private String description;

    @ManyToOne
    @JoinColumn(name = "EVALUATION_ID", referencedColumnName = "ID")
    private EvaluationEntity evaluation;

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = "MATTER_COMMUNITY_SECTION_TASK", joinColumns = {
            @JoinColumn(name = "TASK_ID", nullable = false, updatable = false) },
            inverseJoinColumns = { @JoinColumn(name = "MATTER_COMMUNITY_SECTION_ID",
                    nullable = false, updatable = false) })
    private List<MatterCommunitySectionEntity> matterCommunitySectionList = new ArrayList<MatterCommunitySectionEntity>();

    public TaskEntity(){}

    public TaskEntity(String name, Date completionDate, Date creationDate, MatterCommunitySectionEntity matterCommunitySectionEntity, String description){
        this.name = name;
        this.completionDate = completionDate;
        this.creationDate = creationDate;
        this.matterCommunitySectionList.add(matterCommunitySectionEntity);
        this.description = description;
    }

    public Long getId(){ return id;}

    public void setId(Long id){this.id = id;}

    public String getName(){return name;}

    public void setName(String name){this.name = name;}

    public Date getCompletionDate() {
        return completionDate;
    }

    public void setCompletionDate(Date completionDate){this.completionDate = completionDate;}

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public String getAdjunt() {
        return adjunt;
    }

    public void setAdjunt(String adjunt) {
        this.adjunt = adjunt;
    }

    public Integer getWeight() {
        return weight;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }

    public EvaluationEntity getEvaluation() {
        return evaluation;
    }

    public void setEvaluation(EvaluationEntity evaluation) {
        this.evaluation = evaluation;
    }

    public List<MatterCommunitySectionEntity> getMatterCommunitySectionList() {
        return matterCommunitySectionList;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void addMatterCommunitySectionList(MatterCommunitySectionEntity matterCommunitySectionEntity){
        this.matterCommunitySectionList.add(matterCommunitySectionEntity);
    }

    public void setMatterCommunitySectionList(List<MatterCommunitySectionEntity> matterCommunitySectionList) {
        this.matterCommunitySectionList = matterCommunitySectionList;
    }
}
