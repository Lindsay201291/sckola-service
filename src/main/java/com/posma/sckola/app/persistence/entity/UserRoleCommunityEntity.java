package com.posma.sckola.app.persistence.entity;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Francis Ries on 18/04/2017.
 */
@Entity
@Table(name = "USER_ROLE_COMMUNITY",
        indexes = {
                @Index(name = "IDX_USER_ROLE_COMMUNITY",
                        columnList = "USER_ID,ROLE_ID,COMMUNITY_ID"
                )
        })

public class UserRoleCommunityEntity implements Serializable {

    @Id
    @Basic(optional = false)
    @Column(name="ID")
    @SequenceGenerator(name="USER_ROLE_COMMUNITY_SEQ", sequenceName="S_ID_USERROLECOMMUNITY", initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "USER_ROLE_COMMUNITY_SEQ")
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "USER_ID", nullable = false)
    private UserEntity user;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ROLE_ID", nullable = false)
    private RoleEntity role;

    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.REMOVE)
    @JoinColumn(name = "COMMUNITY_USER_ID", nullable = false)
    private CommunityUserEntity communityUser;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "COMMUNITY_ID", nullable = false)
    private CommunityEntity community;

    @Column(name = "STATUS", nullable = false)
    private Status status = Status.ACTIVE;

    public UserRoleCommunityEntity(){}

    public UserRoleCommunityEntity(UserEntity user, RoleEntity role, CommunityEntity community, CommunityUserEntity communityUser){
        this.user = user;
        this.role = role;
        this.community = community;
        this.communityUser = communityUser;
    }

    public Long getId(){return id;}

    public void setId(Long id){this.id = id;}

    public UserEntity getUser(){return user;}

    public void setUser(UserEntity user){this.user = user;}

    public RoleEntity getRole(){return role;}

    public void setRole(RoleEntity role){this.role = role;}

    public CommunityUserEntity getCommunityUser(){return communityUser;}

    public void setCommunityUser(CommunityUserEntity communityUser){this.communityUser = communityUser;}

    public CommunityEntity getCommunity(){return community;}

    public void setCommunity(CommunityEntity community){this.community = community;}

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

}
