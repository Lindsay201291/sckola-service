package com.posma.sckola.app.persistence.dao.impl;

import com.posma.sckola.app.persistence.dao.AbstractDao;
import com.posma.sckola.app.persistence.dao.TaskCommentDao;
import com.posma.sckola.app.persistence.entity.TaskCommentEntity;
import org.springframework.stereotype.Repository;

import javax.persistence.Query;
import java.util.List;

@Repository("TaskCommentDao")
public class TaskCommentDaoImpl extends AbstractDao<TaskCommentEntity> implements TaskCommentDao{

    public TaskCommentDaoImpl(){
        super();
        setClazz(TaskCommentEntity.class);
    }

    public List<TaskCommentEntity> findAllByTask(Long taskId){
        Query query = this.getEntityManager().createQuery("SELECT m FROM TaskCommentEntity as m WHERE m.task.id = :taskId");
        query.setParameter("taskId", taskId);
        return query.getResultList();
    }
}
