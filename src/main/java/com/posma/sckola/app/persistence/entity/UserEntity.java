package com.posma.sckola.app.persistence.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Blob;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Francis Ries on 27/03/2017.
 */
@Entity
@Table(name = "SYSTEM_USER",
        indexes = {
                @Index(columnList ="MAIL",name = "IDX_MAIL_SYSTEM_USER")
        })
public class UserEntity implements Serializable {

    @Id
    @Basic(optional = false)
    @Column(name="ID")
    @SequenceGenerator(name = "SYSTEM_USER_SEQ", sequenceName = "S_ID_SYSTEM_USER", initialValue = 1, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SYSTEM_USER_SEQ")
    private Long id;

    @Column(name = "IDENTIFICATION", nullable=true, length = 20)
    private String identification;

    @Column(name = "PASSWORD", nullable=true)
    private String password;

    @Column(name = "FIRST_NAME", nullable=true, length = 80)
    private String firstName;

    @Column(name = "SECOND_NAME", nullable=true, length = 80)
    private String secondName;

    @Column(name = "LAST_NAME", nullable=true, length = 80)
    private String lastName;

    @Column(name = "SECOND_LAST_NAME", nullable=true, length = 80)
    private String secondLastName;

    @Column(name = "MAIL", nullable=true, unique = true, length = 255)
    private String mail;

    @Column(name = "MAIL_representative", nullable = true, length = 255)
    private String mailRepresentative;

    @Column(name = "BIRTHDATE", nullable=true)
    @Temporal(TemporalType.DATE)
    private Date birthdate;

    @Column(name = "GENDER", nullable=true)
    // Masculino 0 (true) femenino 1 (false)
    private Gender gender;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name="USER_ID", referencedColumnName="ID")
    private List<CurriculumEntity> curriculumList;

    @Column(name = "STATUS", nullable = false)
    @Enumerated(EnumType.STRING)
    private StatusUser status;

    //@OneToMany(mappedBy="user", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    //private List<NetworksEntity> networks = new ArrayList<NetworksEntity>();;

    @OneToMany(mappedBy="user", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<UserRoleCommunityEntity> userRoleCommunityList = new ArrayList<UserRoleCommunityEntity>();

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = "USER_ROLE", joinColumns = {
            @JoinColumn(name = "USER_ID", nullable = false, updatable = false) },
            inverseJoinColumns = { @JoinColumn(name = "ROLE_ID",
                    nullable = false, updatable = false) })
    private List<RoleEntity> roleList = new ArrayList<RoleEntity>();

    @Column(name = "FOTO", nullable=true)
    private String foto;


    @OneToMany(mappedBy="user", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<NotificationEntity> notification;


    //@OneToMany(mappedBy="teacher", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    //private List<MatterEntity> matterList;


    public UserEntity() {

    }

    public UserEntity(String firstName, String secondName, String lastName, String secondLastName, Gender gender, StatusUser status) {
        this.firstName = firstName;
        this.secondName = secondName;
        this.lastName = lastName;
        this.secondLastName = secondLastName;
        this.gender = gender;
        this.status = status;
    }

    public UserEntity(String mail, String password, StatusUser status) {
        this.mail = mail;
        this.password = password;
        this.status = status;
    }

    public  UserEntity(String firstName, String secondName, String lastName, String secondLastName, Gender gender, String mail, String password, StatusUser status){
        this.firstName = firstName;
        this.secondName = secondName;
        this.lastName = lastName;
        this.secondLastName = secondLastName;
        this.gender = gender;
        this.mail = mail;
        this.password = password;
        this.status = status;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIdentification() {
        return identification;
    }

    public void setIdentification(String identification) {
        this.identification = identification;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getSecondLastName() {
        return secondLastName;
    }

    public void setSecondLastName(String secondLastName) {
        this.secondLastName = secondLastName;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public StatusUser getStatus() {
        return status;
    }

    public void setStatus(StatusUser status) {
        this.status = status;
    }

    public Date getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(Date birthdate) {
        this.birthdate = birthdate;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public List<RoleEntity> getRoleList() {
        return roleList;
    }

    public void setRoleList(List<RoleEntity> roleList) {
        this.roleList = roleList;
    }

    public boolean addRoleList(RoleEntity user) {
        return roleList.add(user);
    }

    public List<UserRoleCommunityEntity> getUserRoleCommunityList() {
        return userRoleCommunityList;
    }

    public void setUserRoleCommunityList(List<UserRoleCommunityEntity> userRoleCommunityList) {
        this.userRoleCommunityList = userRoleCommunityList;
    }

    public boolean addUserRoleCommunityList(UserRoleCommunityEntity userRoleCommunity) {
        return userRoleCommunityList.add(userRoleCommunity);
    }

    public String getMailRepresentative(){return mailRepresentative;}

    public void setMailRepresentative(String mailRepresentative){this.mailRepresentative = mailRepresentative;}

    /*

    public List<MatterEntity> getMatterList() {
        return matterList;
    }

    public void setMatterList(List<MatterEntity> matterList) {
        this.matterList = matterList;
    }

    public boolean addMatterList(MatterEntity matter) {
        return matterList.add(matter);
    }
*/
    public List<CurriculumEntity> getCurriculumList() {
        return curriculumList;
    }

    public void setCurriculumList(List<CurriculumEntity> curriculumList) {
        this.curriculumList = curriculumList;
    }

    public boolean addCurriculumList(CurriculumEntity curriculum) {
        return curriculumList.add(curriculum);
    }

    public String getFoto(){ return foto;}

    public void setFoto(String foto){ this.foto = foto;}

    //public List<NetworksEntity> getNetwork(){ return networks;}
//
    //public void setNetwork(List<NetworksEntity> networks){ this.networks = networks;}
//
    //public boolean addNetworksList(NetworksEntity networksEntity){
    //    return this.networks.add(networksEntity);
    //}


}
