package com.posma.sckola.app.persistence.dao;

import com.posma.sckola.app.persistence.entity.EvaluationToolTemplateEntity;

import java.util.List;

/**
 * Created by Francis Ries on 27/04/2017.
 */
public interface EvaluationToolTemplateDao {

    /**
     * description
     * @since 27/04/2017
     * @param id evaluation identifier
     * @return EvaluationToolTemplateEntity
     * @author PosmaGroup
     * @version 1.0
     */
    EvaluationToolTemplateEntity findOne(long id);

    /**
     * description
     * @since 27/04/2017
     * @return List<EvaluationToolTemplateEntity>
     * @author PosmaGroup
     * @version 1.0
     */
    List<EvaluationToolTemplateEntity> findAll();

    /**
     * description
     * @since 27/04/2017
     * @param evaluationToolTemplate
     * @return EvaluationToolTemplateEntity
     * @author PosmaGroup
     * @version 1.0
     */
    EvaluationToolTemplateEntity create(EvaluationToolTemplateEntity evaluationToolTemplate);

    /**
     * description
     * @since 27/04/2017
     * @param evaluationToolTemplate
     * @return EvaluationToolTemplateEntity
     * @author PosmaGroup
     * @version 1.0
     */
    EvaluationToolTemplateEntity update(EvaluationToolTemplateEntity evaluationToolTemplate);

    /**
     * description
     * @since 27/04/2017
     * @param evaluationToolTemplate
     * @author PosmaGroup
     * @version 1.0
     */
    void delete(EvaluationToolTemplateEntity evaluationToolTemplate);

    /**
     * description
     * @since 27/04/2017
     * @param id evaluation identifier
     * @author PosmaGroup
     * @version 1.0
     */
    void deleteById(long id);
}
