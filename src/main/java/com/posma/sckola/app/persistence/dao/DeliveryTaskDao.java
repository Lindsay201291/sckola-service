package com.posma.sckola.app.persistence.dao;


import com.posma.sckola.app.persistence.entity.DeliveryTaskEntity;

import java.util.List;

public interface DeliveryTaskDao {

    DeliveryTaskEntity findOne(long id);

    List<DeliveryTaskEntity> findAll();

    List<DeliveryTaskEntity> findAllByFieldId(String fieldName, long id);

    DeliveryTaskEntity create(DeliveryTaskEntity deliveryTaskEntity);

    DeliveryTaskEntity update(DeliveryTaskEntity deliveryTaskEntity);

    void delete(DeliveryTaskEntity deliveryTaskEntity);

    void deleteById(long id);

    DeliveryTaskEntity getDeliveryTaskOfTheTaskUser(Long taskId, Long userId);

    List<DeliveryTaskEntity> getAllDeliveryTaskForTask(Long taskId);

    DeliveryTaskEntity getDeliveryTaskOfTheEvaluationUser(Long evaluationId, Long userId);
}
