package com.posma.sckola.app.persistence.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "DELIVERY_TASK")
@EntityListeners({DeliveryTaskEntity.class})
public class DeliveryTaskEntity implements Serializable {

    @Id
    @Basic(optional = false)
    @Column(name="ID")
    @SequenceGenerator(name = "DELIVERY_TASK_SEQ", sequenceName = "S_ID_DELIVERY_TASK", initialValue = 1, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "DELIVERY_TASK_SEQ")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "TASK_ID", referencedColumnName = "ID")
    private TaskEntity task;

    @ManyToOne
    @JoinColumn(name = "USER_ID", referencedColumnName = "ID")
    private UserEntity user;

    @Column(name="adjunct", nullable = true)
    private String adjunt;

    @Column(name="COMPLETION_DATE", nullable = true)
    private Date completionDate;

    @Column(name="VALUE", nullable = true)
    private String value;

    public DeliveryTaskEntity(){}

    public DeliveryTaskEntity(TaskEntity task, String adjunt, Date completionDate, UserEntity user){
        this.task = task;
        this.adjunt = adjunt;
        this.completionDate = completionDate;
        this.user = user;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TaskEntity getTask() {
        return task;
    }

    public void setTask(TaskEntity task) {
        this.task = task;
    }

    public UserEntity getUser() {
        return user;
    }

    public void setUser(UserEntity user) {
        this.user = user;
    }

    public String getAdjunt() {
        return adjunt;
    }

    public void setAdjunt(String adjunt) {
        this.adjunt = adjunt;
    }

    public Date getCompletionDate() {
        return completionDate;
    }

    public void setCompletionDate(Date completionDate) {
        this.completionDate = completionDate;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
