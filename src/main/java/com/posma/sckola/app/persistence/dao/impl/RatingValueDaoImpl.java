package com.posma.sckola.app.persistence.dao.impl;

import com.posma.sckola.app.persistence.dao.AbstractDao;
import com.posma.sckola.app.persistence.dao.RatingValueDao;
import com.posma.sckola.app.persistence.entity.RatingValueEntity;
import org.springframework.stereotype.Repository;

/**
 * Created by Francis on 12/04/2017.
 */

@Repository("RatingValueDao")
public class RatingValueDaoImpl extends AbstractDao<RatingValueEntity> implements RatingValueDao {

    public RatingValueDaoImpl(){
        super ();
        setClazz(RatingValueEntity.class);
    }


}
