package com.posma.sckola.app.persistence.dao.impl;

import com.posma.sckola.app.persistence.dao.AbstractDao;
import com.posma.sckola.app.persistence.dao.EvaluationPlanTemplateDao;
import com.posma.sckola.app.persistence.entity.EvaluationPlanTemplateEntity;
import org.springframework.stereotype.Repository;

/**
 * Created by Francis on 27/04/2017.
 */

@Repository("EvaluationPlanTemplateDao")
public class EvaluationPlanTemplateDaoImpl extends AbstractDao<EvaluationPlanTemplateEntity> implements EvaluationPlanTemplateDao {

    public EvaluationPlanTemplateDaoImpl(){
        super ();
        setClazz(EvaluationPlanTemplateEntity.class);
    }

}
