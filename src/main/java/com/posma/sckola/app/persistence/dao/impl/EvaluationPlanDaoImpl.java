package com.posma.sckola.app.persistence.dao.impl;

import com.posma.sckola.app.persistence.dao.AbstractDao;
import com.posma.sckola.app.persistence.dao.EvaluationPlanDao;
import com.posma.sckola.app.persistence.entity.EvaluationPlanEntity;
import com.posma.sckola.app.persistence.entity.Status;
import org.springframework.stereotype.Repository;

import javax.persistence.Query;
import java.util.List;

/**
 * Created by Francis on 12/04/2017.
 */

@Repository("EvaluationPlanDao")
public class EvaluationPlanDaoImpl extends AbstractDao<EvaluationPlanEntity> implements EvaluationPlanDao {

    public EvaluationPlanDaoImpl(){
        super ();
        setClazz(EvaluationPlanEntity.class);
    }

    /**
     * description
     * @since 26/04/2017
     * @return List<EvaluationPlanEntity>
     * @author PosmaGroup
     * @version 1.0
     */
    @Override
    public List<EvaluationPlanEntity> findAllByTeacherCommunity(Long communityId, Long userId, Status status){
        Query query = this.getEntityManager().createQuery("SELECT m FROM EvaluationPlanEntity m, MatterCommunitySectionEntity mcs " +
                "WHERE mcs.evaluationPlan = m.id " +
                "AND mcs.matterCommunity.community.id = :communityId " +
                "AND mcs.user.id = :userId " +
                "AND m.status = :status");
        query.setParameter("communityId", communityId);
        query.setParameter("userId", userId);
        query.setParameter("status", status);

        return query.getResultList();
    }

    /**
     * description
     * @since 26/04/2017
     * @return List<EvaluationPlanEntity>
     * @author PosmaGroup
     * @version 1.0
     */
    @Override
    public List<EvaluationPlanEntity> findAllByTeacher(Long userId, Status status){
        Query query = this.getEntityManager().createQuery("SELECT m FROM EvaluationPlanEntity m " +
                "WHERE m.user.id = :userId" +
                " AND m.status = :status");
        query.setParameter("userId", userId);
        query.setParameter("status", status);

        return query.getResultList();
    }

    /**
     * description
     * @since 26/04/2017
     * @return List<EvaluationPlanEntity>
     * @author PosmaGroup
     * @version 1.0
     */
    @Override
    public List<EvaluationPlanEntity> findAllByTeacherNotAssociate(Long userId){
        Query query = this.getEntityManager().createQuery("SELECT m FROM EvaluationPlanEntity m " +
                "WHERE m.user.id = :userId " +
                "AND m.status = :status " +
                "AND m.id NOT IN (SELECT mcs.evaluationPlan FROM MatterCommunitySectionEntity mcs " +
                "WHERE  mcs.user.id = :userId AND mcs.status = :status)");
        query.setParameter("userId", userId);
        query.setParameter("status", Status.ACTIVE);

        return query.getResultList();
    }

    @Override
    public List<EvaluationPlanEntity> findByMatterSectionCommunity(Long p_community, Long p_matter, Long p_section, Long p_userId) {

        Query query = this.getEntityManager().createQuery("SELECT mcs.evaluationPlan FROM MatterCommunitySectionEntity mcs, MatterCommunityEntity mc " +
                "WHERE mcs.section.id = :p_section " +
                "AND mcs.matterCommunity.matter.id = :p_matter " +
                "AND mcs.user.id = :p_userId " +
                "AND mc.community.id = :p_community");
        query.setParameter("p_section", p_section);
        query.setParameter("p_matter", p_matter);
        query.setParameter("p_userId", p_userId);
        query.setParameter("p_community", p_community);

        return query.getResultList();

    }
}
