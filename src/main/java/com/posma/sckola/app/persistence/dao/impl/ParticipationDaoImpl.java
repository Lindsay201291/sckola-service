package com.posma.sckola.app.persistence.dao.impl;

import com.posma.sckola.app.persistence.dao.AbstractDao;
import com.posma.sckola.app.persistence.dao.ParticipationDao;
import com.posma.sckola.app.persistence.entity.ParticipationEntity;
import org.springframework.stereotype.Repository;

@Repository("ParticipationDao")
public class ParticipationDaoImpl extends AbstractDao<ParticipationEntity> implements ParticipationDao{

    public ParticipationDaoImpl(){
        super ();
        setClazz(ParticipationEntity.class);
    }
}
