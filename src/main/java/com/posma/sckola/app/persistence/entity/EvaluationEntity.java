package com.posma.sckola.app.persistence.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Created by Francis Ries on 11/04/2017.
 */
@Entity
@Table(name = "EVALUATION",
        indexes = {
            @Index(columnList ="DATE",name = "IDX_DATE_EVALUATION")
        })
public class EvaluationEntity implements Serializable {

    @Id
    @Basic(optional = false)
    @Column(name="ID")
    @SequenceGenerator(name = "EVALUATION_SEQ", sequenceName = "S_ID_EVALUATION", initialValue = 1, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "EVALUATION_SEQ")
    private Long id;

    @Column(name = "OBJECTIVE", nullable=true, length = 255)
    private String objective;

  //  Se habilitara cuando se carge objetivos a una materia
  //  @Column(name = "CONTENT_TO_EVALUATE_ID", nullable=false)
  //  private Content content;

    @Column(name = "WEIGHT", nullable=true)
    private Integer weight;

    @ManyToOne
    @JoinColumn(name = "EVALUATION_TOOL_ID", referencedColumnName = "ID")
    private EvaluationToolEntity evaluationTool;

    @ManyToOne
    @JoinColumn(name = "RATING_SCALE_ID", referencedColumnName = "ID")
    private RatingScaleEntity ratingScale;

    @Column(name = "DATE", nullable=true)
    @Temporal(TemporalType.DATE)
    private Date date;

    @ManyToOne
    @JoinColumn(name="EVALUATION_PLAN_ID")
    private EvaluationPlanEntity evaluationPlan;

    @Column(name = "STATUS", nullable = false)
    private StatusEvaluation status = StatusEvaluation.PENDING;

    @OneToMany(mappedBy="evaluation", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<QualificationUserEntity> qualificationUserList;

    public EvaluationEntity() {

    }

    public EvaluationEntity(String objective,
                            Integer weight,
                            RatingScaleEntity ratingScale,
                            EvaluationToolEntity evaluationTool,
                            EvaluationPlanEntity evaluationPlan,
                            Date date) {
        this.objective = objective;
        this.weight = weight;
        this.ratingScale = ratingScale;
        this.evaluationTool = evaluationTool;
        this.evaluationPlan = evaluationPlan;
        this.date = date;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getObjective() {
        return objective;
    }

    public void setObjective(String objective) {
        this.objective = objective;
    }

    public Integer getWeight() {
        return weight;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }

    public EvaluationToolEntity getEvaluationTool() {
        return evaluationTool;
    }

    public void setEvaluationTool(EvaluationToolEntity evaluationTool) {
        this.evaluationTool = evaluationTool;
    }

    public RatingScaleEntity getRatingScale() {
        return ratingScale;
    }

    public void setRatingScale(RatingScaleEntity ratingScale) {
        this.ratingScale = ratingScale;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public EvaluationPlanEntity getEvaluationPlan() {
        return evaluationPlan;
    }

    public void setEvaluationPlan(EvaluationPlanEntity evaluationPlan) {
        this.evaluationPlan = evaluationPlan;
    }

    public List<QualificationUserEntity> getQualificationUserList() {
        return qualificationUserList;
    }

    public void setQualificationUserList(List<QualificationUserEntity> qualificationUserList) {
        this.qualificationUserList = qualificationUserList;
    }

    public boolean addQualificationUserList(QualificationUserEntity qualificationUser) {
        return qualificationUserList.add(qualificationUser);
    }

    public StatusEvaluation getStatus(){return status;}

    public void setStatus(StatusEvaluation status){this.status = status;}

}
