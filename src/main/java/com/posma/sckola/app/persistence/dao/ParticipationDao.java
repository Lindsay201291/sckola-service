package com.posma.sckola.app.persistence.dao;


import com.posma.sckola.app.persistence.entity.ParticipationEntity;

import java.util.List;

public interface ParticipationDao {

    ParticipationEntity findOne(long id);

    List<ParticipationEntity> findAll();

    ParticipationEntity create(ParticipationEntity participation);

    ParticipationEntity update(ParticipationEntity participation);

    void delete(ParticipationEntity participation);

    void deleteById(long id);
}
