package com.posma.sckola.app.persistence.dao.impl;

import com.posma.sckola.app.persistence.dao.AbstractDao;
import com.posma.sckola.app.persistence.dao.TaskDao;
import com.posma.sckola.app.persistence.entity.TaskEntity;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository("TaskDao")
public class TaskDaoImpl extends AbstractDao<TaskEntity> implements TaskDao {

    public TaskDaoImpl(){
        super();
        setClazz(TaskEntity.class);
    }

    public List<TaskEntity> getAllTaskForMatterCommunitySection(Long matterCommunitySectionId){
        List<TaskEntity> taskEntityList = this.getEntityManager().createQuery("SELECT m FROM TaskEntity as m JOIN m.matterCommunitySectionList as matter WHERE matter.id = '"+ matterCommunitySectionId +"'", TaskEntity.class).getResultList();
        return taskEntityList.size() >= 1 ? taskEntityList:new ArrayList<TaskEntity>();
    }

    public TaskEntity getTaskForEvaluation(Long evaluationId){
        List<TaskEntity> TaskEntityList = this.getEntityManager().createQuery("SELECT m FROM TaskEntity as m where m.evaluation ='"+evaluationId+"'", TaskEntity.class).getResultList();
        return TaskEntityList.size() == 1 ? TaskEntityList.get(0):null;
    }
}
