package com.posma.sckola.app.persistence.dao;


import com.posma.sckola.app.persistence.entity.TaskCommentEntity;

import java.util.List;

public interface TaskCommentDao {

    TaskCommentEntity findOne(long id);

    List<TaskCommentEntity> findAll();

    List<TaskCommentEntity> findAllByFieldId(String fieldName, long id);

    TaskCommentEntity create(TaskCommentEntity taskCommentEntity);

    TaskCommentEntity update(TaskCommentEntity taskCommentEntity);

    void delete(TaskCommentEntity taskCommentEntity);

    void deleteById(long id);

    List<TaskCommentEntity> findAllByTask(Long taskId);
}
