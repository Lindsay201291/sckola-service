package com.posma.sckola.app.persistence.dao.impl;

import com.posma.sckola.app.persistence.dao.AbstractDao;
import com.posma.sckola.app.persistence.dao.DeliveryTaskDao;
import com.posma.sckola.app.persistence.entity.DeliveryTaskEntity;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository("DeliverytaskDao")
public class DeliverytaskDaoImpl extends AbstractDao<DeliveryTaskEntity> implements DeliveryTaskDao  {

    public DeliverytaskDaoImpl(){
        super ();
        setClazz(DeliveryTaskEntity.class);
    }

    public DeliveryTaskEntity getDeliveryTaskOfTheTaskUser(Long taskId, Long userId){
        List<DeliveryTaskEntity> deliveryTaskEntityList = this.getEntityManager().createQuery("SELECT m FROM DeliveryTaskEntity as m where m.task.id ='"+taskId+"' and m.user.id ='"+userId+"'", DeliveryTaskEntity.class).getResultList();
        return deliveryTaskEntityList.size() == 1 ? deliveryTaskEntityList.get(0):null;
    }

    public List<DeliveryTaskEntity> getAllDeliveryTaskForTask(Long taskId){
        List<DeliveryTaskEntity> deliveryTaskEntityList = this.getEntityManager().createQuery("SELECT m FROM DeliveryTaskEntity as m where m.task.id ='"+taskId+"'", DeliveryTaskEntity.class).getResultList();
        return deliveryTaskEntityList.size() >= 1 ? deliveryTaskEntityList:new ArrayList<DeliveryTaskEntity>();
    }

    public DeliveryTaskEntity getDeliveryTaskOfTheEvaluationUser(Long evaluationId, Long userId){
        List<DeliveryTaskEntity> deliveryTaskEntityList = this.getEntityManager().createQuery("SELECT m FROM DeliveryTaskEntity as m where m.task.evaluation ='"+evaluationId+"' and m.user.id ='"+userId+"'", DeliveryTaskEntity.class).getResultList();
        return deliveryTaskEntityList.size() == 1 ? deliveryTaskEntityList.get(0):null;
    }
}
