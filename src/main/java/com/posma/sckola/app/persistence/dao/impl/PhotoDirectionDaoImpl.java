package com.posma.sckola.app.persistence.dao.impl;

import com.posma.sckola.app.persistence.dao.AbstractDao;
import com.posma.sckola.app.persistence.dao.PhotoDirectionDao;
import com.posma.sckola.app.persistence.entity.PhotoDirectionEntity;
import org.springframework.stereotype.Repository;

/**
 * Created by Posma-ricardo on 13/07/2017.
 */
@Repository("PhotoDirectionDao")
public class PhotoDirectionDaoImpl extends AbstractDao<PhotoDirectionEntity> implements PhotoDirectionDao {

    public PhotoDirectionDaoImpl(){
        super();
        setClazz(PhotoDirectionEntity.class);
    }
}
