package com.posma.sckola.app.persistence.dao;

import com.posma.sckola.app.persistence.entity.TaskEntity;

import java.util.List;

public interface TaskDao {


    TaskEntity findOne(long id);

    List<TaskEntity> findAll();

    List<TaskEntity> findAllByFieldId(String fieldName, long id);

    TaskEntity create(TaskEntity taskEntity);

    TaskEntity update(TaskEntity taskEntity);

    void delete(TaskEntity taskEntity);

    void deleteById(long id);

    List<TaskEntity> getAllTaskForMatterCommunitySection(Long matterCommunitySectionId);

    TaskEntity getTaskForEvaluation(Long evaluationId);
}
