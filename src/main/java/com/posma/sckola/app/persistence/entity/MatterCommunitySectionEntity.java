package com.posma.sckola.app.persistence.entity;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Francis Ries on 18/04/2017.
 */
@Entity
@Table(name = "MATTER_COMMUNITY_SECTION",
        indexes = {
                @Index(columnList ="USER_ID",name = "IDX_USER_ID_MATTER_COMMUNITY_SECTION"),
                @Index(columnList ="MATTER_COMMUNITY_ID",name = "IDX_MATTERCOMMUNITY_MATTCOMM_SECTION"),
                @Index(columnList ="SECTION_ID",name = "IDX_SECTION_ID_MATTCOMM_SECTION")
        })
@NamedQueries({
        @NamedQuery(
                name = MatterCommunitySectionEntity.FIND_BY_USER_ID,
                query = "select u from MatterCommunitySectionEntity u where user.id = :id"
        )
})
public class MatterCommunitySectionEntity implements Serializable {

    public static final String FIND_BY_USER_ID = "matterCommunitySection.findByUserId";

    @Id
    @Basic(optional = false)
    @Column(name="ID")
    @SequenceGenerator(name = "MATTER_COMMUNITY_SECTION_SEQ", sequenceName = "S_ID_MATTER_COMMUNITY_SECTION", initialValue = 1, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "MATTER_COMMUNITY_SECTION_SEQ")
    private Long id;


    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "USER_ID", nullable = false)
    private UserEntity user;

    @ManyToOne
    @JoinColumn(name = "MATTER_COMMUNITY_ID", referencedColumnName = "ID", nullable = false)
    private MatterCommunityEntity matterCommunity;

    @ManyToOne
    @JoinColumn(name = "SECTION_ID", referencedColumnName = "ID", nullable = false)
    private SectionEntity section;

    @Column(name="STATUS")
    private StatusClass status;

    @Column(name = "TYPE", nullable=false)
    private  SectionType type;

    @OneToOne
    @JoinColumn(name = "EVALUACTION_PLAN_ID", referencedColumnName = "ID", nullable = true)
    private EvaluationPlanEntity evaluationPlan;

/*
    @OneToOne
    @PrimaryKeyJoinColumn
    private EvaluationPlanEntity evaluationPlan;
*/

    public MatterCommunitySectionEntity() {

    }

    public MatterCommunitySectionEntity(MatterCommunityEntity matterCommunity, SectionEntity section, UserEntity user, StatusClass status, SectionType type) {
        this.matterCommunity = matterCommunity;
        this.section = section;
        this.user = user;
        this.status = status;
        this.type = type;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public UserEntity getUser(){return user;}

    public void setUser(UserEntity user){this.user = user;}

    public MatterCommunityEntity getMatterCommunity() {
        return matterCommunity;
    }

    public void setMatterCommunity(MatterCommunityEntity matterCommunity) {
        this.matterCommunity = matterCommunity;
    }

    public SectionEntity getSection() {
        return section;
    }

    public StatusClass getStatus(){return status;}

    public void setStatus(StatusClass status){this.status = status;}

    public SectionType getType(){return type;}

    public void setType(SectionType type){this.type = type;}

    public void setSection(SectionEntity section) {
        this.section = section;
    }

    public EvaluationPlanEntity getEvaluationPlan() {
        return evaluationPlan;
    }

    public void setEvaluationPlan(EvaluationPlanEntity evaluationPlan) {
        this.evaluationPlan = evaluationPlan;
    }


}
