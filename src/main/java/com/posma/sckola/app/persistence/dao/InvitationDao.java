package com.posma.sckola.app.persistence.dao;


import com.posma.sckola.app.persistence.entity.InvitationEntity;

import java.util.List;

public interface InvitationDao {

    InvitationEntity findOne(long id);

    List<InvitationEntity> findAll();

    InvitationEntity create(InvitationEntity invitation);

    InvitationEntity update(InvitationEntity invitation);

    void delete(InvitationEntity invitation);

    void deleteById(long id);

    InvitationEntity findByMail(String mail);
}
