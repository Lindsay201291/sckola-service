package com.posma.sckola.app.util;

import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Francis on 28/03/2017.
 */


@Service
public class Validation {

    final static String formatMail = "^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}$";
    final static String formatDate = "dd-MM-yyyy";

    public Validation(){}

    /**
     * Valida si el string posee formato correcto de correo electronico.
     *@param email
     *@return true si es correcta o false si no lo es.
     */
    public static synchronized boolean isEmail(String email) {

        boolean valido = false;

        Pattern patronEmail = Pattern.compile(formatMail);

        Matcher mEmail = patronEmail.matcher(email.toUpperCase());
        if (mEmail.matches()){
            valido = true;
        }
        return valido;
    }


    /**
     * Devuele un java.util.Date desde un String en formato dd-MM-yyyy
     * @param dateIn La fecha a convertir a formato Date
     * @return Retorna la fecha en formato Date
     */
    public static synchronized Date stringToDate(String dateIn) {
        if(dateIn == null)
            return null;

        SimpleDateFormat formatoDelTexto = new SimpleDateFormat(formatDate);
        Date dateOut = null;
        try {
            dateOut = formatoDelTexto.parse(dateIn);
            return dateOut;
        } catch (ParseException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    /**
     * Devuele una fecha en String desde un Date con formato dd-MM-yyyy
     * @param dateIn La fecha a convertir a formato String
     * @return Retorna la fecha en formato String
     */
    public static synchronized String dateToString(Date dateIn) {
        if(dateIn == null)
            return null;

        SimpleDateFormat formatoDelTexto = new SimpleDateFormat(formatDate);
        String dateOut = null;
        try {
            dateOut = formatoDelTexto.format(dateIn);
            return dateOut;
        } catch (java.util.IllegalFormatException  ex) {
            ex.printStackTrace();
            return null;
        }
    }

}
