package com.posma.sckola.app.aspects;

import org.apache.log4j.Logger;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;

/**
 * Created by francis on 27/03/2017.
 */
@Aspect
public class LoggingAspect {

    private static final Logger LOGGER = Logger.getLogger("logger");

    /**
     * Este aspecto se ejecuta alrededor de las funciones en los paquetes controllers, business, persistence.dao
     * hace el log antes y luego de ejecutar.
     * @param joinPoint es el punto de intercepcion de la funcion
     * @return El objeto que devuelve la funcion interceptada
     * @throws Throwable
     */
    @Around(" execution(* com.posma.sckola.app.controller.*.*(..)) || execution(* com.posma.sckola.app.business.*.*(..)) || execution(* com.posma.sckola.app.persistence.dao.*.*(..))")
    public Object logAround(ProceedingJoinPoint joinPoint) throws Throwable {

        String className = joinPoint.getTarget().getClass().getName();
        String methodName = joinPoint.getSignature().getName();

        LOGGER.info("Class: "+ className +",Beginning method: "+ methodName);
        Object result = joinPoint.proceed();
        LOGGER.info("Class: "+ className+", Ending: "+ methodName);

        return result;
    }

}
