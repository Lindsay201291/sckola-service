package com.posma.sckola.app.business;


import com.posma.sckola.app.dto.MessageDto;
import com.posma.sckola.app.dto.TaskDto;

public interface TaskBF {

    MessageDto createTask(TaskDto taskDto);

    MessageDto updateTask(TaskDto taskDto);

    MessageDto deleteTask(Long taskId);

    MessageDto getTask(Long taskId);

    MessageDto getAllTaskForMatterCommunirySection(Long matterCommunitySectionId);

}
