package com.posma.sckola.app.business;

import com.posma.sckola.app.dto.DeliveryTaskDto;
import com.posma.sckola.app.dto.MessageDto;

public interface DeliveryTaskBF {

    MessageDto createDeliveryTask(DeliveryTaskDto deliveryTaskDto);

    MessageDto updateDeliveryTask(DeliveryTaskDto deliveryTaskDto);

    MessageDto deleteDeliveryTask(Long deliveryTaskId);

    MessageDto getDeliveryTask(Long deliveryTaskId);

    MessageDto getAllDeliveryTaskForTask(Long taskId);

    MessageDto getAllDeliveryTaskForTaskUser(Long taskId, Long userId);
}
