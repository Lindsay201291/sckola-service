package com.posma.sckola.app.business.impl;

import com.posma.sckola.app.business.DeliveryTaskBF;
import com.posma.sckola.app.dto.DeliveryTaskDto;
import com.posma.sckola.app.dto.MessageDto;
import com.posma.sckola.app.persistence.dao.DeliveryTaskDao;
import com.posma.sckola.app.persistence.dao.TaskDao;
import com.posma.sckola.app.persistence.dao.UserDao;
import com.posma.sckola.app.persistence.entity.DeliveryTaskEntity;
import com.posma.sckola.app.persistence.entity.TaskEntity;
import com.posma.sckola.app.persistence.entity.UserEntity;
import com.posma.sckola.app.util.EntityMapper;
import com.posma.sckola.app.util.SystemMessage;
import com.posma.sckola.app.util.Validation;
import org.json.simple.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class DeliveryTaskBFImpl implements DeliveryTaskBF {

    @Autowired
    DeliveryTaskDao deliveryTaskDao;

    @Autowired
    Validation validation;

    @Autowired
    EntityMapper entityMapper;

    @Autowired
    UserDao userDao;

    @Autowired
    TaskDao taskDao;

    @Autowired
    SystemMessage systemMessage;

    @Override
    @Transactional
    public MessageDto createDeliveryTask(DeliveryTaskDto deliveryTaskDto){
        MessageDto messageDto = new MessageDto();

        try{
            DeliveryTaskEntity deliveryTaskEntity = deliveryTaskDao.getDeliveryTaskOfTheTaskUser(deliveryTaskDto.getTask().getId(),deliveryTaskDto.getUser().getId());
            UserEntity userEntity = userDao.findOne(deliveryTaskDto.getUser().getId());
            TaskEntity taskEntity = taskDao.findOne(deliveryTaskDto.getTask().getId());

            if(deliveryTaskEntity != null){
                deliveryTaskEntity.setAdjunt(deliveryTaskDto.getAdjunt());
                deliveryTaskEntity.setCompletionDate(validation.stringToDate(deliveryTaskDto.getCompletionDate()));
                deliveryTaskDao.update(deliveryTaskEntity);
                messageDto.setMessage("Tarea entregada actualizada");
                return messageDto;
            }

            deliveryTaskEntity = entityMapper.boToEntity(deliveryTaskDto);
            deliveryTaskEntity.setUser(userEntity);
            deliveryTaskEntity.setTask(taskEntity);

            deliveryTaskEntity = deliveryTaskDao.create(deliveryTaskEntity);

            if(deliveryTaskEntity == null){
                messageDto.setSuccess(false);
                messageDto.setErrorCode("SK-001");
                messageDto.setMessage(systemMessage.getMessage("SK-001"));
            }

            messageDto.setResponse(entityMapper.entityToBO(deliveryTaskEntity));

        }catch(Exception e){
            e.printStackTrace();
            messageDto.setSuccess(false);
            messageDto.setMessage(e.getMessage());
        }

        return messageDto;
    }

    @Override
    @Transactional
    public MessageDto updateDeliveryTask(DeliveryTaskDto deliveryTaskDto){
        MessageDto messageDto = new MessageDto();

        try{
            DeliveryTaskEntity deliveryTaskEntity = deliveryTaskDao.getDeliveryTaskOfTheTaskUser(deliveryTaskDto.getTask().getId(),deliveryTaskDto.getUser().getId());

            if(deliveryTaskEntity == null){
                messageDto.setSuccess(false);
                messageDto.setErrorCode("SK-002");
                messageDto.setMessage(systemMessage.getMessage("SK-002"));
                return messageDto;
            }

            deliveryTaskEntity.setValue(deliveryTaskDto.getValue());

            deliveryTaskEntity = deliveryTaskDao.update(deliveryTaskEntity);

            if(deliveryTaskEntity == null){
                messageDto.setSuccess(false);
                messageDto.setErrorCode("SK-003");
                messageDto.setMessage(systemMessage.getMessage("SK-003"));
                return messageDto;
            }

            messageDto.setResponse(entityMapper.entityToBO(deliveryTaskEntity));

        }catch(Exception e){
            e.printStackTrace();
            messageDto.setSuccess(false);
            messageDto.setMessage(e.getMessage());
        }

        return messageDto;
    }

    @Override
    @Transactional
    public MessageDto deleteDeliveryTask(Long deliveryTaskId){
        MessageDto messageDto = new MessageDto();

        try{
            DeliveryTaskEntity deliveryTaskEntity = deliveryTaskDao.findOne(deliveryTaskId);

            if(deliveryTaskEntity == null){
                messageDto.setSuccess(false);
                messageDto.setErrorCode("SK-002");
                messageDto.setMessage(systemMessage.getMessage("SK-002"));
                return messageDto;
            }

            deliveryTaskDao.delete(deliveryTaskEntity);

        }catch(Exception e){
            e.printStackTrace();
            messageDto.setSuccess(false);
            messageDto.setMessage(e.getMessage());
        }

        return messageDto;
    }

    @Override
    @Transactional
    public MessageDto getDeliveryTask(Long deliveryTaskId){
        MessageDto messageDto = new MessageDto();

        try{
            DeliveryTaskEntity deliveryTaskEntity = deliveryTaskDao.findOne(deliveryTaskId);

            if(deliveryTaskEntity == null){
                messageDto.setSuccess(false);
                messageDto.setErrorCode("SK-002");
                messageDto.setMessage(systemMessage.getMessage("SK-002"));
                return messageDto;
            }

            messageDto.setResponse(entityMapper.entityToBO(deliveryTaskEntity));

        }catch(Exception e){
            e.printStackTrace();
            messageDto.setSuccess(false);
            messageDto.setMessage(e.getMessage());
        }

        return messageDto;
    }

    @Override
    @Transactional
    public MessageDto getAllDeliveryTaskForTask(Long taskId){
        MessageDto messageDto = new MessageDto();

        try{
            List<DeliveryTaskEntity> deliveryTaskEntitys = deliveryTaskDao.getAllDeliveryTaskForTask(taskId);

            if(deliveryTaskEntitys == null){
                messageDto.setSuccess(false);
                messageDto.setErrorCode("SK-002");
                messageDto.setMessage(systemMessage.getMessage("SK-002"));
                return messageDto;
            }

            List<DeliveryTaskDto> deliveryTaskDtos = new ArrayList<DeliveryTaskDto>();
            for(DeliveryTaskEntity deliveryTaskEntity1: deliveryTaskEntitys){
                deliveryTaskDtos.add(entityMapper.entityToBO(deliveryTaskEntity1));
            }

            messageDto.setResponse(deliveryTaskDtos);

        }catch(Exception e){
            e.printStackTrace();
            messageDto.setSuccess(false);
            messageDto.setMessage(e.getMessage());
        }

        return messageDto;
    }

    @Override
    @Transactional
    public MessageDto getAllDeliveryTaskForTaskUser(Long taskId, Long userId){
        MessageDto messageDto = new MessageDto();

        try{
            DeliveryTaskEntity deliveryTaskEntity = deliveryTaskDao.getDeliveryTaskOfTheTaskUser(taskId, userId);

            DeliveryTaskDto deliveryTaskDto = null;

            if(deliveryTaskEntity != null){
                deliveryTaskDto = entityMapper.entityToBO(deliveryTaskEntity);
            }


            messageDto.setResponse(deliveryTaskDto);

        }catch(Exception e){
            e.printStackTrace();
            messageDto.setSuccess(false);
            messageDto.setMessage(e.getMessage());
        }

        return messageDto;
    }
}
