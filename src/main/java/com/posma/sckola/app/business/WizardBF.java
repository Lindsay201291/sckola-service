package com.posma.sckola.app.business;

import com.posma.sckola.app.dto.MessageDto;
import com.posma.sckola.app.dto.WizardDto;
import com.posma.sckola.app.persistence.entity.UserEntity;

/**
 * Created by Francis on 29/05/2017.
 */
public interface WizardBF {


    /**
     * Consulta la informacion referente al proceso del wizard para el usuario userId
     * @since 29/05/2017
     * @return MessageDto (wizardDto)
     * @author FrancisRies
     * @version 1.0
     */
    MessageDto get(Long userId);

    /**
     * validar inicio de session en el systema
     * @since 29/05/2017
     * @return identifier login
     * @author FrancisRies
     * @version 1.0
     */
    MessageDto save(UserEntity userEntity);


    /**
     * Service that update step name from wizard
     * @since 29/05/2017
     * @return identifier login
     * @author FrancisRies
     * @version 1.0
     */
    MessageDto update(WizardDto wizardDto);

    /**
     * Cierra el ciclo del wizard
     * @since 29/05/2017
     * @param wizardDto
     * @return wizardDto
     * @author FrancisRies
     * @version 1.0
     */
    MessageDto finish(WizardDto wizardDto);


    MessageDto updateCommunity(Long idCommunity, Long IdUser);

    MessageDto updateProfileUser( Long IdUser);

}


