package com.posma.sckola.app.business;

import com.posma.sckola.app.dto.MessageDto;
import com.posma.sckola.app.dto.SectionTypeDto;


/**
 * Created by Francis Ries on 27/03/2017.
 */
public interface MatterCommunityBF {


    /**
     * Service query all Matters from a community
     * @since 20/04/2017
     * @author FrancisRies
     * @version 1.0
     */
    //MessageDto matterGetAllByCommunity(Long communityId);



    /**
     * Service get detall from Matters from community
     * @since 21/04/2017
     * @author FrancisRies
     * @version 1.0
     */
    MessageDto matterCommunityGetById(Long matterCommunityId);



    /**
     * creea la clase (relacion Materia, seccion, profesor, SectionType)
     * @since 21/04/2017
     * @author FrancisRies
     * @version 1.0
     */
    MessageDto matterCommunitySectionCreate(Long matterCommunityId, Long sectionId, Long userId, SectionTypeDto sectionType);

}


