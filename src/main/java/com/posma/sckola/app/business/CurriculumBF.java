package com.posma.sckola.app.business;

import com.posma.sckola.app.dto.MessageDto;
import com.posma.sckola.app.dto.CurriculumDto;


/**
 * Created by Francis Ries on 27/03/2017.
 */
public interface CurriculumBF {

    /**
     * Service to create curriculum
     * @since 20/04/2017
     * @param curriculumDto
     * @param userId
     * @author FrancisRies
     * @version 1.0
     */
    MessageDto curriculumCreate(CurriculumDto curriculumDto, Long userId);


    /**
     * Service to update data from curriculum
     * @since 20/04/2017
     * @param curriculumId curriculum identifier
     * @param curriculumDto curriculum dto with all params to update
     * @author FrancisRies
     * @version 1.0
     */
    MessageDto curriculumUpdate(Long curriculumId, CurriculumDto curriculumDto);

}


