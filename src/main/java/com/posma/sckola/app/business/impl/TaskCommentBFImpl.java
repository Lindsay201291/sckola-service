package com.posma.sckola.app.business.impl;

import com.posma.sckola.app.business.TaskCommentBF;
import com.posma.sckola.app.dto.MessageDto;
import com.posma.sckola.app.dto.TaskCommentDto;
import com.posma.sckola.app.persistence.dao.TaskCommentDao;
import com.posma.sckola.app.persistence.dao.TaskDao;
import com.posma.sckola.app.persistence.dao.UserDao;
import com.posma.sckola.app.persistence.entity.TaskCommentEntity;
import com.posma.sckola.app.persistence.entity.TaskEntity;
import com.posma.sckola.app.persistence.entity.UserEntity;
import com.posma.sckola.app.util.EntityMapper;
import com.posma.sckola.app.util.SystemMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class TaskCommentBFImpl implements TaskCommentBF{

    @Autowired
    EntityMapper entityMapper;

    @Autowired
    SystemMessage systemMessage;

    @Autowired
    TaskCommentDao taskCommentDao;

    @Autowired
    UserDao userDao;

    @Autowired
    TaskDao taskDao;

    @Override
    @Transactional
    public MessageDto createTaskComment(TaskCommentDto taskCommentDto){
        MessageDto messageDto = new MessageDto();

        try{
            TaskCommentEntity taskCommentEntity = new TaskCommentEntity();
            TaskEntity taskEntity = taskDao.findOne(taskCommentDto.getTask().getId());
            UserEntity userEntity = userDao.findOne(taskCommentDto.getUser().getId());

            if(userEntity == null || taskEntity == null){
                messageDto.setSuccess(false);
                messageDto.setErrorCode("SK-002");
                messageDto.setMessage(systemMessage.getMessage("SK-002"));
                return messageDto;
            }

            taskCommentEntity = entityMapper.boToEntity(taskCommentDto);
            taskCommentEntity.setTask(taskEntity);
            taskCommentEntity.setUser(userEntity);

            taskCommentEntity = taskCommentDao.create(taskCommentEntity);

            if(taskCommentEntity == null){
                messageDto.setSuccess(false);
                messageDto.setErrorCode("SK-001");
                messageDto.setMessage(systemMessage.getMessage("SK-001"));
            }

            messageDto.setResponse(entityMapper.entityToBO(taskCommentEntity));

        }catch(Exception e){
            e.printStackTrace();
            messageDto.setSuccess(false);
            messageDto.setMessage(e.getMessage());
        }

        return messageDto;
    }

    @Override
    @Transactional
    public MessageDto updateTaskComment(TaskCommentDto taskCommentDto){
        MessageDto messageDto = new MessageDto();

        try{
            TaskCommentEntity taskCommentEntity = taskCommentDao.findOne(taskCommentDto.getId());

            if(taskCommentEntity == null){
                messageDto.setSuccess(false);
                messageDto.setErrorCode("SK-002");
                messageDto.setMessage(systemMessage.getMessage("SK-002"));
                return messageDto;
            }

            taskCommentEntity.setComment(taskCommentDto.getComment());

            taskCommentEntity = taskCommentDao.update(taskCommentEntity);

            if(taskCommentEntity == null){
                messageDto.setSuccess(false);
                messageDto.setErrorCode("SK-001");
                messageDto.setMessage(systemMessage.getMessage("SK-001"));
            }

            messageDto.setResponse(entityMapper.entityToBO(taskCommentEntity));

        }catch(Exception e){
            e.printStackTrace();
            messageDto.setSuccess(false);
            messageDto.setMessage(e.getMessage());
        }

        return messageDto;
    }

    @Override
    @Transactional
    public MessageDto deleteTaskComment(Long taskCommentId){
        MessageDto messageDto = new MessageDto();

        try{
            TaskCommentEntity taskCommentEntity = taskCommentDao.findOne(taskCommentId);

            if(taskCommentEntity == null){
                messageDto.setSuccess(false);
                messageDto.setErrorCode("SK-002");
                messageDto.setMessage(systemMessage.getMessage("SK-002"));
                return messageDto;
            }

            taskCommentDao.delete(taskCommentEntity);

        }catch(Exception e){
            e.printStackTrace();
            messageDto.setSuccess(false);
            messageDto.setMessage(e.getMessage());
        }

        return messageDto;
    }

    @Override
    @Transactional
    public MessageDto getAllTaskCommentForTask(Long taskId){
        MessageDto messageDto = new MessageDto();

        try{
            List<TaskCommentEntity> taskCommentEntity = taskCommentDao.findAllByTask(taskId);

            if(taskCommentEntity == null){
                messageDto.setSuccess(false);
                messageDto.setErrorCode("SK-002");
                messageDto.setMessage(systemMessage.getMessage("SK-002"));
                return messageDto;
            }

            List<TaskCommentDto> taskCommentDtos = new ArrayList<TaskCommentDto>();

            for(TaskCommentEntity taskCommentEntity1: taskCommentEntity){
                taskCommentDtos.add(entityMapper.entityToBO(taskCommentEntity1));
            }

            messageDto.setResponse(taskCommentDtos);

        }catch(Exception e){
            e.printStackTrace();
            messageDto.setSuccess(false);
            messageDto.setMessage(e.getMessage());
        }

        return messageDto;
    }
}
