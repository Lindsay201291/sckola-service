package com.posma.sckola.app.business.impl;

import com.posma.sckola.app.business.NotificationBF;
import com.posma.sckola.app.dto.*;
import com.posma.sckola.app.persistence.dao.AssistanceByMatterDao;
import com.posma.sckola.app.persistence.dao.ConfMailDao;
import com.posma.sckola.app.persistence.dao.EvaluationDao;
import com.posma.sckola.app.persistence.dao.MatterCommunitySectionDao;
import com.posma.sckola.app.persistence.dao.NotificationDao;
import com.posma.sckola.app.persistence.dao.UserDao;
import com.posma.sckola.app.persistence.dao.UserRoleCommunityDao;
import com.posma.sckola.app.persistence.entity.*;
import com.posma.sckola.app.util.EntityMapper;
import com.posma.sckola.app.util.NotificationMessage;
import com.posma.sckola.app.util.SystemMessage;
import com.posma.sckola.app.util.Validation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.persistence.NoResultException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Properties;

/**
 * Created by Francis on 16/02/2017.
 */
@Service
@EnableScheduling
public class NotificationBFImpl implements NotificationBF {

    @Autowired
    ConfMailDao confMailDao;

    @Autowired
    MatterCommunitySectionDao matterCommunitySectionDao;

    @Autowired
    UserRoleCommunityDao userRoleCommunityDao;

    @Autowired
    AssistanceByMatterDao assistanceByMatterDao;

    @Autowired
    NotificationDao notificationDao;

    @Autowired
    EvaluationDao evaluationDao;

    @Autowired
    EntityMapper entityMapper;

    @Autowired
    Validation validation;

    @Autowired
    SystemMessage systemMessage;

    @Autowired
    UserDao userDao;

    @Autowired
    NotificationMessage notificationMessage;

    /**
     * Service that query all mail request
     *
     * @return NotificationDto
     * @author FrancisRies
     * @version 1.0
     * @since 16/02/2017
     */
    @Override
    @Transactional
    public Boolean sendNotification(NotificationDto notificationDto) {
        ConfMailEntity mailEntity = confMailDao.findOne(1);
        boolean tokenSendMail = true;
        if (!sendMail(notificationDto.getTo(), notificationDto.getTitle(), notificationDto.getText())) {
            if (!sendMail(notificationDto.getTo(), notificationDto.getTitle(), notificationDto.getText())) {
                if (!sendMail(notificationDto.getTo(), notificationDto.getTitle(), notificationDto.getText())) {
                    // no fue posible enviar el correo luego de tres intentos
                    // cambiar esto para un been que crea procesos por cada solicitud y que en caso de falla duerme 1min y vuelve a intentar, luevo 2 min y vuelve a tratar,
                    // en unltima instacia notifica la falla a un proceso que manejara las fallas de correo, para que el usuario pueda volver a enviar la invitacion
                    tokenSendMail = false;
                }
            }
        }
        ;

        return new Boolean(tokenSendMail);
    }

    /**
     * Observer for notifications in active users
     */
    @Override
    @Transactional
    @Scheduled(fixedDelay = 30000)
    public void newNotifications() {
        List<UserEntity> activeUsers = userDao.findAllByFieldStatus(StatusUser.ACTIVE);
        for (UserEntity user : activeUsers) {
            updateNotificationsByUser(user);
        }
    }


    /**
     * Used to update the status of the notification to SENDED.
     *
     * @param notification notification to change status.
     * @return boolean
     */
    @Override
    @Transactional
    public boolean updateNotificationStatus(DashboardNotificationDto notification) {


        NotificationEntity notificationUpdate = notificationDao.findOne(notification.getId());
        notificationUpdate.setStatus(NotificationStatus.SENDED);
        notificationDao.update(notificationUpdate);
        return true;
    }

    /**
     * Service to get notifications in database.
     *
     * @return List with all the notifications to send.
     */
    @Override
    public List<DashboardNotificationDto> getNotifications() {

        List<UserEntity> activeUsers = userDao.findAllByFieldStatus(StatusUser.ACTIVE);
        List<NotificationEntity> notificationsDB = new ArrayList<NotificationEntity>();
        for (UserEntity user : activeUsers) {
            notificationsDB.addAll(notificationDao.findByUser(user.getMail()));
        }
        return processToResult(notificationsDB);
    }

    /**
     * Gets history notifications for a user in a community
     * @param userMail email of the user to find history
     * @param communityId id of the community
     * @return return history notifications and active notifications actually
     */
    @Override
    public MessageDto getHistoryNotificationByUser(String userMail, Long communityId){
        MessageDto messageDto = new MessageDto();
        try{
            List<NotificationEntity> notifications = new ArrayList<>(notificationDao.findHistoryNotification(userMail,communityId));
            List<DashboardNotificationDto> notificationsResult = processToResult(notifications);
            NotificationHistoryDto history = new NotificationHistoryDto();
            history.setNotifications(notificationsResult);
            int activeNotifications = 0;
            for(DashboardNotificationDto notification: notificationsResult){
                if(notification.isActive()){
                    activeNotifications++;
                }
            }
            history.setActiveNotifications(activeNotifications);
            messageDto.setResponse(history);
            return messageDto;
        }
        catch(NoResultException ex){
            messageDto.setSuccess(false);
            messageDto.setErrorCode("SK-002");
            messageDto.setMessage(systemMessage.getMessage("SK-002") + " - Matter not exists");
            return messageDto;
        }
    }

    /**
     * Service to get all active notifications for an user.
     *
     * @param userMail mail from user to get notifications.
     * @return List with all the active notifications from the given mail.
     */
    @Override
    @Transactional
    public MessageDto getActiveNotificationsByUser(String userMail, Long id) {
        MessageDto messageDto = new MessageDto();
        try {
            List<NotificationEntity> notifications = new ArrayList<>(notificationDao.findActiveNotificationByUser(userMail,NotificationType.CARTELERA));
            messageDto.setResponse(processToResult(notifications));
            return messageDto;
        } catch (NoResultException ex) {
            messageDto.setSuccess(false);
            messageDto.setErrorCode("SK-002");
            messageDto.setMessage(systemMessage.getMessage("SK-002") + " - Matter not exists");
            return messageDto;
        }
    }

    /**
     * Sets an active notification to inactive, because the user already attended it.
     *
     * @param id notification id.
     * @return MessageDto with success or error message.
     */
    @Override
    @Transactional
    public MessageDto removeActiveNotification(Long id) {
        MessageDto messageDto = new MessageDto();
        try {
            NotificationEntity notificationUpdate = notificationDao.findOne(id);
            notificationUpdate.setActive(false);
            notificationDao.update(notificationUpdate);
            messageDto.setResponse(true);
            return messageDto;
        } catch (NoResultException ex) {
            messageDto.setSuccess(false);
            messageDto.setErrorCode("SK-002");
            messageDto.setMessage(systemMessage.getMessage("SK-002") + " - Matter not exists");
            return messageDto;
        }
    }

    /**
     * Gets value List Dto for an evaluation ratings
     *
     * @param ratings scale of the evaluation
     * @return List with all the values dto
     */
    public List<EvaluationValueDto> getScaleDto(List<RatingValueEntity> ratings) {
        List<EvaluationValueDto> values = new ArrayList<>();
        for (RatingValueEntity qualification : ratings) {
            EvaluationValueDto evaluationValue = new EvaluationValueDto();
            evaluationValue.setId(qualification.getId());
            evaluationValue.setValue(qualification.getQualification().getValue());
            evaluationValue.setText(qualification.getQualification().getText());
            evaluationValue.setWeight(qualification.getQualification().getWeight());
            values.add(evaluationValue);
        }
        return values;
    }

    /**
     * Parse an evaluation entity to EvaluationDto of a notification
     * @param evaluation evaluation that belongs to the notification
     * @return EvaluationDto
     */
    public EvaluationDto parseEvaluation(EvaluationEntity evaluation){
        EvaluationDto evaluationDto = new EvaluationDto();
        evaluationDto.setId(evaluation.getId());
        evaluationDto.setStatus(evaluation.getStatus().name());
        EvaluationScaleDto scale = new EvaluationScaleDto();
        scale.setId(evaluation.getRatingScale().getId());
        scale.setEvaluationValueList(getScaleDto(evaluation.getRatingScale().getRatingValueList()));
        evaluationDto.setEvaluationScale(scale);
        return evaluationDto;
    }

    /**
     * Receives a list of notifications and converts it to its respective data transfer object
     *
     * @param notifications notifications to send
     * @return List of notifications data transfer objects
     */
    @Transactional
    public List<DashboardNotificationDto> processToResult(List<NotificationEntity> notifications) {
        List<DashboardNotificationDto> result = new ArrayList<DashboardNotificationDto>();
        for (NotificationEntity notification : notifications) {
            DashboardNotificationDto notificationResult = new DashboardNotificationDto();
            if(notification.getCommunity()!=null) {
                CommunityDto community = new CommunityDto();
                community.setId(notification.getCommunity().getId());
                community.setName(notification.getCommunity().getName());
                notificationResult.setCommunity(community);
            }
            if (notification.getMatter()!=null) {
                MatterDto matter = new MatterDto();
                matter.setId(notification.getMatter().getId());
                matter.setName(notification.getMatter().getMatterCommunity().getName());
                notificationResult.setMatter(matter);
            }
            if (notification.getEvaluation() != null) {
                EvaluationDto evaluationDto;
                if(notification.getEvaluation().getStatus() == StatusEvaluation.PENDING){
                    evaluationDto = parseEvaluation(notification.getEvaluation());
                }
                else{
                    evaluationDto = entityMapper.entityToBO(notification.getEvaluation());
                }
                notificationResult.setEvaluation(evaluationDto);
            }
            if(notification.getSection()!=null) {
                SectionDto section = new SectionDto();
                section.setId(notification.getSection().getId());
                section.setName(notification.getSection().getName());
                notificationResult.setSection(section);
            }


            notificationResult.setId(notification.getId());
            notificationResult.setFrom(notification.getTypeName());
            notificationResult.setText(notification.getText());
            notificationResult.setUsername(notification.getUser().getMail());
            notificationResult.setType(notification.getType());
            notificationResult.setActive(notification.isActive());
            notificationResult.setDate(notification.getDate().toString());
            result.add(notificationResult);
        }
        return result;
    }

    /**
     * Get all notifications with status NOT_SEND for an specific user.
     *
     * @param user user owner of the notifications.
     */
    public void updateNotificationsByUser(UserEntity user) {

        boolean communitySeleccted=false;

        //materias por comunidades.
        List<MatterCommunitySectionEntity> teacherMatters = matterCommunitySectionDao.findAllMatterCommunitySectionByTeacher(user.getId(), StatusClass.ACTIVE);

        //*************************
        //Selecciono una comunidad?
        List<UserRoleCommunityEntity> userRoleCommunities = null;
        for(RoleEntity userRole : user.getRoleList()) { //busca las comunidades a la que pertenece por cada Rol del usuario
            if(userRole.getName().equals("TEACHER")) { //Filtra solo si el Rol es de profesor
                userRoleCommunities = userRoleCommunityDao.findAllByFieldUserIdRoleId(user.getId(), userRole.getId());
                if (userRoleCommunities.size() <= 0) {
                    createNotification(user, null, NotificationType.COMMUNITY_REQUEST  , null); //Notificacion de comunidad faltante
                }else{
                    communitySeleccted=true;
                }
            }
        }
        //****************************************
        //Lleno Los Datos de al menos una materia?
        if (teacherMatters.size()<=0 && communitySeleccted){
            //El profesor no ha agregado ninguna clase o materia
            createNotification(user, null, NotificationType.SUBJECT_REQUEST, null);//Notificacion de materia faltante
        }

        //*********************************

        for (MatterCommunitySectionEntity sectionEntity : teacherMatters) {

            AssistanceByMatterPk assistanceByMatterPk = new AssistanceByMatterPk();
            assistanceByMatterPk.setMatterCommunitySection(sectionEntity.getId());
            assistanceByMatterPk.setDate(new Date());
            List<AssistanceByMatterEntity> assistances = assistanceByMatterDao.findAllForSectionForDate(assistanceByMatterPk);

            //Tomo la asistencia? Tiene estudiantes en tu sección?
            if (assistances.size() == 0) {
                if (sectionEntity.getSection().getStudentList().size() > 0){
                    //****Si no ha colocado la asistencia a la seccion***//
                    createNotification(user,sectionEntity, NotificationType.ASSISTANCE_REQUEST, null);
                }else{
                    //***Si no tiene estudiantes en una seccion***//
                    createNotification(user,sectionEntity, NotificationType.STUDENTS_REQUEST, null);
                }
            }

            //Tiene Plan de evaluación Cargado?
            if (sectionEntity.getEvaluationPlan() != null) {
                List<EvaluationEntity> evaluations = sectionEntity.getEvaluationPlan().getEvaluationList();
                Date date = new Date();
                DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                Date formattedDate = validation.stringToDate(dateFormat.format(new Date()));

                for (EvaluationEntity evaluation : evaluations) {
                    Date evaluationDate = validation.stringToDate(dateFormat.format(evaluation.getDate()));

                    //Ha colocado califiaciones para la materia/seccion?
                    if (formattedDate.equals(evaluationDate) && sectionEntity.getSection().getStudentList().size() > 0) {
                        createNotification(user, sectionEntity, NotificationType.SCORE_REQUEST, evaluation);
                    }
                }
            }else{
                //No tiene plan de evaluación asociado a esa seccion
                createNotification(user, sectionEntity, NotificationType.EVALUATIONPLAN_REQUEST, null);
            }
        }
    }

    /**
     * Creates a new notification in the database.
     *
     * @param matterCommunitySection Info of the Notification
     * @param type                   Notification type
     * @return
     */
    public NotificationEntity createNotification(UserEntity user, MatterCommunitySectionEntity matterCommunitySection, NotificationType type, EvaluationEntity evaluation) {
        NotificationEntity notification = new NotificationEntity();
        switch (type){
            case ASSISTANCE_REQUEST :
                //notification.setType(NotificationType.CARTELERA);
                notification.setTypeName(notificationMessage.getAttendance());
                notification.setText(notificationMessage.getAttendance_request());
                break;
            case ASSISTANCE_SUCCESS:
                //notification.setType(NotificationType.CARTELERA);
                notification.setTypeName(notificationMessage.getAttendance());
                notification.setText(notificationMessage.getAttendance_request());
                break;
            case SCORE_REQUEST:
                //notification.setType(NotificationType.CARTELERA);
                notification.setTypeName(notificationMessage.getCalifications());
                notification.setText(notificationMessage.getCalification_request());
                break;
            case SCORE_SUCCESS:
                //notification.setType(NotificationType.CARTELERA);
                notification.setTypeName(notificationMessage.getCalifications());
                notification.setText(notificationMessage.getCalification_success());
                break;
            case PROFILE_REQUEST:
                //notification.setType(NotificationType.CARTELERA);
                notification.setTypeName(notificationMessage.getProfile());
                notification.setText(notificationMessage.getProfile_request());
                break;
            case PROFILE_SUCCESS:
                //notification.setType(NotificationType.CARTELERA);
                notification.setTypeName(notificationMessage.getProfile());
                notification.setText(notificationMessage.getProfile_success());
                break;
            case SECTION_REQUEST:
                //notification.setType(NotificationType.CARTELERA);
                notification.setTypeName(notificationMessage.getSection());
                notification.setText(notificationMessage.getSection_request());
                break;
            case SECTION_SUCCESS:
                //notification.setType(NotificationType.CARTELERA);
                notification.setTypeName(notificationMessage.getSection());
                notification.setText(notificationMessage.getSection_success());
                break;
            case SUBJECT_REQUEST:
                //notification.setType(NotificationType.CARTELERA);
                notification.setTypeName(notificationMessage.getSubject());
                notification.setText(notificationMessage.getSubject_request());
                break;
            case SUBJECT_SUCCESS:
                //notification.setType(NotificationType.CARTELERA);
                notification.setTypeName(notificationMessage.getSubject());
                notification.setText(notificationMessage.getSubject_request());
                break;
            case EVALUATIONPLAN_REQUEST:
                //notification.setType(NotificationType.CARTELERA);
                notification.setTypeName(notificationMessage.getEvaluationPlan());
                notification.setText(notificationMessage.getEvaluationPlan_request());
                break;
            case EVALUATIONPLAN_SUCCESS:
                //notification.setType(NotificationType.CARTELERA);
                notification.setTypeName(notificationMessage.getEvaluationPlan());
                notification.setText(notificationMessage.getEvaluationPlan_success());
                break;
            case STUDENTS_REQUEST:
                //notification.setType(NotificationType.CARTELERA);
                notification.setTypeName(notificationMessage.getStudents());
                notification.setText(notificationMessage.getStudents_request());
                break;
            case STUDENTS_SUCCESS:
                //notification.setType(NotificationType.CARTELERA);
                notification.setTypeName(notificationMessage.getStudents());
                notification.setText(notificationMessage.getStudents_success());
                break;
            case COMMUNITY_REQUEST:
                //notification.setType(NotificationType.CARTELERA);
                notification.setTypeName(notificationMessage.getCommunity());
                notification.setText(notificationMessage.getCommunity_request());
                break;
            case COMMUNITY_SUCCESS:
                //notification.setType(NotificationType.CARTELERA);
                notification.setTypeName(notificationMessage.getCommunity());
                notification.setText(notificationMessage.getCommunity_success());
                break;
            case GRADED_STUDENT:
                //notification.setType(NotificationType.CARTELERA);
                notification.setTypeName(notificationMessage.getCalifications());
                notification.setText(notificationMessage.getGraded_student());
                break;
        }

        if(matterCommunitySection!=null) {
            notification.setUser(matterCommunitySection.getUser());
            notification.setCommunity(matterCommunitySection.getMatterCommunity().getCommunity());
            notification.setMatter(matterCommunitySection);
            notification.setSection(matterCommunitySection.getSection());
            notification.setEvaluation(evaluation);
        }else{
            notification.setUser(user);
        }

        notification.setStatus(NotificationStatus.NOT_SENDED);
        notification.setActive(true);
        notification.setType(type);
        Date date = new Date();
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        if(calendar.get(calendar.DAY_OF_WEEK) != 1 || calendar.get(calendar.DAY_OF_WEEK) != 7){
              notification.setDate(validation.stringToDate(dateFormat.format(date)));
            if (!notificationExist(notification)) {
                return notificationDao.create(notification);
            } else {
            return notification;
            }
        }else{
            return null;
        }
    }


    /**
     * Checks if a notification already exist
     *
     * @param notification notification to check
     * @return boolean
     */
    public boolean notificationExist(NotificationEntity notification) {

        if (notificationDao.findByNotification(notification).size() > 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Send mail to users.
     * @param to_bcc destinataries
     * @param subject subject of the mail
     * @param text content of the mail
     * @return boolean
     */
    private boolean sendMail(String to_bcc, String subject, String text) {
        boolean tokenSendMail = false;

        ConfMailEntity mailEntity = confMailDao.findOne(1);

        Properties props = new Properties();
        //NAME OF MAIL HOST, IN THIS CASE IS: smtp.gmail.com
        props.setProperty("mail.smtp.host", mailEntity.getHostSMTP());  //"smtp.gmail.com"

        //TLS IF ENABLE
        String enable = "true";
        props.setProperty("mail.smtp.starttls.enable", enable);

        //PORT OF GMAIL TO SEND MAILS
        props.setProperty("mail.smtp.port", mailEntity.getPort().toString()); //"587"

        //USER NAME
        props.setProperty("mail.smtp.user", mailEntity.getEmail()); //"posmagroupcompany@gmail.com"

        //USER AND PASWWORD TO CONNECT
        props.setProperty("mail.smtp.auth", "true");

        Session session = Session.getDefaultInstance(props);
        session.setDebug(true);

        MimeMessage message = new MimeMessage(session);

        try {
            //RECIPTS
            message.addRecipients(Message.RecipientType.BCC, to_bcc);
            message.setFrom(new InternetAddress(mailEntity.getEmail()));

            message.setSubject(subject);

            //message.setText(text);
            message.setText(text, "ISO-8859-1", "html");
            //SEND
            Transport t = session.getTransport(mailEntity.getProtocol()); //"smtp"
            t.connect(mailEntity.getUserName(), mailEntity.getPassword()); // clave
            t.sendMessage(message, message.getAllRecipients());

            t.close();
            tokenSendMail = true;
            return tokenSendMail;
        } catch (Exception e) {
            e.printStackTrace();
            return tokenSendMail;
        }

    }

    /**
     * Gets history notifications for a user for info
     * @param userMail email of the user to find history
     * @return return history notifications and active notifications actually
     */
    @Override
    public MessageDto getHistoryNotificationByInfo(String userMail){
        MessageDto messageDto = new MessageDto();
        try {
            List<NotificationEntity> notifications = new ArrayList<>(notificationDao.findActiveNotificationByUserByInfo(userMail));
            List<DashboardNotificationDto> notificationsResult = processToResultInfo(notifications);
            NotificationHistoryDto history = new NotificationHistoryDto();
            history.setNotifications(notificationsResult);
            int activeNotifications = 0;
            for(DashboardNotificationDto notification: notificationsResult){
                if(notification.isActive()){
                    activeNotifications++;
                }
            }
            history.setActiveNotifications(activeNotifications);
            messageDto.setResponse(history);
            return messageDto;
        } catch (NoResultException ex) {
            messageDto.setSuccess(false);
            messageDto.setErrorCode("SK-002");
            messageDto.setMessage(systemMessage.getMessage("SK-002") + " - user not exists");
            return messageDto;
        }
    }

    /**
     * Receives a list of notifications and converts it to its respective data transfer object
     *
     * @param notifications notifications to send
     * @return List of notifications data transfer objects
     */
    @Transactional
    public List<DashboardNotificationDto> processToResultInfo(List<NotificationEntity> notifications) {
        List<DashboardNotificationDto> result = new ArrayList<DashboardNotificationDto>();
        for (NotificationEntity notification : notifications) {
            DashboardNotificationDto notificationResult = new DashboardNotificationDto();
            notificationResult.setId(notification.getId());
            notificationResult.setFrom(notification.getTypeName());
            notificationResult.setText(notification.getText());
            notificationResult.setType(notification.getType());
            notificationResult.setActive(notification.isActive());
            notificationResult.setDate(notification.getDate().toString());
            result.add(notificationResult);
        }
        return result;
    }
}