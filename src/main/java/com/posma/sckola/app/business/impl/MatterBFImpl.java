package com.posma.sckola.app.business.impl;

import com.posma.sckola.app.business.MatterBF;
import com.posma.sckola.app.dto.MatterCommunityDto;
import com.posma.sckola.app.dto.MatterCommunitySectionDto;
import com.posma.sckola.app.dto.MatterDto;
import com.posma.sckola.app.dto.MessageDto;
import com.posma.sckola.app.persistence.dao.*;
import com.posma.sckola.app.persistence.entity.*;
import com.posma.sckola.app.util.EntityMapper;
import com.posma.sckola.app.util.SystemMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.NoResultException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Francis Ries on 20/04/2017.
 */
@Service
public class MatterBFImpl implements MatterBF {


    @Autowired
    RoleDao roleDao;

    @Autowired
    MatterDao matterDao;

    @Autowired
    EntityMapper entityMapper;

    @Autowired
    SystemMessage systemMessage;

    @Autowired
    CommunityDao communityDao;

    @Autowired
    MatterCommunityDao matterCommunityDao;

    @Autowired
    MatterCommunitySectionDao matterCommunitySectionDao;

    @Autowired
    SectionDao sectionDao;

    /**
     * Service query all Matters
     * @since 20/04/2017
     * @author FrancisRies
     * @version 1.0
     */
    @Override
    public MessageDto matterGetAll() {

        MessageDto messageDto = new MessageDto();

        List<MatterEntity> matterEntityList = matterDao.findAll();
        List<MatterDto> matterDtoList = new ArrayList<MatterDto>(matterEntityList.size());

        for(MatterEntity matterEntity:matterEntityList){
            matterDtoList.add(entityMapper.entityToBO(matterEntity));
        }

        messageDto.setResponse(matterDtoList);

        return messageDto;
    }



    /**
     * Service query all Matters from a community
     * @since 20/04/2017
     * @author FrancisRies
     * @version 1.0
     */
    @Override
    public MessageDto matterGetAllByCommunity(Long communityId){

        MessageDto messageDto = new MessageDto();

        // buscar la materia en la comunidad a ver si ya existe
        List<MatterCommunityEntity> matterCommunityEntityList = matterCommunityDao.findAllByFieldId("COMMUNITY_ID", communityId.longValue());
        List<MatterCommunityDto> matterCommunityDtoList = new ArrayList<MatterCommunityDto>(matterCommunityEntityList.size());

        for(MatterCommunityEntity matterCommunityEntity:matterCommunityEntityList){
            matterCommunityDtoList.add(entityMapper.entityToBO(matterCommunityEntity));
        }

        messageDto.setResponse(matterCommunityDtoList);

        return messageDto;
    }


    /**
     * Service that copy a Matter to a Community
     * @since 20/04/2017
     * @author FrancisRies
     * @version 1.0
     */
    @Override
    @Transactional
    public MessageDto matterAssociate(Long matterId, Long communityId, boolean associate) {

        MessageDto messageDto = new MessageDto();

        MatterEntity matterEntity;
        CommunityEntity communityEntity;
        try{
            matterEntity = matterDao.findOne(matterId.longValue());
            communityEntity = communityDao.findOne(communityId.longValue());

            if(matterEntity == null || communityEntity == null){
                messageDto.setSuccess(false);
                messageDto.setErrorCode("SK-002");
                messageDto.setMessage(systemMessage.getMessage("SK-002")+ " - Matter or Community not exists");
                return messageDto;
            }

        }catch (NoResultException e){
            messageDto.setSuccess(false);
            messageDto.setErrorCode("SK-002");
            messageDto.setMessage(systemMessage.getMessage("SK-002")+ " - Matter or Community not exists");
            return messageDto;
        }

        // buscar la materia en la comunidad a ver si ya existe
        List<MatterCommunityEntity> matterCommunityEntityList = matterCommunityDao.findAllByMatterCommunityId(matterId.longValue(),communityId.longValue());

        //associate ?
        if(associate){
            if(matterCommunityEntityList != null && matterCommunityEntityList.size() > 0){
                messageDto.setSuccess(false);
                messageDto.setErrorCode("SK-005");
                messageDto.setMessage(systemMessage.getMessage("SK-005")+ " - "+matterEntity.getName());
                return messageDto;
            }
            //realizar copia
            MatterCommunityEntity matterCommunityEntity = new MatterCommunityEntity();

            matterCommunityEntity.setName(matterEntity.getName());
            matterCommunityEntity.setMatter(matterEntity);
            matterCommunityEntity.setCommunity(communityEntity);
            matterCommunityEntity.setObjective(matterEntity.getObjective());
            matterCommunityEntity.setEspecificObjective(matterEntity.getEspecificObjective());
            matterCommunityEntity.setDescription(matterEntity.getDescription());

            matterCommunityEntity = matterCommunityDao.create(matterCommunityEntity);

            messageDto.setResponse(entityMapper.entityToBO(matterCommunityEntity));

        }else{
            // disassociate
            if(matterCommunityEntityList == null || matterCommunityEntityList.size() == 0){
                messageDto.setSuccess(false);
                messageDto.setErrorCode("SK-006");
                messageDto.setMessage(systemMessage.getMessage("SK-006")+ " - "+matterEntity.getName()+"/"+communityEntity.getName());
                return messageDto;
            }

            MatterCommunityEntity matterCommunityEntity = matterCommunityEntityList.get(0);

            matterCommunityDao.delete(matterCommunityEntity);

        }

        return messageDto;
    }



    /**
     * Service get detall from Matters from community
     * @since 21/04/2017
     * @author FrancisRies
     * @version 1.0
     */
    @Override
    public MessageDto matterGetByIdCommunityId(Long matterId, Long communityId){

        MessageDto messageDto = new MessageDto();
        MatterCommunityEntity matterCommunityEntity = new MatterCommunityEntity();

        // buscar la materia en la comunidad a ver si ya existe
        List<MatterCommunityEntity> matterCommunityEntityList = matterCommunityDao.findAllByMatterCommunityId(matterId.longValue(), communityId.longValue());

        if(matterCommunityEntityList != null && matterCommunityEntityList.size() > 0)
            matterCommunityEntity = matterCommunityEntityList.get(0);

        messageDto.setResponse(entityMapper.entityToBO(matterCommunityEntity));

        return messageDto;
    }


    /**
     * Service get detall from Matters
     * @since 21/04/2017
     * @author FrancisRies
     * @version 1.0
     */
    @Override
    public MessageDto matterGetById(Long matterId){

        MessageDto messageDto = new MessageDto();
        MatterEntity matterEntity;

        try{
            matterEntity = matterDao.findOne(matterId.longValue());

            if(matterEntity == null){
                messageDto.setSuccess(false);
                messageDto.setErrorCode("SK-002");
                messageDto.setMessage(systemMessage.getMessage("SK-002")+ " - Matter not exists");
                return messageDto;
            }

            messageDto.setResponse(entityMapper.entityToBO(matterEntity));
            return messageDto;

        }catch (NoResultException e){
            messageDto.setSuccess(false);
            messageDto.setErrorCode("SK-002");
            messageDto.setMessage(systemMessage.getMessage("SK-002")+ " - Matter not exists");
            return messageDto;
        }
    }




    /**
     * list matter from community, by witch a teacher(teaches) or a student(attends)
     * @since 21/04/2017
     * @author FrancisRies
     * @version 1.0
     */
    @Override
    @Transactional
    public MessageDto matterCommunityGetAllByUserRole(Long communityId, Long roleId, Long userId){

        MessageDto messageDto = new MessageDto();
        List<MatterCommunitySectionDto> matterCommunitySectionDtoList = new ArrayList<MatterCommunitySectionDto>();

        RoleEntity roleEntity = roleDao.findOne(roleId.longValue());

        if("TEACHER".equalsIgnoreCase(roleEntity.getName())) {

            // buscar la materia en la comunidad a ver si ya existe
            List<MatterCommunitySectionEntity> matterCommunitySectionEntityList = matterCommunitySectionDao.findAllMatterCommunitySectionByTeacher(communityId.longValue(), userId.longValue(), StatusClass.ACTIVE);


            for (MatterCommunitySectionEntity matterCommunitySectionEntity : matterCommunitySectionEntityList) {
                matterCommunitySectionDtoList.add(entityMapper.entityToBO(matterCommunitySectionEntity));
            }

            messageDto.setResponse(matterCommunitySectionDtoList);
        }

        if("STUDENT".equalsIgnoreCase(roleEntity.getName())) {
            // falta codigo para el caso de ser estudiante

            List<SectionEntity> sectionEntityList = sectionDao.findAllByCommunity(communityId,StatusSection.ACTIVE);
            for(SectionEntity sectionEntity : sectionEntityList){
                for(UserEntity userEntity : sectionEntity.getStudentList()){
                    if(userEntity.getId().equals(userId)){
                        for (MatterCommunitySectionEntity matterCommunitySectionEntity : sectionEntity.getMatterCommunitySectionList()){
                            matterCommunitySectionDtoList.add(entityMapper.entityToBO(matterCommunitySectionEntity));
                        }
                    }
                }
            }
            messageDto.setResponse(matterCommunitySectionDtoList);
        }

        return messageDto;
    }

    /**
     * list matter from community, by witch a teacher(teaches) or a student(attends) without evaluation plan
     * @since 21/04/2017
     * @author FrancisRies
     * @version 1.0
     */
    @Override
    @Transactional
    public MessageDto matterCommunityGetAllByUserRoleWithoutEvaluationPlan(Long communityId, Long roleId, Long userId){

        MessageDto messageDto = new MessageDto();
        MatterCommunityEntity matterCommunityEntity = new MatterCommunityEntity();
        List<MatterCommunitySectionDto> matterCommunitySectionDtoList = new ArrayList<MatterCommunitySectionDto>();

        RoleEntity roleEntity = roleDao.findOne(roleId.longValue());

        if("TEACHER".equalsIgnoreCase(roleEntity.getName())) {

            // buscar la materia en la comunidad a ver si ya existe
            List<MatterCommunitySectionEntity> matterCommunitySectionEntityList = matterCommunitySectionDao.findAllMatterCommunitySectionByTeacherWithoutEvaluationPlan(communityId.longValue(), userId.longValue(), StatusClass.ACTIVE);


            for (MatterCommunitySectionEntity matterCommunitySectionEntity : matterCommunitySectionEntityList) {
                matterCommunitySectionDtoList.add(entityMapper.entityToBO(matterCommunitySectionEntity));
            }

            messageDto.setResponse(matterCommunitySectionDtoList);
        }

        if("STUDENT".equalsIgnoreCase(roleEntity.getName())) {
            // falta codigo para el caso de ser estudiante
        }

        return messageDto;
    }

    /**
     * list matter from community, by witch a teacher(teaches) or a student(attends) and type(Integral or not Integral)
     * @since 10/05/2017
     * @author RicardoBalza
     * @version 1.0
     */
    @Override
    @Transactional
    public MessageDto matterCommunityGetAllByUserRoleByType(Long communityId, Long roleId, Long userId, String type){

        MessageDto messageDto = new MessageDto();
        MatterCommunityEntity matterCommunityEntity = new MatterCommunityEntity();
        List<MatterCommunitySectionDto> matterCommunitySectionDtoList = new ArrayList<MatterCommunitySectionDto>();

        RoleEntity roleEntity = roleDao.findOne(roleId.longValue());

        if("TEACHER".equalsIgnoreCase(roleEntity.getName())) {

            // buscar la materia en la comunidad a ver si ya existe
            List<MatterCommunitySectionEntity> matterCommunitySectionEntityList = matterCommunitySectionDao.findAllMatterCommunitySectionByTeacherByType(
                    communityId.longValue(), userId.longValue(), StatusClass.ACTIVE, SectionType.valueOf(type));


            for (MatterCommunitySectionEntity matterCommunitySectionEntity : matterCommunitySectionEntityList) {
                matterCommunitySectionDtoList.add(entityMapper.entityToBO(matterCommunitySectionEntity));
            }

            messageDto.setResponse(matterCommunitySectionDtoList);
        }

        if("STUDENT".equalsIgnoreCase(roleEntity.getName())) {
            // falta codigo para el caso de ser estudiante
        }

        return messageDto;
    }





    /**
     * Listar todas las materias impartidas por un profesor
     * @since 17/05/2017
     * @author FrancisRies
     * @version 1.0
     */
    @Override
    @Transactional
    public MessageDto matterCommunityGetAllByTeacher(Long roleId, Long userId){

        MessageDto messageDto = new MessageDto();
        List<MatterCommunitySectionDto> matterCommunitySectionDtoList = new ArrayList<MatterCommunitySectionDto>();

        RoleEntity roleEntity = roleDao.findOne(roleId.longValue());

        if("TEACHER".equalsIgnoreCase(roleEntity.getName())) {

            // buscar la materia en la comunidad a ver si ya existe
            List<MatterCommunitySectionEntity> matterCommunitySectionEntityList = matterCommunitySectionDao.findAllMatterCommunitySectionByTeacher(userId.longValue(), StatusClass.ACTIVE);


            for (MatterCommunitySectionEntity matterCommunitySectionEntity : matterCommunitySectionEntityList) {
                matterCommunitySectionDtoList.add(entityMapper.entityToBO(matterCommunitySectionEntity));
            }

            messageDto.setResponse(matterCommunitySectionDtoList);
        }

        if("STUDENT".equalsIgnoreCase(roleEntity.getName())) {
            // falta codigo para el caso de ser estudiante
        }

        return messageDto;
    }







    /**
     * Service to update data from matter
     * @since 19/04/2017
     * @param matterId matter identifier
     * @param matterDto matter dto with all params to update
     * @author FrancisRies
     * @version 1.0
     */
    /*
    @Override
    @Transactional
    public MessageDto matterUpdate(Long matterId, MatterDto matterDto){
        MessageDto messageDto = new MessageDto();

        MatterEntity matterEntity;
        UserEntity userEntity;
        CommunityEntity communityEntity;

        try{
            // busca el curriculo que se quiere actualizar
            matterEntity = matterDao.findOne(matterId);

            if (matterEntity == null){
                messageDto.setSuccess(false);
                messageDto.setErrorCode("SK-002");
                messageDto.setMessage(systemMessage.getMessage("SK-002")+ " - Matter not exists");
                return messageDto;
            }
        }catch (NoResultException e){
            messageDto.setSuccess(false);
            messageDto.setErrorCode("SK-002");
            messageDto.setMessage(systemMessage.getMessage("SK-002")+ " - Matter not exists");

            return messageDto;
        }

        try{

            userEntity = userDao.findOne(matterDto.getUserId().longValue());
            communityEntity = communityDao.findOne(matterDto.getCommunity().getId().longValue());

            if(userEntity == null || communityEntity == null){
                messageDto.setSuccess(false);
                messageDto.setErrorCode("SK-002");
                messageDto.setMessage(systemMessage.getMessage("SK-002")+ " - User or Community not exists");
                return messageDto;
            }

        }catch (NoResultException e){
            messageDto.setSuccess(false);
            messageDto.setErrorCode("SK-002");
            messageDto.setMessage(systemMessage.getMessage("SK-002")+ " - User or Community not exists");
            return messageDto;
        }

        try{
            matterEntity.setTitle(matterDto.getTitle());
            matterEntity.setDate(validation.stringToDate(matterDto.getDate()));
            matterEntity.setCommunity(communityEntity);
            matterEntity.setUser(userEntity);

            matterEntity = matterDao.update(matterEntity);

            if (matterEntity != null && matterEntity.getId() != null) {
                messageDto.setMessage("Matter Update");
                messageDto.setResponse(entityMapper.entityToBO(matterEntity));
            }else{
                messageDto.setSuccess(false);
                messageDto.setErrorCode("SK-003");
                messageDto.setMessage(systemMessage.getMessage("SK-003"));
            }

        }catch (PersistenceException pe){
            pe.printStackTrace();
            messageDto.setSuccess(false);
            messageDto.setErrorCode("SK-001");
            messageDto.setMessage(systemMessage.getMessage("SK-001"));
        }

        return messageDto;
    }
*/

}

