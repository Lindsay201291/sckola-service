package com.posma.sckola.app.business.impl;

import com.posma.sckola.app.business.TaskBF;
import com.posma.sckola.app.dto.MatterCommunitySectionDto;
import com.posma.sckola.app.dto.MessageDto;
import com.posma.sckola.app.dto.TaskDto;
import com.posma.sckola.app.persistence.dao.DeliveryTaskDao;
import com.posma.sckola.app.persistence.dao.EvaluationDao;
import com.posma.sckola.app.persistence.dao.MatterCommunitySectionDao;
import com.posma.sckola.app.persistence.dao.TaskDao;
import com.posma.sckola.app.persistence.entity.*;
import com.posma.sckola.app.util.EntityMapper;
import com.posma.sckola.app.util.SystemMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class TaskBFImpl implements TaskBF {

    @Autowired
    EntityMapper entityMapper;

    @Autowired
    EvaluationDao evaluationDao;

    @Autowired
    MatterCommunitySectionDao matterCommunitySectionDao;

    @Autowired
    TaskDao taskDao;

    @Autowired
    DeliveryTaskDao deliveryTaskDao;

    @Autowired
    SystemMessage systemMessage;

    @Override
    @Transactional
    public MessageDto createTask(TaskDto taskDto){
        MessageDto messageDto = new MessageDto();

        try{
            EvaluationEntity evaluationEntity = null;
            if(taskDto.getEvaluationId() != null){
                evaluationEntity = evaluationDao.findOne(taskDto.getEvaluationId());
            }

            TaskEntity taskEntity = entityMapper.boToEntity(taskDto);
            taskEntity.setEvaluation(evaluationEntity);
            if(taskDto.getEvaluationId() != null) {
                taskEntity.setWeight(evaluationEntity.getWeight());
            }else{
                taskEntity.setWeight(null);
            }

            for(MatterCommunitySectionDto matterCommunitySectionDto: taskDto.getMatterCommunitySectionList()){
                MatterCommunitySectionEntity matterCommunitySectionEntity = matterCommunitySectionDao.findOne(matterCommunitySectionDto.getId());
                taskEntity.addMatterCommunitySectionList(matterCommunitySectionEntity);
            }

            taskEntity = taskDao.create(taskEntity);

            if (taskEntity == null){
                messageDto.setSuccess(false);
                messageDto.setErrorCode("SK-001");
                messageDto.setMessage(systemMessage.getMessage("SK-001"));
            }

            messageDto.setResponse(entityMapper.entityToBO(taskEntity));
        }catch(Exception e){
            e.printStackTrace();
            messageDto.setSuccess(false);
            messageDto.setMessage(e.getMessage());
        }

        return messageDto;
    }

    @Override
    @Transactional
    public MessageDto updateTask(TaskDto taskDto){
        MessageDto messageDto = new MessageDto();

        try{
            TaskEntity taskEntity1 = taskDao.findOne(taskDto.getId());

            if (taskEntity1 == null){
                messageDto.setSuccess(false);
                messageDto.setErrorCode("SK-002");
                messageDto.setMessage(systemMessage.getMessage("SK-002"));
                return messageDto;
            }

            EvaluationEntity evaluationEntity = null;
            if(taskDto.getEvaluationId() != null){
                evaluationEntity = evaluationDao.findOne(taskDto.getEvaluationId());
            }
            TaskEntity taskEntity = entityMapper.boToEntity(taskDto, taskEntity1);
            taskEntity.setEvaluation(evaluationEntity);

            taskEntity = taskDao.update(taskEntity);

            if (taskEntity == null){
                messageDto.setSuccess(false);
                messageDto.setErrorCode("SK-003");
                messageDto.setMessage(systemMessage.getMessage("SK-003"));
                return messageDto;
            }

            messageDto.setResponse(entityMapper.entityToBO(taskEntity));
        }catch(Exception e){
            e.printStackTrace();
            messageDto.setSuccess(false);
            messageDto.setMessage(e.getMessage());
        }

        return messageDto;
    }

    @Override
    @Transactional
    public MessageDto deleteTask(Long taskId){
        MessageDto messageDto = new MessageDto();

        try{
            TaskEntity taskEntity = taskDao.findOne(taskId);
            List<DeliveryTaskEntity> deliveryTaskEntity = deliveryTaskDao.getAllDeliveryTaskForTask(taskId);

            if(deliveryTaskEntity != null){
                messageDto.setSuccess(false);
                messageDto.setErrorCode("SK-002");
                messageDto.setMessage(systemMessage.getMessage("SK-002"));
                return messageDto;
            }

            taskDao.delete(taskEntity);

        }catch(Exception e){
            e.printStackTrace();
            messageDto.setSuccess(false);
            messageDto.setMessage(e.getMessage());
        }

        return messageDto;
    }

    @Override
    @Transactional
    public MessageDto getTask(Long taskId){
        MessageDto messageDto = new MessageDto();

        try{
            TaskEntity taskEntity = taskDao.findOne(taskId);

            if(taskEntity == null){
                messageDto.setSuccess(false);
                messageDto.setErrorCode("SK-002");
                messageDto.setMessage(systemMessage.getMessage("SK-002"));
                return messageDto;
            }

            messageDto.setResponse(entityMapper.entityToBO(taskEntity));

        }catch(Exception e){
            e.printStackTrace();
            messageDto.setSuccess(false);
            messageDto.setMessage(e.getMessage());
        }

        return messageDto;
    }

    @Override
    @Transactional
    public MessageDto getAllTaskForMatterCommunirySection(Long matterCommunitySectionId){
        MessageDto messageDto = new MessageDto();

        try{
            List<TaskEntity> taskEntity = taskDao.getAllTaskForMatterCommunitySection(matterCommunitySectionId);

            if(taskEntity == null){
                messageDto.setSuccess(false);
                messageDto.setErrorCode("SK-002");
                messageDto.setMessage(systemMessage.getMessage("SK-002"));
                return messageDto;
            }

            List<TaskDto> taskDtos = new ArrayList<TaskDto>();

            for(TaskEntity taskEntity1: taskEntity){
                taskDtos.add(entityMapper.entityToBO(taskEntity1));
            }
            messageDto.setResponse(taskDtos);

        }catch(Exception e){
            e.printStackTrace();
            messageDto.setSuccess(false);
            messageDto.setMessage(e.getMessage());
        }

        return messageDto;
    }
}
