package com.posma.sckola.app.business.impl;

import com.posma.sckola.app.business.NotificationBF;
import com.posma.sckola.app.business.SectionBF;
import com.posma.sckola.app.dto.*;
import com.posma.sckola.app.persistence.dao.*;
import com.posma.sckola.app.persistence.entity.*;
import com.posma.sckola.app.util.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.NoResultException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by Francis Ries on 20/04/2017.
 */
@Service
public class SectionBFImpl implements SectionBF {


    @Autowired
    SectionDao sectionDao;

    @Autowired
    WizardDao wizardDao;

    @Autowired
    UserDao userDao;

    @Autowired
    MatterCommunitySectionDao matterCommunitySectionDao;

    @Autowired
    EntityMapper entityMapper;

    @Autowired
    SystemMessage systemMessage;

    @Autowired
    CommunityDao communityDao;

    @Autowired
    NotificationBF notificationBf;

    @Autowired
    ServiceProperties serviceProperties;

    @Autowired
    Encrypt encrypt;

    @Autowired
    ValidationAccountDao validationAccountDao;

    @Autowired
    NotificationDao notificationDao;

    @Autowired
    NotificationMessage notificationMessage;
    

    /**
     * Service query all sections from a community with status "ACTIVE"
     * @since 24/04/2017
     * @author FrancisRies
     * @version 1.0
     */
    @Override
    @Transactional
    public MessageDto sectionCommunityGetAll(Long communityId){

        MessageDto messageDto = new MessageDto();

        List<SectionEntity> SectionEntityList = sectionDao.findAllByCommunity(communityId.longValue(), StatusSection.ACTIVE);
        List<SectionDto> sectionDtoList = new ArrayList<>(SectionEntityList.size());

        for(SectionEntity sectionEntity:SectionEntityList){
            sectionDtoList.add(entityMapper.entityToBO(sectionEntity));
        }

        messageDto.setResponse(sectionDtoList);

        return messageDto;
    }


    /**
     * Service create section in a community
     * @since 24/04/2017
     * @author FrancisRies
     * @version 1.0
     */
    @Override
    @Transactional
    public MessageDto sectionCommunityCreate(Long communityId, SectionDto sectionDto, Long idUser){

        WizardEntity wizardEntity;
        MessageDto messageDto = new MessageDto();
        List<WizardEntity> wizardEntityList = wizardDao.findAllByFieldId("USER_ID", idUser);

        try{
            CommunityEntity communityEntity = communityDao.findOne(communityId);

            if(communityEntity == null || communityEntity.getId() == null){
                messageDto.setSuccess(false);
                messageDto.setErrorCode("SK-002");
                messageDto.setMessage(systemMessage.getMessage("SK-002")+ " - Community not exists");
                return messageDto;
            }

            List<SectionDto> sectionDtoList = (List<SectionDto>)sectionCommunityGetAll(communityId).getResponse();

            for(SectionDto sectionDtoAux: sectionDtoList){
                //valida que no exista una seccion con el mismo nombre con status "ACTIVE"
                if(sectionDtoAux.getName().equalsIgnoreCase(sectionDto.getName().trim().toUpperCase())){
                    messageDto.setSuccess(false);
                    messageDto.setErrorCode("SK-005");
                    messageDto.setMessage(systemMessage.getMessage("SK-005"));
                    return messageDto;
                }
            }

            SectionEntity sectionEntity = new SectionEntity(sectionDto.getName().trim().toUpperCase(),
                    communityEntity,
                    StatusSection.ACTIVE);

            sectionEntity = sectionDao.create(sectionEntity);

            if(sectionEntity != null && sectionEntity.getId() != null) {
                messageDto.setResponse(entityMapper.entityToBO(sectionEntity));
                if(wizardEntityList.size() >= 1) {
                    wizardEntity = wizardEntityList.get(0);
                    long idSection = sectionEntity.getId();
                    MatterCommunitySectionEntity matterCommunitySectionEntity = matterCommunitySectionDao.findOne(idSection);
                    wizardEntity.setMatterCommunitySection(matterCommunitySectionEntity);
                    wizardEntity = wizardDao.update(wizardEntity);
                    entityMapper.entityToBO(wizardEntity);
                }

            }else{
                messageDto.setSuccess(false);
                messageDto.setErrorCode("SK-001");
                messageDto.setMessage(systemMessage.getMessage("SK-001"));
            }

        }catch (NoResultException e){
            messageDto.setSuccess(false);
            messageDto.setErrorCode("SK-002");
            messageDto.setMessage(systemMessage.getMessage("SK-002")+ " - Matter, Community or User not exists");
        }

        return messageDto;
    }


    /**
     * Consultar el listado de estudiantes de una seccion
     * @since 25/04/2017
     * @param sectionId section identifier
     * @author FrancisRies
     * @version 1.0
     */

    @Override
    @Transactional
    public MessageDto getAllUserSection(Long sectionId){MessageDto messageDto = new MessageDto();

        List<UserDto> userDtoList = new ArrayList<UserDto>();

        try {
            SectionEntity sectionEntity = sectionDao.findOne(sectionId);

            if(sectionEntity == null || sectionEntity.getId() == null){
                messageDto.setSuccess(false);
                messageDto.setErrorCode("SK-002");
                messageDto.setMessage(systemMessage.getMessage("SK-002")+ " - Matter or Community not exists");
                return messageDto;
            }

            for (UserEntity userEntity: sectionEntity.getStudentList()) {
                userDtoList.add(entityMapper.entityToBO(userEntity));
            }
            messageDto.setResponse(userDtoList);


        }catch (Exception e){
            e.printStackTrace();
            messageDto.setSuccess(false);
            messageDto.setErrorCode("GET-COM-USER-001");
            messageDto.setMessage(systemMessage.getErrorCode().get("GET-COM-USER-001"));
        }

        return messageDto;
    }

    /**
     * Add user to a section
     * @since 25/04/2017
     * @param userId user identifier
     * @param sectionId section identifier
     * @author FrancisRies
     * @version 1.0
     */
    @Override
    @Transactional
    public MessageDto associateUserSection(Long userId, Long sectionId){
        MessageDto messageDto = new MessageDto();

        final String textKeyLogo = "<LOGO>";
        final String textKeyCode = "<CODE>";
        final String textKeyURLBase = "<URLBASE>";
        final String textPerfil = "Recuerda llenar tu perfil";

        String urlBaseFront = serviceProperties.getURLServerFront(); //"http://localhost:8000/";
        String urlLogo = serviceProperties.getURLLogo(); //"http://www.posmagroup.com/assets/images/logoPosma.png";

/*        String validationText = "<h4>Validaci&oacute;n de cuenta skola</h4><br/>" +
                "Su cuenta fue creada exitosamente <br> " +
                "Para completar el registro, nacesitamos validar su cuenta <br/> Por favor has click en el siguiente enlace:<br> " +
                "<a href=\"http://<IPHOST>:8080/skola/user/validate?code=<code>\"> http://<IPHOST>:8080/skola/user/validate?code=<code></a> <br> " +
                "Si en link no funciona, copia el enlace y pegalo en una nueva ventada del browser";

                String validationText2 = "<h4>Validaci&oacute;n de cuenta skola</h4><br/>" +
                "Su cuenta fue creada exitosamente <br> " +
                "Para completar el registro, nacesitamos validar su cuenta <br/> Por favor has click en el siguiente enlace:<br> " +
                "<a href=\""+urlLogin+"/#/user/validate?code="+textKeyCode+"\"> "+urlLogin+"/#/user/validate?code="+textKeyCode+"></a> <br> " +
                "Si en link no funciona, copia el enlace y pegalo en una nueva ventada del browser";
        */

        String validationText = "<table border=0 cellspacing=0 cellpadding=0 width=512 bgcolor=#000000\n" +
                "       style=\"background-color:#f0f0f0;margin:0 auto;max-width:512px;width:inherit\">\n" +
                "    <tbody>\n" +
                "    <tr>\n" +
                "        <td bgcolor=\"#F6F8FA\" style=\"background-color:#f6f8fa;padding:28px 0 20px 0\">\n" +
                "            <table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"100%\"\n" +
                "                   style=\"width:100%!important;min-width:100%!important\">\n" +
                "                <tbody>\n" +
                "                <tr>\n" +
                "                    <td align=\"center\" width=\"100%\">\n" +
                "                        <a href=\""+textKeyURLBase+"\"\n" +
                "                           style=\"color:#008cc9;display:inline-block;text-decoration:none\" target=\"_blank\" data-saferedirecturl=\""+urlBaseFront+"\">\n" +
                "                            <img alt=\"Skola\" border=\"0\" src=\""+textKeyLogo+"\" height=\"60\" width=\"68\"\n" +
                "                                 style=\"outline:none;color:#ffffff;text-decoration:none\">\n" +
                "                        </a>\n" +
                "                    </td>\n" +
                "                </tr>\n" +
                "                <tr>\n" +
                "                    <td align=\"center\" width=\"100%\" style=\"padding:16px 24px 0 24px\">\n" +
                "                        <h2 style=\"margin:0;color:#262626;font-weight:200;font-size:20px;padding-bottom:5px;line-height:1.2\">Validaci&oacute;n de cuenta Sckola</h2>\n" +
                "                    </td>\n" +
                "                </tr>\n" +
                "                </tbody>\n" +
                "            </table>\n" +
                "        </td>\n" +
                "    </tr>\n" +
                "    <tr>\n" +
                "        <td>\n" +
                "            <table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"100%\" style=\"margin:0 10px;max-width:492px\">\n" +
                "                <tbody>\n" +
                "                <tr>\n" +
                "                    <td align=\"center\" style=\"padding:25px 0;text-align:center\">\n" +
                "                        <p style=\"margin:0;color:#262626;font-weight:100;font-size:16px;padding-bottom:15px;line-height:1.167\">Estudiante su cuenta en Sckola fue creada<br/>\n" +
                "                            Para completar el registro, tienes 24 horas para validar su cuenta<br/>\n" +
                "                            Por favor has click en el siguiente enlace:<br/></p>\n" +
                "                        <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"display:inline-block\">\n" +
                "                            <tbody>\n" +
                "                            <tr>\n" +
                "                                <td align=\"center\" valign=\"middle\">\n" +
                "                                    <a href=\""+textKeyURLBase+"\"\n" +
                "                                       style=\"word-wrap:normal;color:#008cc9;word-break:normal;white-space:nowrap;display:block;text-decoration:none\" target=\"_blank\">\n" +
                "                                        <table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"auto\">\n" +
                "                                            <tbody>\n" +
                "                                            <tr>\n" +
                "                                                <td bgcolor=\"#008CC9\" style=\"padding:6px 16px;color:#ffffff;;font-weight:bold;font-size:16px;border-color:#008cc9;background-color:#008cc9;border-radius:2px;border-width:1px;border-style:solid\">\n" +
                "                                                    <a href=\""+textKeyURLBase+"#/user/validate?code="+textKeyCode+"\" style=\"color:#dff0d8;text-decoration:none\"> "+textKeyURLBase+"#/user/validate?code="+textKeyCode+"</a> <br>\n" +
                "                                                </td>\n" +
                "                                            </tr>\n" +
                "                                            </tbody>\n" +
                "                                        </table>\n" +
                "                                    </a>\n" +
                "                                </td>\n" +
                "                            </tr>\n" +
                "                            </tbody>\n" +
                "                        </table>\n" +
                "                    </td>\n" +
                "                </tr>\n" +
                "                </tbody>\n" +
                "            </table>\n" +
                "        </td>\n" +
                "    </tr>\n" +
                "    </tbody>\n" +
                "</table>;\n";

        try {
            SectionEntity sectionEntity = sectionDao.findOne(sectionId);

            UserEntity userEntity = userDao.findOne(userId);

            if(sectionEntity == null || sectionEntity.getId() == null){
                messageDto.setSuccess(false);
                messageDto.setErrorCode("SK-002");
                messageDto.setMessage(systemMessage.getMessage("SK-002")+ " - User or Section not exists");
                return messageDto;
            }



            sectionEntity.addStudentList(userEntity);
            sectionEntity = sectionDao.update(sectionEntity);

            if(sectionEntity != null || sectionEntity.getId() != null){

                Date date = Calendar.getInstance().getTime();
                String timeStamp = new SimpleDateFormat("yyyyMMddHHmmss").format(date);
                String validationCode = encrypt.getHash(encrypt.MD5,timeStamp + userEntity.getMail());

                ValidationAccountEntity validationAccountEntity = validationAccountDao.findByUserId(userEntity);
                if(validationAccountEntity == null){
                    validationAccountEntity = new ValidationAccountEntity();
                    validationAccountEntity.setUserId(userEntity);
                }
                validationAccountEntity.setDate(date);
                validationAccountEntity.setValidationCode(validationCode);

                //Preparar contenido del correo
                validationText = validationText.replaceAll(textKeyCode,validationCode);
                validationText = validationText.replaceAll(textKeyURLBase, urlBaseFront);
                validationText = validationText.replaceAll(textKeyLogo, urlLogo);

                // crea la entrada para la posterior validacion del correo Electronico
                if(userEntity.getStatus() != StatusUser.ACTIVE) {
                    userEntity.setStatus(StatusUser.TO_VALIDATE);
                    userDao.update(userEntity);
                    validationAccountDao.create(validationAccountEntity);

                    NotificationEntity notificationEntity = new NotificationEntity();
                    notificationEntity.setText(textPerfil);
                    notificationEntity.setStatus(NotificationStatus.SENDED);
                    notificationEntity.setActive(true);
                    notificationEntity.setTypeName("Perfil");
                    notificationEntity.setDate(date);
                    notificationEntity.setType(NotificationType.INFO);
                    notificationEntity.setUser(userEntity);
                    List<NotificationEntity> notificationEntity1 = notificationDao.findActiveNotificationByUserByInfo(userEntity.getMail());
                    boolean encontro = false;
                    for(NotificationEntity notificationEntity2: notificationEntity1){
                        if(notificationEntity2.getTypeName().equalsIgnoreCase("Perfil")){
                         encontro = true;
                        }
                    }
                    if(!encontro) {
                        notificationDao.create(notificationEntity);
                    }

                    NotificationDto notificationDto = new NotificationDto();
                    notificationDto.setText(validationText);
                    notificationDto.setTitle("Correo de validacion de estudiante");
                    notificationDto.setTo(userEntity.getMail());

                    notificationBf.sendNotification(notificationDto);
                }

                messageDto.setIdRequest(userId);
                messageDto.setResponse(entityMapper.entityToBO(sectionEntity));
                //oculta el mensaje de solicitud de estudiantes
                MessageDto notification = notificationBf.getHistoryNotificationByInfo(userEntity.getMail());
                NotificationHistoryDto historyDto = (NotificationHistoryDto) notification.getResponse();

                for (DashboardNotificationDto dashboardNotificationDto: historyDto.getNotifications()) {
                    if(dashboardNotificationDto.getFrom().equals(notificationMessage.getStudents()))
                        notificationBf.removeActiveNotification(dashboardNotificationDto.getId());

                }
                return messageDto;
            }

        }catch (DataIntegrityViolationException cve){
            cve.printStackTrace();
        }catch (Exception e){
            e.printStackTrace();
        }

        messageDto.setSuccess(false);
        messageDto.setErrorCode("SK-001");
        messageDto.setMessage(systemMessage.getErrorCode().get("SK-001"));

        return messageDto;
    }


    /**
     * remove user from a section
     * @since 25/04/2017
     * @param userId user identifier
     * @param sectionId section identifier
     * @author FrancisRies
     * @version 1.0
     */
    @Override
    @Transactional
    public MessageDto desassociateUserSection(Long userId, Long sectionId){
        MessageDto messageDto = new MessageDto();

        try {
            SectionEntity sectionEntity = sectionDao.findOne(sectionId);

            UserEntity userEntity = userDao.findOne(userId);

            if(sectionEntity == null || sectionEntity.getId() == null){
                messageDto.setSuccess(false);
                messageDto.setErrorCode("SK-002");
                messageDto.setMessage(systemMessage.getMessage("SK-002")+ " - User or Section not exists");
                return messageDto;
            }

            sectionEntity.removeStudentList(userEntity);
            sectionEntity = sectionDao.update(sectionEntity);

            if(sectionEntity != null || sectionEntity.getId() != null){
                messageDto.setIdRequest(userId);
                messageDto.setResponse(entityMapper.entityToBO(sectionEntity));
                return messageDto;
            }

        }catch (DataIntegrityViolationException cve){
            cve.printStackTrace();
        }catch (Exception e){
            e.printStackTrace();
        }

        messageDto.setSuccess(false);
        messageDto.setErrorCode("SK-001");
        messageDto.setMessage(systemMessage.getErrorCode().get("SK-001"));

        return messageDto;
    }
}

