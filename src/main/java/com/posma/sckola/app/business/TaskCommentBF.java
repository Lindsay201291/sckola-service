package com.posma.sckola.app.business;


import com.posma.sckola.app.dto.MessageDto;
import com.posma.sckola.app.dto.TaskCommentDto;

public interface TaskCommentBF {

    MessageDto createTaskComment(TaskCommentDto taskCommentDto);

    MessageDto updateTaskComment(TaskCommentDto taskCommentDto);

    MessageDto deleteTaskComment(Long taskCommentId);

    MessageDto getAllTaskCommentForTask(Long taskId);
}
