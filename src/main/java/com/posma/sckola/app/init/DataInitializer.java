package com.posma.sckola.app.init;

import com.posma.sckola.app.business.EvaluationPlanTemplateBF;
import com.posma.sckola.app.persistence.dao.*;
import com.posma.sckola.app.persistence.entity.*;
import com.posma.sckola.app.util.SystemMessage;
import com.posma.sckola.app.util.Validation;
import org.apache.commons.codec.digest.DigestUtils;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;

import javax.persistence.EntityManagerFactory;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


/**
 * Created by francis on 27/03/2017.
 */
public class DataInitializer {

    @Autowired
    private EntityManagerFactory entityManagerFactory;

    @Autowired
    SystemMessage systemMessage;

    @Autowired
    CommunityDao communityDao;

    @Autowired
    CommunityUserDao communityUserDao;

    @Autowired
    UserDao userDao;

    @Autowired
    RoleDao roleDao;

    @Autowired
    EvaluationDao evaluationDao;

    @Autowired
    RatingScaleDao ratingScaleDao;

    @Autowired
    EvaluationToolDao evaluationToolDao;

    @Autowired
    Validation validation;

    @Autowired
    EvaluationPlanTemplateBF evaluationPlanTemplateBF;

    @Autowired
    MatterDao matterDao;

    @Autowired
    MatterCommunityDao matterCommunityDao;

    @Autowired
    MatterCommunitySectionDao matterCommunitySectionDao;

    @Autowired
    EvaluationPlanDao evaluationPlanDao;

    @Autowired
    SectionDao sectionDao;

    @Autowired
    WizardDao wizardDao;

    @Autowired
    NotificationDao notificationDao;

    public void init() throws Exception {

        SessionFactory sessionFactory = entityManagerFactory.unwrap(SessionFactory.class);
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        List<RoleEntity> roles = roleDao.findAll();


        if(roles.isEmpty()) {
            // ******************* Create ConfMailEntity *******************
            //ConfMailEntity confMailEntity = new ConfMailEntity("posmagroupcompany@gmail.com","smtp", new Long(587), "posmagroupcompany@gmail.com","posmagroup2016","smtp.gmail.com");
            ConfMailEntity confMailEntity = new ConfMailEntity("test.posma@gmail.com", "smtp", new Long(587), "test.posma@gmail.com", "fwyxnmhswweixkiz", "smtp.gmail.com");
            ConfMailEntity confMailEntity2 = new ConfMailEntity("posmagroupcompany@gmail.com","smtp", new Long(587), "posmagroupcompany@gmail.com", "prueba", "smtp.gmail.com");

            session.persist(confMailEntity);
            session.persist(confMailEntity2);

            transaction.commit();
            session.flush();


            // ******************* Create RoleEntity *******************
            transaction = session.beginTransaction();

            RoleEntity roleEntity1 = new RoleEntity("TEACHER");
            RoleEntity roleEntity2 = new RoleEntity("STUDENT");
            RoleEntity roleEntity3 = new RoleEntity("MANAGEMENT");
            RoleEntity roleEntity4 = new RoleEntity("REPRESENTATIVE");

            session.persist(roleEntity1);
            session.persist(roleEntity2);
            session.persist(roleEntity3);
            session.persist(roleEntity4);

            transaction.commit();
            session.flush();

            // ******************* Load SystemMessageEntity *******************

            transaction = session.beginTransaction();
            SystemMessageEntity systemMessageEntity0 = new SystemMessageEntity("GET-000", "solicitud con parametros invalidos/incompletos", HttpStatus.BAD_REQUEST);
            SystemMessageEntity systemMessageEntity1 = new SystemMessageEntity("GET-COM-USER-001", "Error consultando información en el sistema", HttpStatus.BAD_REQUEST);
            SystemMessageEntity systemMessageEntity2 = new SystemMessageEntity("POS-COM-USER-001", "Error creando la asociacion");
            SystemMessageEntity systemMessageEntity3 = new SystemMessageEntity("POS-COM-USER-002", "Parametros de entrada no válido");
            SystemMessageEntity systemMessageEntity4 = new SystemMessageEntity("POS-COM-USER-003", "Asociacion ya existe");
            SystemMessageEntity systemMessageEntity5 = new SystemMessageEntity("POS-COM-USER-004", "No se puede desasocia porque la relación no existe");
            SystemMessageEntity systemMessageEntity6 = new SystemMessageEntity("POS-COM-USER-005", "No se pudo realizar la desasociación");

            SystemMessageEntity systemMessageEntity10 = new SystemMessageEntity("SK-000", "Transacción Exitosa");
            SystemMessageEntity systemMessageEntity9 = new SystemMessageEntity("SK-001", "Creación fallida");
            SystemMessageEntity systemMessageEntity7 = new SystemMessageEntity("SK-002", "Parametros de entrada no válido");
            SystemMessageEntity systemMessageEntity8 = new SystemMessageEntity("SK-003", "Actualización fallida");
            SystemMessageEntity systemMessageEntity11 = new SystemMessageEntity("SK-004", "Transacción Exitosa");
            SystemMessageEntity systemMessageEntity12 = new SystemMessageEntity("SK-005", "Objecto ya existe en el sistema");
            SystemMessageEntity systemMessageEntity13 = new SystemMessageEntity("SK-006", "No se puede desasocia porque la relación no existe");
            SystemMessageEntity systemMessageEntity14 = new SystemMessageEntity("SK-007", "Eliminación fallida");
            SystemMessageEntity systemMessageEntity15 = new SystemMessageEntity("SK-008", "Objeto no encontrado");

            session.persist(systemMessageEntity0);
            session.persist(systemMessageEntity1);
            session.persist(systemMessageEntity2);
            session.persist(systemMessageEntity3);
            session.persist(systemMessageEntity4);
            session.persist(systemMessageEntity5);
            session.persist(systemMessageEntity6);
            session.persist(systemMessageEntity7);
            session.persist(systemMessageEntity8);
            session.persist(systemMessageEntity9);
            session.persist(systemMessageEntity10);
            session.persist(systemMessageEntity11);
            session.persist(systemMessageEntity12);
            session.persist(systemMessageEntity13);
            session.persist(systemMessageEntity14);
            session.persist(systemMessageEntity15);

            transaction.commit();
            session.flush();


            // ******************* Create CommunityEntity *******************
            //Siempre se debe cargar y que no hay modulo de crear comunidad todavia
            transaction = session.beginTransaction();
            CommunityEntity communityEntity1 = new CommunityEntity("Juan XXIII", "Instituto Educativo cubre Basica y Secundaria con especializacion en el área de ciencias");
            CommunityEntity communityEntity2 = new CommunityEntity("Vitegui", "Instituto Educativo cubre Inicial, Basica y Secundaria");
            CommunityEntity communityEntity3 = new CommunityEntity("Ilustre Americano", "Instituto Educativo cubre Basica y Secundaria");
            CommunityEntity communityEntity4 = new CommunityEntity("Santa Maria", "Instituto Universitario");
            CommunityEntity communityEntity5 = new CommunityEntity("Boyaca", "Instituto Educativo cubre Inicial, Basica y Secundaria");
            CommunityEntity communityEntity6 = new CommunityEntity("Maria Auxiliadora", "Instituto Educativo cubre Inicial, Basica y Secundaria");
            CommunityEntity communityEntity7 = new CommunityEntity("Universidad Central", "Instituto Universitario");
            CommunityEntity communityEntity8 = new CommunityEntity("Universidad Católica Andres Bello", "Instituto Universitario");
            CommunityEntity communityEntity9 = new CommunityEntity("Universidad Simón Bolivar", "Instituto Universitario");
            CommunityEntity communityEntity10 = new CommunityEntity("Universidad Simón Rodriguez", "Instituto Universitario");
            CommunityEntity communityEntity11 = new CommunityEntity("Universidad Nacional Experimental Politécnica de la Fuerza Armada Bolivariana", "Instituto Universitario");
            CommunityEntity communityEntity12 = new CommunityEntity("Universidad de los Andes", "Instituto Universitario");
            CommunityEntity communityEntity13 = new CommunityEntity("Universidad del Zulia", "Instituto Universitario");
            CommunityEntity communityEntity14 = new CommunityEntity("Universidad Bicentenaria de Aragua", "Instituto Universitario");
            CommunityEntity communityEntity15 = new CommunityEntity("Universidad de Carabobo", "Instituto Universitario");
            CommunityEntity communityEntity16 = new CommunityEntity("Universidad de Oriente Venezuela", "Instituto Universitario");
            CommunityEntity communityEntity17 = new CommunityEntity("Universidad Metropolitana Caracas", "Instituto Universitario");
            CommunityEntity communityEntity18 = new CommunityEntity("Universidad Nacional Experimental de los Llanos Occidentales Ezequiel Zamora", "Instituto Universitario");
            CommunityEntity communityEntity19 = new CommunityEntity("Universidad Pedagógica Experimental Libertador", "Instituto Universitario");
            CommunityEntity communityEntity20 = new CommunityEntity("Instituto de Estudios Superiores de Administración", "Instituto Universitario");
            CommunityEntity communityEntity21 = new CommunityEntity("Universidad Bolívariana de Venezuela", "Instituto Universitario");
            CommunityEntity communityEntity22 = new CommunityEntity("Nuestra Señora Del Carmen", "Instituto Educativo cubre preescolar, Basica y Secundaria");
            CommunityEntity communityEntity23 = new CommunityEntity("Ambrosio Plaza", "Instituto Educativo cubre preescolar, Basica y Secundaria con a especializacion en el area de ciencias");
            CommunityEntity communityEntity24 = new CommunityEntity("Los Caciques", "Instituto Educativo cubre Basica y secundaria");
            CommunityEntity communityEntity25 = new CommunityEntity("Instituto Peter Drucker CA", "Instituto Educativo cubre Basica y secundaria");
            CommunityEntity communityEntity26 = new CommunityEntity("Instituto Universitario De Tecnologia Ruffino Blanco Fombona", "Instituto Universitario");
            CommunityEntity communityEntity27 = new CommunityEntity("Maternal Preescolar U.E Montessori", "Instituto educativo de preescolar");
            CommunityEntity communityEntity28 = new CommunityEntity("Preescolar y Guardería Infantil Lev Vygotsk", "Instituto educativo de preescolar");
            CommunityEntity communityEntity29 = new CommunityEntity("Santa Marta", "Instituto Educativo que cubre preescolar, Basica y Secundaria");
            CommunityEntity communityEntity30 = new CommunityEntity("Jose Atanasio Girardot", "Instituto Educativo cubre Basica y secundaria");
            CommunityEntity communityEntity31 = new CommunityEntity("María Teresa de Nezer", "Instituto Educativo cubre Basica y secundaria");
            CommunityEntity communityEntity32 = new CommunityEntity("Pedro Isaías Ojeda Blanco", "Instituto Educativo cubre Preescolar, Basica y secundaria");
            CommunityEntity communityEntity33 = new CommunityEntity("El Gran Aborigen", "Instituto Educativo cubre Basica");
            CommunityEntity communityEntity34 = new CommunityEntity("Santa Clara", "Instituto Educativo cubre Basica");

            session.persist(communityEntity1);
            session.persist(communityEntity2);
            session.persist(communityEntity3);
            session.persist(communityEntity4);
            session.persist(communityEntity5);
            session.persist(communityEntity6);
            session.persist(communityEntity7);
            session.persist(communityEntity8);
            session.persist(communityEntity9);
            session.persist(communityEntity10);
            session.persist(communityEntity11);
            session.persist(communityEntity12);
            session.persist(communityEntity13);
            session.persist(communityEntity14);
            session.persist(communityEntity15);
            session.persist(communityEntity16);
            session.persist(communityEntity17);
            session.persist(communityEntity18);
            session.persist(communityEntity19);
            session.persist(communityEntity20);
            session.persist(communityEntity21);
            session.persist(communityEntity22);
            session.persist(communityEntity23);
            session.persist(communityEntity24);
            session.persist(communityEntity25);
            session.persist(communityEntity26);
            session.persist(communityEntity27);
            session.persist(communityEntity28);
            session.persist(communityEntity29);
            session.persist(communityEntity30);
            session.persist(communityEntity31);
            session.persist(communityEntity32);
            session.persist(communityEntity33);
            session.persist(communityEntity34);


            transaction.commit();
            session.flush();
            // ******************* Create CommunityUserEntity *******************

            transaction = session.beginTransaction();
            CommunityUserEntity communityUserEntity = new CommunityUserEntity(communityDao.findOne(1));
            //CommunityUserEntity communityUserEntity1 = new CommunityUserEntity(communityDao.findOne(2));
            //CommunityUserEntity communityUserEntity2 = new CommunityUserEntity(communityDao.findOne(3));
            //CommunityUserEntity communityUserEntity3 = new CommunityUserEntity(communityDao.findOne(4));
//
            session.persist(communityUserEntity);
            //session.persist(communityUserEntity1);
            //session.persist(communityUserEntity2);
            //session.persist(communityUserEntity3);
//
            transaction.commit();
            session.flush();


            // ******************* Create UserEntity *******************
            transaction = session.beginTransaction();

            UserEntity userEntitys1 = new UserEntity("POSMA@GMAIL.COM", DigestUtils.shaHex("proyec7o5ckol@.2018"), StatusUser.ACTIVE);
            userEntitys1.addRoleList(roleEntity1);
            //userEntitys1.addRoleList(roleEntity2);

            UserEntity userEntitys2 = new UserEntity("POSMA@HOTMAIL.COM", DigestUtils.shaHex("p0sm@a.2018"), StatusUser.ACTIVE);
            userEntitys2.addRoleList(roleEntity1);

            UserEntity userEntitys3 = new UserEntity("POSMA.AMANDAARAUJO@GMAIL.COM", DigestUtils.shaHex("am@nd@.sckola18"), StatusUser.ACTIVE);
            userEntitys3.addRoleList(roleEntity1);

            UserEntity userEntitys4 = new UserEntity("GUIMART@GMAIL.COM", DigestUtils.shaHex("anton1o.2018"), StatusUser.ACTIVE);
            userEntitys4.addRoleList(roleEntity1);

            UserEntity userEntitys5 = new UserEntity("ERIKA.ARAU@GMAIL.COM", DigestUtils.shaHex("er1k@.sckola"), StatusUser.ACTIVE);
            userEntitys5.addRoleList(roleEntity1);

            UserEntity userEntitys6 = new UserEntity("OSCARANTONIOPARALESFUENTES@GMAIL.COM", DigestUtils.shaHex("osc@r.anton1o"), StatusUser.ACTIVE);
            userEntitys6.addRoleList(roleEntity1);

            UserEntity userEntitys7 = new UserEntity("POSMA.ROBERTHPARALES@GMAIL.COM", DigestUtils.shaHex("rober7h.sckol@2018"),StatusUser.ACTIVE);
            userEntitys7.addRoleList(roleEntity1);

            UserEntity userEntitys8 = new UserEntity("Rosana","","Ramos","",Gender.FEMALE,"RRAMOS@HOTMAIL.COM", DigestUtils.shaHex("paris1921"),StatusUser.ACTIVE);
            userEntitys8.addRoleList(roleEntity1);

            UserEntity userEntitys9 = new UserEntity("Sandra","","Torres","",Gender.FEMALE,"TORRESSANDRA@HOTMAIL.COM", DigestUtils.shaHex("enamorada"), StatusUser.ACTIVE);
            userEntitys9.addRoleList(roleEntity1);

            UserEntity userEntity0 = new UserEntity("Roberto", "", "Rodriguez", "", Gender.MALE, StatusUser.UNASSIGNED);
            userEntity0.addRoleList(roleEntity2);

            UserEntity userEntity1 = new UserEntity("José", "Maria", "Vargas", "Martinez", Gender.MALE, StatusUser.UNASSIGNED);
            userEntity1.addRoleList(roleEntity2);

            UserEntity userEntity2 = new UserEntity("Martha", "Angelica", "Villareal", "Angulo", Gender.FEMALE, StatusUser.UNASSIGNED);
            userEntity2.addRoleList(roleEntity2);

            //UserEntity userEntity0 = new UserEntity("Roberto", "", "Rodriguez", "", Gender.MALE, StatusUser.UNASSIGNED);
            //userEntity0.addRoleList(roleEntity2);
//
            //UserEntity userEntity1 = new UserEntity("Mario", "", "Perez", "", Gender.MALE, StatusUser.ACTIVE);
            //userEntity1.addRoleList(roleEntity2);
//
            //UserEntity userEntity2 = new UserEntity("José", "Maria", "Vargas", "Martinez", Gender.MALE, StatusUser.UNASSIGNED);
            //userEntity2.addRoleList(roleEntity2);
//
            //UserEntity userEntity3 = new UserEntity("Martha", "Angelica", "Villareal", "Angulo", Gender.FEMALE, StatusUser.UNASSIGNED);
            //userEntity3.addRoleList(roleEntity2);
//
            //UserEntity userEntity4 = new UserEntity("POSMA@YAHOO.COM", DigestUtils.shaHex("123"), StatusUser.ACTIVE);
            //userEntity4.addRoleList(roleEntity1);
            //userEntity4.setFirstName("POSMA");
            //userEntity4.setLastName("GROUP");
            //userEntity4.setFoto("logoPosma.png");
//
            //UserEntity userEntity5 = new UserEntity("Carlos", "", "Rodriguez", "", Gender.MALE, StatusUser.UNASSIGNED);
            //userEntity3.addRoleList(roleEntity2);
//
            //UserEntity userEntity6 = new UserEntity("Juan", "", "Rodriguez", "", Gender.MALE, StatusUser.UNASSIGNED);
            //userEntity3.addRoleList(roleEntity2);
//
            //UserEntity userEntity7 = new UserEntity("Roberto", "", "Perez", "", Gender.MALE, StatusUser.UNASSIGNED);
            //userEntity3.addRoleList(roleEntity2);

        /*UserEntity userEntity7 = new UserEntity("Roberto","Perez Martinez", Gender.MALE, StatusUser.UNASSIGNED);
        UserEntity userEntity8 = new UserEntity("Jose","Perez", Gender.MALE, StatusUser.UNASSIGNED);
        UserEntity userEntity9 = new UserEntity("Maria","Rodriguez", Gender.FEMALE, StatusUser.UNASSIGNED);
        UserEntity userEntity10 = new UserEntity("Martha","Rodriguez", Gender.FEMALE, StatusUser.UNASSIGNED);
        UserEntity userEntity11 = new UserEntity("Petra","Rodriguez", Gender.FEMALE, StatusUser.UNASSIGNED);
        UserEntity userEntity12 = new UserEntity("Monica","Rodriguez", Gender.FEMALE, StatusUser.UNASSIGNED);
        UserEntity userEntity13 = new UserEntity("Vanessa","Rodriguez", Gender.FEMALE, StatusUser.UNASSIGNED);
        UserEntity userEntity14 = new UserEntity("Maria Alejandra","Rodriguez Valdivieso", Gender.FEMALE, StatusUser.UNASSIGNED);
        UserEntity userEntity15 = new UserEntity("Antonio","Parales", Gender.MALE, StatusUser.UNASSIGNED);
        UserEntity userEntity16 = new UserEntity("Ericka","Rodriguez", Gender.FEMALE, StatusUser.UNASSIGNED);
        UserEntity userEntity17 = new UserEntity("Ricardo","Rodriguez", Gender.MALE, StatusUser.UNASSIGNED);
        UserEntity userEntity18 = new UserEntity("Oswaldo","Rodriguez", Gender.MALE, StatusUser.UNASSIGNED);
        UserEntity userEntity19 = new UserEntity("Enrique","Talavera", Gender.MALE, StatusUser.UNASSIGNED);
*/
            session.persist(userEntitys1);
            session.persist(userEntitys2);
            session.persist(userEntitys3);
            session.persist(userEntitys4);
            session.persist(userEntitys5);
            session.persist(userEntitys6);
            session.persist(userEntitys7);
            session.persist(userEntitys8);
            session.persist(userEntitys9);
            session.persist(userEntity0);
            session.persist(userEntity1);
            session.persist(userEntity2);

            //session.persist(userEntity0);
            //session.persist(userEntity1);
            //session.persist(userEntity2);
            //session.persist(userEntity3);
            //session.persist(userEntity4);
            //session.persist(userEntity5);
            //session.persist(userEntity6);
            //session.persist(userEntity7);
        /*session.persist(userEntity8);
        session.persist(userEntity9);
        session.persist(userEntity10);
        session.persist(userEntity11);
        session.persist(userEntity12);
        session.persist(userEntity13);
        session.persist(userEntity14);
        session.persist(userEntity15);
        session.persist(userEntity16);
        session.persist(userEntity17);
        session.persist(userEntity18);
        session.persist(userEntity19);
*/


            transaction.commit();
            session.flush();

            // ******************* entrada en el wizard *******************
            //para cada usuario tiene su wizard
            transaction = session.beginTransaction();
            WizardEntity wizardEntity = new WizardEntity(userDao.findOne(2));
            WizardEntity wizardEntity1 = new WizardEntity(userDao.findOne(1));
            WizardEntity wizardEntity2 = new WizardEntity(userDao.findOne(3));
            WizardEntity wizardEntity3 = new WizardEntity(userDao.findOne(4));
            WizardEntity wizardEntity4 = new WizardEntity(userDao.findOne(5));
            WizardEntity wizardEntity5 = new WizardEntity(userDao.findOne(6));
            WizardEntity wizardEntity6 = new WizardEntity(userDao.findOne(7));
            WizardEntity wizardEntity7 = new WizardEntity(userDao.findOne(8));
            WizardEntity wizardEntity8 = new WizardEntity(userDao.findOne(9));

            session.persist(wizardEntity);
            session.persist(wizardEntity1);
            session.persist(wizardEntity2);
            session.persist(wizardEntity3);
            session.persist(wizardEntity4);
            session.persist(wizardEntity5);
            session.persist(wizardEntity6);
            session.persist(wizardEntity7);
            session.persist(wizardEntity8);

            transaction.commit();
            session.flush();

            // ***************** notificaciones de perfil ********************
            //para cada nuevo usuario su notificacion de perfil
            transaction = session.beginTransaction();
            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            Date date = validation.stringToDate(dateFormat.format(new Date()));
            NotificationEntity notificationEntity = new NotificationEntity("Perfil","Por favor complete los datos de su perfil.",NotificationType.INFO, userDao.findOne(1), date);
            NotificationEntity notificationEntity1 = new NotificationEntity("Perfil","Por favor complete los datos de su perfil.",NotificationType.INFO, userDao.findOne(2), date);
            NotificationEntity notificationEntity2 = new NotificationEntity("Perfil","Por favor complete los datos de su perfil.",NotificationType.INFO, userDao.findOne(3), date);
            NotificationEntity notificationEntity3 = new NotificationEntity("Perfil","Por favor complete los datos de su perfil.",NotificationType.INFO, userDao.findOne(4), date);
            NotificationEntity notificationEntity4 = new NotificationEntity("Perfil","Por favor complete los datos de su perfil.",NotificationType.INFO, userDao.findOne(5), date);
            NotificationEntity notificationEntity5 = new NotificationEntity("Perfil","Por favor complete los datos de su perfil.",NotificationType.INFO, userDao.findOne(6), date);
            NotificationEntity notificationEntity6 = new NotificationEntity("Perfil","Por favor complete los datos de su perfil.",NotificationType.INFO, userDao.findOne(7), date);

            session.persist(notificationEntity);
            session.persist(notificationEntity1);
            session.persist(notificationEntity2);
            session.persist(notificationEntity3);
            session.persist(notificationEntity4);
            session.persist(notificationEntity5);
            session.persist(notificationEntity6);

            transaction.commit();
            session.flush();


            // ******************* precargar prueba de estudiantes a una comunidad *******************
            transaction = session.beginTransaction();
//
            CommunityEntity communityEntity_0 = communityDao.findOne(1);
            //CommunityEntity communityEntity_1 = communityDao.findOne(2);
            //CommunityEntity communityEntity_2 = communityDao.findOne(3);
            //CommunityEntity communityEntity_4 = communityDao.findOne(4);
//
//
            CommunityUserEntity auxCUE = communityUserDao.findOne(1);
            //CommunityUserEntity auxCUE1 = communityUserDao.findOne(2);
            //CommunityUserEntity auxCUE2 = communityUserDao.findOne(3);
            //CommunityUserEntity auxCUE4 = communityUserDao.findOne(4);
//
            UserRoleCommunityEntity userRoleCommunityEntity0 = new UserRoleCommunityEntity(
                    userDao.findOne(10), roleDao.findOne(2), communityEntity_0, auxCUE);
            UserRoleCommunityEntity userRoleCommunityEntity1 = new UserRoleCommunityEntity(
                    userDao.findOne(11), roleDao.findOne(2), communityEntity_0, auxCUE);
            UserRoleCommunityEntity userRoleCommunityEntity2 = new UserRoleCommunityEntity(
                    userDao.findOne(12), roleDao.findOne(2), communityEntity_0, auxCUE);
            UserRoleCommunityEntity userRoleCommunityEntity3 = new UserRoleCommunityEntity(
                    userDao.findOne(8), roleDao.findOne(1), communityEntity_0, auxCUE);
            UserRoleCommunityEntity userRoleCommunityEntity4 = new UserRoleCommunityEntity(
                    userDao.findOne(9), roleDao.findOne(1), communityEntity_0, auxCUE);
            //UserRoleCommunityEntity userRoleCommunityEntity3_2 = new UserRoleCommunityEntity(
            //        userDao.findOne(1), roleDao.findOne(1), communityEntity_2, auxCUE2);
            //UserRoleCommunityEntity userRoleCommunityEntity4 = new UserRoleCommunityEntity(
            //        userDao.findOne(7), roleDao.findOne(1), communityEntity_4, auxCUE4);
            //UserRoleCommunityEntity userRoleCommunityEntity5 = new UserRoleCommunityEntity(
            //        userDao.findOne(8), roleDao.findOne(2), communityEntity_4, auxCUE4);
            //UserRoleCommunityEntity userRoleCommunityEntity6 = new UserRoleCommunityEntity(
            //        userDao.findOne(9), roleDao.findOne(2), communityEntity_4, auxCUE4);
            //UserRoleCommunityEntity userRoleCommunityEntity7 = new UserRoleCommunityEntity(
            //        userDao.findOne(10), roleDao.findOne(2), communityEntity_4, auxCUE4);
//
            session.persist(userRoleCommunityEntity0);
            session.persist(userRoleCommunityEntity1);
            session.persist(userRoleCommunityEntity2);
            session.persist(userRoleCommunityEntity3);
            session.persist(userRoleCommunityEntity4);
            //session.persist(userRoleCommunityEntity3_2);
            //session.persist(userRoleCommunityEntity4);
            //session.persist(userRoleCommunityEntity5);
            //session.persist(userRoleCommunityEntity6);
            //session.persist(userRoleCommunityEntity7);
//
            transaction.commit();
            session.flush();

            // ******************* Create MatterEntity *******************
            //siempre se debe cargar mientras no se haya creado un modulo de crear materias nuevas
            transaction = session.beginTransaction();

            MatterEntity matterEntity = new MatterEntity("Matemática", "");
            MatterEntity matterEntity1 = new MatterEntity("Castellano", "");
            MatterEntity matterEntity2 = new MatterEntity("Biología", "");
            MatterEntity matterEntity3 = new MatterEntity("Química", "");
            MatterEntity matterEntity4 = new MatterEntity("Dibujo Técnico", "");
            MatterEntity matterEntity5 = new MatterEntity("Contabilidad", "");
            MatterEntity matterEntity6 = new MatterEntity("Educación Ciudadana", "");
            MatterEntity matterEntity7 = new MatterEntity("Arte Contemporaneo", "");
            MatterEntity matterEntity8 = new MatterEntity("Educación para el Trabajo", "");
            MatterEntity matterEntity9 = new MatterEntity("Historia Universal", "");
            MatterEntity matterEntity10 = new MatterEntity("Historia", "");

            session.persist(matterEntity);
            session.persist(matterEntity1);
            session.persist(matterEntity2);
            session.persist(matterEntity3);
            session.persist(matterEntity4);
            session.persist(matterEntity5);
            session.persist(matterEntity6);
            session.persist(matterEntity7);
            session.persist(matterEntity8);
            session.persist(matterEntity9);
            session.persist(matterEntity10);

            transaction.commit();
            session.flush();


            // ******************* Create MatterCommunityEntity *******************

            transaction = session.beginTransaction();
            MatterEntity matterEntityTemp1 = matterDao.findOne(1);
            MatterCommunityEntity matterCommunityEntity1 = new MatterCommunityEntity(matterEntityTemp1.getName(), matterEntityTemp1.getDescription(), matterEntityTemp1, communityDao.findOne(1));
//
            //CommunityEntity communityEntity = communityDao.findOne(2);
            //MatterEntity matterEntityTemp2 = matterDao.findOne(2);
            //MatterCommunityEntity matterCommunityEntity2 = new MatterCommunityEntity(matterEntityTemp2.getName(), matterEntityTemp2.getDescription(), matterEntityTemp2, communityEntity);
//
            //MatterCommunityEntity matterCommunityEntity3 = new MatterCommunityEntity(matterEntityTemp1.getName(), matterEntityTemp1.getDescription(), matterEntityTemp1, communityEntity);
            //MatterEntity matterEntityTemp4 = matterDao.findOne(3);
            //MatterCommunityEntity matterCommunityEntity4 = new MatterCommunityEntity(matterEntityTemp4.getName(), matterEntityTemp4.getDescription(), matterEntityTemp4, communityEntity);
//
            //MatterEntity matterEntityTemp5 = matterDao.findOne(4);
            //MatterCommunityEntity matterCommunityEntity5 = new MatterCommunityEntity(matterEntityTemp5.getName(), matterEntityTemp5.getDescription(), matterEntityTemp5, communityDao.findOne(4));
//
            session.persist(matterCommunityEntity1);
            //session.persist(matterCommunityEntity2);
            //session.persist(matterCommunityEntity3);
            //session.persist(matterCommunityEntity4);
            //session.persist(matterCommunityEntity5);
//
            transaction.commit();
            session.flush();

            // ******************* section default para prueba *******************

            //transaction = session.beginTransaction();
//
            //SectionEntity sectionEntity = new SectionEntity("1 A", communityEntity1, StatusSection.ACTIVE);
            //UserEntity userEntity4 = userDao.findOne(10);
            //UserEntity userEntity5 = userDao.findOne(11);
            //UserEntity userEntity6 = userDao.findOne(12);
            //sectionEntity.addStudentList(userEntity4);
            //sectionEntity.addStudentList(userEntity5);
            //sectionEntity.addStudentList(userEntity6);
            //SectionEntity sectionEntity1 = new SectionEntity("1 B", communityEntity2, StatusSection.ACTIVE);
            //SectionEntity sectionEntity2 = new SectionEntity("9 B", communityEntity1, StatusSection.INACTIVE);
            //SectionEntity sectionEntity3 = new SectionEntity("7 C", communityEntity1, StatusSection.ACTIVE);
            //SectionEntity sectionEntity4 = new SectionEntity("5 A", communityEntity2, StatusSection.ACTIVE);
            //SectionEntity sectionEntity5 = new SectionEntity("5 B", communityEntity2, StatusSection.ACTIVE);
            //SectionEntity sectionEntity6 = new SectionEntity("6 B", communityEntity4, StatusSection.ACTIVE);
//
            //session.persist(sectionEntity);
            //session.persist(sectionEntity1);
            //session.persist(sectionEntity2);
            //session.persist(sectionEntity3);
            //session.persist(sectionEntity4);
            //session.persist(sectionEntity5);
            //session.persist(sectionEntity6);
            //transaction.commit();
            //session.flush();


            // ******************* Crear class MatterCommunityEntity *******************
            //transaction = session.beginTransaction();
//
            //UserEntity userEntity = userDao.findOne(8);
            //UserEntity userEntityTemp2 = userDao.findOne(7);
            //MatterCommunitySectionEntity mcsEntity1 = new MatterCommunitySectionEntity(matterCommunityDao.findOne(1), sectionDao.findOne(1), userEntity, StatusClass.ACTIVE, SectionType.INTEGRAL);
            //MatterCommunitySectionEntity mcsEntity2 = new MatterCommunitySectionEntity(matterCommunityDao.findOne(2), sectionDao.findOne(2), userEntity, StatusClass.ACTIVE, SectionType.INTEGRAL);
            //MatterCommunitySectionEntity mcsEntity3 = new MatterCommunitySectionEntity(matterCommunityDao.findOne(3), sectionDao.findOne(5), userEntity, StatusClass.ACTIVE, SectionType.INTEGRAL);
            //MatterCommunitySectionEntity mcsEntity4 = new MatterCommunitySectionEntity(matterCommunityDao.findOne(4), sectionDao.findOne(6), userEntity, StatusClass.ACTIVE, SectionType.INTEGRAL);
            //MatterCommunitySectionEntity mcsEntity5 = new MatterCommunitySectionEntity(matterCommunityDao.findOne(5), sectionDao.findOne(7), userEntityTemp2, StatusClass.ACTIVE, SectionType.INTEGRAL);
//
            //session.merge(mcsEntity1);
            //session.merge(mcsEntity2);
            //session.merge(mcsEntity3);
            //session.merge(mcsEntity4);
            //session.merge(mcsEntity5);
//
            //transaction.commit();
            //session.flush();

            // ******************* Create OrganizationEntity *******************
            //transaction = session.beginTransaction();
//
            //OrganizationEntity organizationEntity1 = new OrganizationEntity("Av. principal Diego, Edif. la gloria. Buenos Aires, Argentina", "Juan XXIII", "posmaarg@hotmail.com", "J-4325344-6");
            //OrganizationEntity organizationEntity2 = new OrganizationEntity("Colinas de Carrizal, Los Teques Miranda. Venezuela", "Ilustre Americano", "posmala@hotmail.com", "J-5450032-1");
            //OrganizationEntity organizationEntity3 = new OrganizationEntity("Calle arriba, Los Teques Miranda. Venezuela", "Santa Maria", "posma@hotmail.com", "J-5450042-1");
//
            //session.persist(organizationEntity1);
            //session.persist(organizationEntity2);
            //session.persist(organizationEntity3);
//
            //transaction.commit();
            //session.flush();


            // ******************* Create EvaluationToolEntity *******************
            //siempre se debe estar habilitido hasta que se puedan crear evaluaciones
            transaction = session.beginTransaction();

            EvaluationToolEntity evaluationToolEntity1 = new EvaluationToolEntity("Examen", "Es una evaluacion escrita individual");
            EvaluationToolEntity evaluationToolEntity2 = new EvaluationToolEntity("Examen Oral", "Es una evaluacion Oral individual");
            EvaluationToolEntity evaluationToolEntity3 = new EvaluationToolEntity("Exposición", "Es una evaluacion de presentacion de un tema");
            EvaluationToolEntity evaluationToolEntity4 = new EvaluationToolEntity("Trabajo Escrito", "Es una evaluacion de un trabajo escrito");
            EvaluationToolEntity evaluationToolEntity5 = new EvaluationToolEntity("Talleres", "Es una evaluacion grupal");
            EvaluationToolEntity evaluationToolEntity6 = new EvaluationToolEntity("Tareas", "Es una evaluacion que se manda para la casa");
            EvaluationToolEntity evaluationToolEntity7 = new EvaluationToolEntity("Observacion", "Es una evaluacion sobre el comportamiento del alumno en clase");
            EvaluationToolEntity evaluationToolEntity8 = new EvaluationToolEntity("Formativas", "Es una evaluacion para comprobar los conocimientos son diarias para primaria");
            EvaluationToolEntity evaluationToolEntity9 = new EvaluationToolEntity("Sumativas", "Es una evaluacion es la que utiliza el docente para hacer los boletines en primaria");
            EvaluationToolEntity evaluationToolEntity10 = new EvaluationToolEntity("Debates", "Es una evaluacion en la que paticipan los alumnos en un tema especifico");
            EvaluationToolEntity evaluationToolEntity11 = new EvaluationToolEntity("Mapa Conceptual", "Es una evaluacion que manda el profesor sobre un tema especifico");
            EvaluationToolEntity evaluationToolEntity12 = new EvaluationToolEntity("Informe Cualitativo", "Es una evaluacion que hace el profesor");
            EvaluationToolEntity evaluationToolEntity13 = new EvaluationToolEntity("Prueba Practica", "Es una evaluacion que manda el profesor para poner en practica los conocimientos del estudiantes");
            EvaluationToolEntity evaluationToolEntity14 = new EvaluationToolEntity("Entrevista", "Es una evaluacion que consiste en una conversacion entre el evaluado y el evaluador para determinar su progreso");
            EvaluationToolEntity evaluationToolEntity15 = new EvaluationToolEntity("Ensayo", "Es una evaluacion en la que el estudiante tiene la libertad para responder, ya que este decidira como enfocar el problema del tema");


            session.persist(evaluationToolEntity1);
            session.persist(evaluationToolEntity2);
            session.persist(evaluationToolEntity3);
            session.persist(evaluationToolEntity4);
            session.persist(evaluationToolEntity5);
            session.persist(evaluationToolEntity6);
            session.persist(evaluationToolEntity7);
            session.persist(evaluationToolEntity8);
            session.persist(evaluationToolEntity9);
            session.persist(evaluationToolEntity10);
            session.persist(evaluationToolEntity11);
            session.persist(evaluationToolEntity12);
            session.persist(evaluationToolEntity13);
            session.persist(evaluationToolEntity14);
            session.persist(evaluationToolEntity15);

            transaction.commit();
            session.flush();


            // ******************* Create RatingScaleEntity *******************
            //para crear las escalas de valores de las evaluaciones
            transaction = session.beginTransaction();

            RatingScaleEntity ratingScaleEntity1 = new RatingScaleEntity("Literales", "Son las escalas de calificacion de la A hasta la F");
            RatingScaleEntity ratingScaleEntity2 = new RatingScaleEntity("Del 0 al 20", "Son las escalas de calificacion del 0 al 20");
            RatingScaleEntity ratingScaleEntity3 = new RatingScaleEntity("Del 0 al 5", "Son las escalas de calificacion del 0 al 5");
            RatingScaleEntity ratingScaleEntity4 = new RatingScaleEntity("Del 0 al 100", "Son las escalas de calificacion del 0 al 100");

            // ******************* Asignarle RatingValue a RatingScale de Literales ******************

            ratingScaleEntity1.addRatingValueList(new RatingValueEntity(new Qualification("A", "20 puntos"), ratingScaleEntity1));
            ratingScaleEntity1.addRatingValueList(new RatingValueEntity(new Qualification("B", "15 puntos"), ratingScaleEntity1));
            ratingScaleEntity1.addRatingValueList(new RatingValueEntity(new Qualification("C", "10 puntos"), ratingScaleEntity1));
            ratingScaleEntity1.addRatingValueList(new RatingValueEntity(new Qualification("D", "5 puntos"), ratingScaleEntity1));
            ratingScaleEntity1.addRatingValueList(new RatingValueEntity(new Qualification("E", "3 puntos"), ratingScaleEntity1));
            ratingScaleEntity1.addRatingValueList(new RatingValueEntity(new Qualification("F", "0 puntos"), ratingScaleEntity1));

            // ******************* Asignarle RatingValue a RatingScale de Del 0 al 20 *******************

            ratingScaleEntity2.addRatingValueList(new RatingValueEntity(new Qualification("20", "20 puntos"), ratingScaleEntity2));
            ratingScaleEntity2.addRatingValueList(new RatingValueEntity(new Qualification("19", "19 puntos"), ratingScaleEntity2));
            ratingScaleEntity2.addRatingValueList(new RatingValueEntity(new Qualification("18", "18 puntos"), ratingScaleEntity2));
            ratingScaleEntity2.addRatingValueList(new RatingValueEntity(new Qualification("17", "17 puntos"), ratingScaleEntity2));
            ratingScaleEntity2.addRatingValueList(new RatingValueEntity(new Qualification("16", "16 puntos"), ratingScaleEntity2));
            ratingScaleEntity2.addRatingValueList(new RatingValueEntity(new Qualification("15", "15 puntos"), ratingScaleEntity2));
            ratingScaleEntity2.addRatingValueList(new RatingValueEntity(new Qualification("14", "14 puntos"), ratingScaleEntity2));
            ratingScaleEntity2.addRatingValueList(new RatingValueEntity(new Qualification("13", "13 puntos"), ratingScaleEntity2));
            ratingScaleEntity2.addRatingValueList(new RatingValueEntity(new Qualification("12", "12 puntos"), ratingScaleEntity2));
            ratingScaleEntity2.addRatingValueList(new RatingValueEntity(new Qualification("11", "11 puntos"), ratingScaleEntity2));
            ratingScaleEntity2.addRatingValueList(new RatingValueEntity(new Qualification("10", "10 puntos"), ratingScaleEntity2));
            ratingScaleEntity2.addRatingValueList(new RatingValueEntity(new Qualification("9", "9 puntos"), ratingScaleEntity2));
            ratingScaleEntity2.addRatingValueList(new RatingValueEntity(new Qualification("8", "8 puntos"), ratingScaleEntity2));
            ratingScaleEntity2.addRatingValueList(new RatingValueEntity(new Qualification("7", "7 puntos"), ratingScaleEntity2));
            ratingScaleEntity2.addRatingValueList(new RatingValueEntity(new Qualification("6", "6 puntos"), ratingScaleEntity2));
            ratingScaleEntity2.addRatingValueList(new RatingValueEntity(new Qualification("5", "5 puntos"), ratingScaleEntity2));
            ratingScaleEntity2.addRatingValueList(new RatingValueEntity(new Qualification("4", "4 puntos"), ratingScaleEntity2));
            ratingScaleEntity2.addRatingValueList(new RatingValueEntity(new Qualification("3", "3 puntos"), ratingScaleEntity2));
            ratingScaleEntity2.addRatingValueList(new RatingValueEntity(new Qualification("2", "2 puntos"), ratingScaleEntity2));
            ratingScaleEntity2.addRatingValueList(new RatingValueEntity(new Qualification("1", "1 puntos"), ratingScaleEntity2));
            ratingScaleEntity2.addRatingValueList(new RatingValueEntity(new Qualification("0", "0 puntos"), ratingScaleEntity2));

            // ******************* Asignarle RatingValue a RatingScale de Del 0 a 5 *******************

            ratingScaleEntity3.addRatingValueList(new RatingValueEntity(new Qualification("5", "5 puntos"), ratingScaleEntity3));
            ratingScaleEntity3.addRatingValueList(new RatingValueEntity(new Qualification("4", "4 puntos"), ratingScaleEntity3));
            ratingScaleEntity3.addRatingValueList(new RatingValueEntity(new Qualification("3", "3 puntos"), ratingScaleEntity3));
            ratingScaleEntity3.addRatingValueList(new RatingValueEntity(new Qualification("2", "2 puntos"), ratingScaleEntity3));
            ratingScaleEntity3.addRatingValueList(new RatingValueEntity(new Qualification("1", "1 puntos"), ratingScaleEntity3));
            ratingScaleEntity3.addRatingValueList(new RatingValueEntity(new Qualification("0", "0 puntos"), ratingScaleEntity3));

            // ******************* Asignarle RatingValue a RatingScale de del 0 al 100 *******************

            ratingScaleEntity4.addRatingValueList(new RatingValueEntity(new Qualification("100", "100 puntos"), ratingScaleEntity4));
            ratingScaleEntity4.addRatingValueList(new RatingValueEntity(new Qualification("99", "99 puntos"), ratingScaleEntity4));
            ratingScaleEntity4.addRatingValueList(new RatingValueEntity(new Qualification("98", "98 puntos"), ratingScaleEntity4));
            ratingScaleEntity4.addRatingValueList(new RatingValueEntity(new Qualification("97", "97 puntos"), ratingScaleEntity4));
            ratingScaleEntity4.addRatingValueList(new RatingValueEntity(new Qualification("96", "96 puntos"), ratingScaleEntity4));
            ratingScaleEntity4.addRatingValueList(new RatingValueEntity(new Qualification("95", "95 puntos"), ratingScaleEntity4));
            ratingScaleEntity4.addRatingValueList(new RatingValueEntity(new Qualification("94", "94 puntos"), ratingScaleEntity4));
            ratingScaleEntity4.addRatingValueList(new RatingValueEntity(new Qualification("93", "93 puntos"), ratingScaleEntity4));
            ratingScaleEntity4.addRatingValueList(new RatingValueEntity(new Qualification("92", "92 puntos"), ratingScaleEntity4));
            ratingScaleEntity4.addRatingValueList(new RatingValueEntity(new Qualification("91", "91 puntos"), ratingScaleEntity4));
            ratingScaleEntity4.addRatingValueList(new RatingValueEntity(new Qualification("90", "90 puntos"), ratingScaleEntity4));
            ratingScaleEntity4.addRatingValueList(new RatingValueEntity(new Qualification("89", "89 puntos"), ratingScaleEntity4));
            ratingScaleEntity4.addRatingValueList(new RatingValueEntity(new Qualification("88", "88 puntos"), ratingScaleEntity4));
            ratingScaleEntity4.addRatingValueList(new RatingValueEntity(new Qualification("87", "87 puntos"), ratingScaleEntity4));
            ratingScaleEntity4.addRatingValueList(new RatingValueEntity(new Qualification("86", "86 puntos"), ratingScaleEntity4));
            ratingScaleEntity4.addRatingValueList(new RatingValueEntity(new Qualification("85", "85 puntos"), ratingScaleEntity4));
            ratingScaleEntity4.addRatingValueList(new RatingValueEntity(new Qualification("84", "84 puntos"), ratingScaleEntity4));
            ratingScaleEntity4.addRatingValueList(new RatingValueEntity(new Qualification("83", "83 puntos"), ratingScaleEntity4));
            ratingScaleEntity4.addRatingValueList(new RatingValueEntity(new Qualification("82", "82 puntos"), ratingScaleEntity4));
            ratingScaleEntity4.addRatingValueList(new RatingValueEntity(new Qualification("81", "81 puntos"), ratingScaleEntity4));
            ratingScaleEntity4.addRatingValueList(new RatingValueEntity(new Qualification("80", "80 puntos"), ratingScaleEntity4));
            ratingScaleEntity4.addRatingValueList(new RatingValueEntity(new Qualification("79", "79 puntos"), ratingScaleEntity4));
            ratingScaleEntity4.addRatingValueList(new RatingValueEntity(new Qualification("78", "78 puntos"), ratingScaleEntity4));
            ratingScaleEntity4.addRatingValueList(new RatingValueEntity(new Qualification("77", "77 puntos"), ratingScaleEntity4));
            ratingScaleEntity4.addRatingValueList(new RatingValueEntity(new Qualification("76", "76 puntos"), ratingScaleEntity4));
            ratingScaleEntity4.addRatingValueList(new RatingValueEntity(new Qualification("75", "75 puntos"), ratingScaleEntity4));
            ratingScaleEntity4.addRatingValueList(new RatingValueEntity(new Qualification("74", "74 puntos"), ratingScaleEntity4));
            ratingScaleEntity4.addRatingValueList(new RatingValueEntity(new Qualification("73", "73 puntos"), ratingScaleEntity4));
            ratingScaleEntity4.addRatingValueList(new RatingValueEntity(new Qualification("72", "72 puntos"), ratingScaleEntity4));
            ratingScaleEntity4.addRatingValueList(new RatingValueEntity(new Qualification("71", "71 puntos"), ratingScaleEntity4));
            ratingScaleEntity4.addRatingValueList(new RatingValueEntity(new Qualification("70", "70 puntos"), ratingScaleEntity4));
            ratingScaleEntity4.addRatingValueList(new RatingValueEntity(new Qualification("69", "69 puntos"), ratingScaleEntity4));
            ratingScaleEntity4.addRatingValueList(new RatingValueEntity(new Qualification("68", "68 puntos"), ratingScaleEntity4));
            ratingScaleEntity4.addRatingValueList(new RatingValueEntity(new Qualification("67", "67 puntos"), ratingScaleEntity4));
            ratingScaleEntity4.addRatingValueList(new RatingValueEntity(new Qualification("66", "66 puntos"), ratingScaleEntity4));
            ratingScaleEntity4.addRatingValueList(new RatingValueEntity(new Qualification("65", "65 puntos"), ratingScaleEntity4));
            ratingScaleEntity4.addRatingValueList(new RatingValueEntity(new Qualification("64", "64 puntos"), ratingScaleEntity4));
            ratingScaleEntity4.addRatingValueList(new RatingValueEntity(new Qualification("63", "63 puntos"), ratingScaleEntity4));
            ratingScaleEntity4.addRatingValueList(new RatingValueEntity(new Qualification("62", "62 puntos"), ratingScaleEntity4));
            ratingScaleEntity4.addRatingValueList(new RatingValueEntity(new Qualification("61", "61 puntos"), ratingScaleEntity4));
            ratingScaleEntity4.addRatingValueList(new RatingValueEntity(new Qualification("60", "60 puntos"), ratingScaleEntity4));
            ratingScaleEntity4.addRatingValueList(new RatingValueEntity(new Qualification("59", "59 puntos"), ratingScaleEntity4));
            ratingScaleEntity4.addRatingValueList(new RatingValueEntity(new Qualification("58", "58 puntos"), ratingScaleEntity4));
            ratingScaleEntity4.addRatingValueList(new RatingValueEntity(new Qualification("57", "57 puntos"), ratingScaleEntity4));
            ratingScaleEntity4.addRatingValueList(new RatingValueEntity(new Qualification("56", "56 puntos"), ratingScaleEntity4));
            ratingScaleEntity4.addRatingValueList(new RatingValueEntity(new Qualification("55", "55 puntos"), ratingScaleEntity4));
            ratingScaleEntity4.addRatingValueList(new RatingValueEntity(new Qualification("54", "54 puntos"), ratingScaleEntity4));
            ratingScaleEntity4.addRatingValueList(new RatingValueEntity(new Qualification("53", "53 puntos"), ratingScaleEntity4));
            ratingScaleEntity4.addRatingValueList(new RatingValueEntity(new Qualification("52", "52 puntos"), ratingScaleEntity4));
            ratingScaleEntity4.addRatingValueList(new RatingValueEntity(new Qualification("51", "51 puntos"), ratingScaleEntity4));
            ratingScaleEntity4.addRatingValueList(new RatingValueEntity(new Qualification("50", "50 puntos"), ratingScaleEntity4));
            ratingScaleEntity4.addRatingValueList(new RatingValueEntity(new Qualification("49", "49 puntos"), ratingScaleEntity4));
            ratingScaleEntity4.addRatingValueList(new RatingValueEntity(new Qualification("48", "48 puntos"), ratingScaleEntity4));
            ratingScaleEntity4.addRatingValueList(new RatingValueEntity(new Qualification("47", "47 puntos"), ratingScaleEntity4));
            ratingScaleEntity4.addRatingValueList(new RatingValueEntity(new Qualification("46", "46 puntos"), ratingScaleEntity4));
            ratingScaleEntity4.addRatingValueList(new RatingValueEntity(new Qualification("45", "45 puntos"), ratingScaleEntity4));
            ratingScaleEntity4.addRatingValueList(new RatingValueEntity(new Qualification("44", "44 puntos"), ratingScaleEntity4));
            ratingScaleEntity4.addRatingValueList(new RatingValueEntity(new Qualification("43", "43 puntos"), ratingScaleEntity4));
            ratingScaleEntity4.addRatingValueList(new RatingValueEntity(new Qualification("42", "42 puntos"), ratingScaleEntity4));
            ratingScaleEntity4.addRatingValueList(new RatingValueEntity(new Qualification("41", "41 puntos"), ratingScaleEntity4));
            ratingScaleEntity4.addRatingValueList(new RatingValueEntity(new Qualification("40", "40 puntos"), ratingScaleEntity4));
            ratingScaleEntity4.addRatingValueList(new RatingValueEntity(new Qualification("39", "39 puntos"), ratingScaleEntity4));
            ratingScaleEntity4.addRatingValueList(new RatingValueEntity(new Qualification("38", "38 puntos"), ratingScaleEntity4));
            ratingScaleEntity4.addRatingValueList(new RatingValueEntity(new Qualification("37", "37 puntos"), ratingScaleEntity4));
            ratingScaleEntity4.addRatingValueList(new RatingValueEntity(new Qualification("36", "36 puntos"), ratingScaleEntity4));
            ratingScaleEntity4.addRatingValueList(new RatingValueEntity(new Qualification("35", "35 puntos"), ratingScaleEntity4));
            ratingScaleEntity4.addRatingValueList(new RatingValueEntity(new Qualification("34", "34 puntos"), ratingScaleEntity4));
            ratingScaleEntity4.addRatingValueList(new RatingValueEntity(new Qualification("33", "33 puntos"), ratingScaleEntity4));
            ratingScaleEntity4.addRatingValueList(new RatingValueEntity(new Qualification("32", "32 puntos"), ratingScaleEntity4));
            ratingScaleEntity4.addRatingValueList(new RatingValueEntity(new Qualification("31", "31 puntos"), ratingScaleEntity4));
            ratingScaleEntity4.addRatingValueList(new RatingValueEntity(new Qualification("30", "30 puntos"), ratingScaleEntity4));
            ratingScaleEntity4.addRatingValueList(new RatingValueEntity(new Qualification("29", "29 puntos"), ratingScaleEntity4));
            ratingScaleEntity4.addRatingValueList(new RatingValueEntity(new Qualification("28", "28 puntos"), ratingScaleEntity4));
            ratingScaleEntity4.addRatingValueList(new RatingValueEntity(new Qualification("27", "27 puntos"), ratingScaleEntity4));
            ratingScaleEntity4.addRatingValueList(new RatingValueEntity(new Qualification("26", "26 puntos"), ratingScaleEntity4));
            ratingScaleEntity4.addRatingValueList(new RatingValueEntity(new Qualification("25", "25 puntos"), ratingScaleEntity4));
            ratingScaleEntity4.addRatingValueList(new RatingValueEntity(new Qualification("24", "24 puntos"), ratingScaleEntity4));
            ratingScaleEntity4.addRatingValueList(new RatingValueEntity(new Qualification("23", "23 puntos"), ratingScaleEntity4));
            ratingScaleEntity4.addRatingValueList(new RatingValueEntity(new Qualification("22", "22 puntos"), ratingScaleEntity4));
            ratingScaleEntity4.addRatingValueList(new RatingValueEntity(new Qualification("21", "21 puntos"), ratingScaleEntity4));
            ratingScaleEntity4.addRatingValueList(new RatingValueEntity(new Qualification("20", "20 puntos"), ratingScaleEntity4));
            ratingScaleEntity4.addRatingValueList(new RatingValueEntity(new Qualification("19", "19 puntos"), ratingScaleEntity4));
            ratingScaleEntity4.addRatingValueList(new RatingValueEntity(new Qualification("18", "18 puntos"), ratingScaleEntity4));
            ratingScaleEntity4.addRatingValueList(new RatingValueEntity(new Qualification("17", "17 puntos"), ratingScaleEntity4));
            ratingScaleEntity4.addRatingValueList(new RatingValueEntity(new Qualification("16", "16 puntos"), ratingScaleEntity4));
            ratingScaleEntity4.addRatingValueList(new RatingValueEntity(new Qualification("15", "15 puntos"), ratingScaleEntity4));
            ratingScaleEntity4.addRatingValueList(new RatingValueEntity(new Qualification("14", "14 puntos"), ratingScaleEntity4));
            ratingScaleEntity4.addRatingValueList(new RatingValueEntity(new Qualification("13", "13 puntos"), ratingScaleEntity4));
            ratingScaleEntity4.addRatingValueList(new RatingValueEntity(new Qualification("12", "12 puntos"), ratingScaleEntity4));
            ratingScaleEntity4.addRatingValueList(new RatingValueEntity(new Qualification("11", "11 puntos"), ratingScaleEntity4));
            ratingScaleEntity4.addRatingValueList(new RatingValueEntity(new Qualification("10", "10 puntos"), ratingScaleEntity4));
            ratingScaleEntity4.addRatingValueList(new RatingValueEntity(new Qualification("9", "9 puntos"), ratingScaleEntity4));
            ratingScaleEntity4.addRatingValueList(new RatingValueEntity(new Qualification("8", "8 puntos"), ratingScaleEntity4));
            ratingScaleEntity4.addRatingValueList(new RatingValueEntity(new Qualification("7", "7 puntos"), ratingScaleEntity4));
            ratingScaleEntity4.addRatingValueList(new RatingValueEntity(new Qualification("6", "6 puntos"), ratingScaleEntity4));
            ratingScaleEntity4.addRatingValueList(new RatingValueEntity(new Qualification("5", "5 puntos"), ratingScaleEntity4));
            ratingScaleEntity4.addRatingValueList(new RatingValueEntity(new Qualification("4", "4 puntos"), ratingScaleEntity4));
            ratingScaleEntity4.addRatingValueList(new RatingValueEntity(new Qualification("3", "3 puntos"), ratingScaleEntity4));
            ratingScaleEntity4.addRatingValueList(new RatingValueEntity(new Qualification("2", "2 puntos"), ratingScaleEntity4));
            ratingScaleEntity4.addRatingValueList(new RatingValueEntity(new Qualification("1", "1 puntos"), ratingScaleEntity4));
            ratingScaleEntity4.addRatingValueList(new RatingValueEntity(new Qualification("0", "0 puntos"), ratingScaleEntity4));

            session.persist(ratingScaleEntity1);
            session.persist(ratingScaleEntity2);
            session.persist(ratingScaleEntity3);
            session.persist(ratingScaleEntity4);

            transaction.commit();
            session.flush();


            // ********************* Carga 3 planes de evaluacion del usuario 1 *********************
            //transaction = session.beginTransaction();
//
            //EvaluationPlanEntity evaluationPlanEntity = new EvaluationPlanEntity("Plan de Evaluacion Matematica 5C", userDao.findOne(8));
            //EvaluationPlanEntity evaluationPlanEntity1 = new EvaluationPlanEntity("Plan de Evaluacion Matematica 5A", userDao.findOne(1));
            //EvaluationPlanEntity evaluationPlanEntity2 = new EvaluationPlanEntity("Plan de Evaluacion Castellano 5C", userDao.findOne(1));
//
            //evaluationPlanEntity.addEvaluationList(new EvaluationEntity("Examen", 25, ratingScaleDao.findOne(2), evaluationToolDao.findOne(1), evaluationPlanEntity, validation.stringToDate("28-04-2019")));
            //evaluationPlanEntity.addEvaluationList(new EvaluationEntity("Examen Oral", 25, ratingScaleDao.findOne(2), evaluationToolDao.findOne(2), evaluationPlanEntity, validation.stringToDate("2-05-2019")));
            //evaluationPlanEntity.addEvaluationList(new EvaluationEntity("Exposición", 50, ratingScaleDao.findOne(2), evaluationToolDao.findOne(3), evaluationPlanEntity, validation.stringToDate("23-06-2019")));
//
            //evaluationPlanEntity1.addEvaluationList(new EvaluationEntity("", 10, ratingScaleDao.findOne(4), evaluationToolDao.findOne(4), evaluationPlanEntity1, validation.stringToDate("28-04-2017")));
            //evaluationPlanEntity1.addEvaluationList(new EvaluationEntity("", 20, ratingScaleDao.findOne(1), evaluationToolDao.findOne(5), evaluationPlanEntity1, validation.stringToDate("30-04-2017")));
            //evaluationPlanEntity1.addEvaluationList(new EvaluationEntity("", 15, ratingScaleDao.findOne(2), evaluationToolDao.findOne(6), evaluationPlanEntity1, validation.stringToDate("2-05-2017")));
            //evaluationPlanEntity1.addEvaluationList(new EvaluationEntity("", 25, ratingScaleDao.findOne(2), evaluationToolDao.findOne(2), evaluationPlanEntity1, validation.stringToDate("1-06-2017")));
            //evaluationPlanEntity1.addEvaluationList(new EvaluationEntity("", 5, ratingScaleDao.findOne(3), evaluationToolDao.findOne(7), evaluationPlanEntity1, validation.stringToDate("26-05-2017")));
//
            //evaluationPlanEntity2.addEvaluationList(new EvaluationEntity("", 25, ratingScaleDao.findOne(1), evaluationToolDao.findOne(1), evaluationPlanEntity2, validation.stringToDate("21-04-2017")));
            //evaluationPlanEntity2.addEvaluationList(new EvaluationEntity("", 25, ratingScaleDao.findOne(2), evaluationToolDao.findOne(2), evaluationPlanEntity2, validation.stringToDate("21-06-2017")));
            //evaluationPlanEntity2.addEvaluationList(new EvaluationEntity("", 10, ratingScaleDao.findOne(4), evaluationToolDao.findOne(4), evaluationPlanEntity2, validation.stringToDate("28-06-2017")));
            //evaluationPlanEntity2.addEvaluationList(new EvaluationEntity("", 15, ratingScaleDao.findOne(2), evaluationToolDao.findOne(6), evaluationPlanEntity2, validation.stringToDate("28-06-2017")));
            //evaluationPlanEntity2.addEvaluationList(new EvaluationEntity("", 50, ratingScaleDao.findOne(3), evaluationToolDao.findOne(3), evaluationPlanEntity2, validation.stringToDate("28-06-2017")));
//
            //session.persist(evaluationPlanEntity);
            //session.persist(evaluationPlanEntity1);
            //session.persist(evaluationPlanEntity2);
//
            //transaction.commit();
            //session.flush();

            // ********************* Carga 1 plan de evaluacion del usuario 7 *********************
            //transaction = session.beginTransaction();
//
            //EvaluationPlanEntity evaluationPlanEntity3 = new EvaluationPlanEntity("Plan de Evaluacion Quimica 6B", userDao.findOne(7));
//
            //evaluationPlanEntity.addEvaluationList(new EvaluationEntity("", 25, ratingScaleDao.findOne(2), evaluationToolDao.findOne(1), evaluationPlanEntity3, validation.stringToDate("28-09-2017")));
            //evaluationPlanEntity.addEvaluationList(new EvaluationEntity("", 25, ratingScaleDao.findOne(2), evaluationToolDao.findOne(2), evaluationPlanEntity3, validation.stringToDate("8-10-2017")));
            //evaluationPlanEntity.addEvaluationList(new EvaluationEntity("", 25, ratingScaleDao.findOne(2), evaluationToolDao.findOne(3), evaluationPlanEntity3, validation.stringToDate("15-10-2017")));
            //evaluationPlanEntity.addEvaluationList(new EvaluationEntity("", 25, ratingScaleDao.findOne(2), evaluationToolDao.findOne(4), evaluationPlanEntity3, validation.stringToDate("22-10-2017")));
//
            //session.persist(evaluationPlanEntity3);
//
            //transaction.commit();
            //session.flush();

            // ********************* Asociar un plan de evaluacion a la clase de usuario 7 *********************
            //transaction = session.beginTransaction();
////
            //MatterCommunitySectionEntity matterCommunitySectionEntityTemp = matterCommunitySectionDao.findOne(1);
            //EvaluationPlanEntity evaluationPlanEntity1 = evaluationPlanDao.findOne(1);
////
            //matterCommunitySectionEntityTemp.setEvaluationPlan(evaluationPlanEntity1);
////
            //session.merge(matterCommunitySectionEntityTemp);
////
            //transaction.commit();
            //session.flush();

            // ********************* Asociar estudiantes a la seccion 6 B *********************
            //transaction = session.beginTransaction();
//
            //SectionEntity sectionEntityTemp = sectionDao.findOne(7);
//
            //UserEntity userEntityTemp3 = userDao.findOne(8);
            //UserEntity userEntityTemp4 = userDao.findOne(9);
            //UserEntity userEntityTemp5 = userDao.findOne(10);
//
            //Set<UserEntity> userEntitySet = new HashSet<UserEntity>();
            //userEntitySet.add(userEntityTemp3);
            //userEntitySet.add(userEntityTemp4);
            //userEntitySet.add(userEntityTemp5);
//
            //sectionEntityTemp.setStudentList(userEntitySet);
//
            //session.merge(sectionEntityTemp);
//
            //transaction.commit();
            //session.flush();

            // precargar dos plantillas:
            //evaluationPlanTemplateBF.newEvaluationPlanTemplate(1L, "PLAN EVAL. Matematicas 4C 2016");
            //evaluationPlanTemplateBF.newEvaluationPlanTemplate(2L, "Plan Eval. Matematicas 5A 2015");

            // ********************* Carga la direccion donde se guardan las fotos *********************
            transaction = session.beginTransaction();

            PhotoDirectionEntity photoDirectionEntity = new PhotoDirectionEntity("/www/sckola.com/app/static/images/");

            session.persist(photoDirectionEntity);

            transaction.commit();
            session.flush();

            // ******************* Añadir al status de avance del profesor de prueba en el wizard *******************
            //transaction = session.beginTransaction();
//
            //WizardEntity wizardEntityTest = wizardDao.findOne(8);
            //WizardEntity wizardEntityTest1 = wizardDao.findOne(7);
//
            //wizardEntityTest.setCommunity(communityDao.findOne(1));
            //wizardEntityTest.setMatterCommunitySection(matterCommunitySectionDao.findOne(1));
            //wizardEntityTest.setEvaluationPlan(evaluationPlanDao.findOne(1));
//
            //wizardEntityTest1.setCommunity(communityEntity_4);
            //wizardEntityTest1.setMatterCommunitySection(matterCommunitySectionDao.findOne(5));
            //wizardEntityTest1.setUserProfile(7);
            //wizardEntityTest1.setEvaluationPlan(evaluationPlanDao.findOne(4));
//
            //session.merge(wizardEntityTest);
            //session.merge(wizardEntityTest1);
//
            //transaction.commit();
            //session.flush();

        }
        // Carga en memoria el listado de mensajes del sistema guardado en BD
        systemMessage.reload();
    }
}
