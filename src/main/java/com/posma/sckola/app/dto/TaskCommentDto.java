package com.posma.sckola.app.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class TaskCommentDto {

    private Long id;

    private TaskDto task;

    private UserDto user;

    private String comment;

    private Date date;

    public TaskCommentDto(){}
}
