package com.posma.sckola.app.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by Francis on 27/03/2017.
 */
@Getter
@Setter
public class MessageDto {

    /*
     *  Identificator the original object
     */
    private Long idRequest;

    /*
     *  Identificator to Success transaction
     */
    private Boolean success = true;

    /*
     *  Error Code from Message to NorthBound
     */
    private String errorCode = "";

    /*
     *  Message to NorthBound
     */
    private String message = "";


    /*
     *  Object response to NorthBound example: projectDto, RoleDto, InvitationDto, etc...
     */
    private Object response;

    public MessageDto(){

    }
}
