package com.posma.sckola.app.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * Created by Posma-dev on 30/08/2018.
 */
@Getter
@Setter
public class NetworksDto implements Serializable {

    private Long id;
    private String password;
    private String network;

    public NetworksDto(){ }
}
