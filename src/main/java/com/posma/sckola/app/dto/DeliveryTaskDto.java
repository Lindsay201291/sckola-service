package com.posma.sckola.app.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class DeliveryTaskDto implements Serializable {

    public Long id;

    public TaskDto task;

    private UserDto user;

    private String adjunt;

    private String completionDate;

    private String value;
}
