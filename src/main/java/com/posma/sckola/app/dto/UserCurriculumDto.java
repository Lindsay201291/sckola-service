package com.posma.sckola.app.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Francis Ries on 19/04/2017.
 *
 * Aun no en uso
 */
@Getter
@Setter
public class UserCurriculumDto implements Serializable {

    private Long id;

    private String title;

    private CommunityDto community;

    private Date date;

    public UserCurriculumDto() {}

    public UserCurriculumDto(String title, CommunityDto community, Date date) {
        this.title = title;
        this.community = community;
        this.date = date;
    }

    @Override
    public String toString(){
        return "{ \n" +
                " 'id':" + id + ",\n" +
                " 'title':'" + title + "',\n" +
                " 'community':" + community.toString() + ",\n" +
                " 'date':'" + date + "'\n" +
                "}";
    }

}
