package com.posma.sckola.app.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Francis Ries on 27/03/2017.
 */
@Getter
@Setter
public class UserDto implements Serializable {

    private Long id;
    private String identification;
    private String firstName;
    private String secondName;
    private String lastName;
    private String secondLastName;
    private String mail;
    private String status;
    private String password;
    // format dd-MM-YYYY
    private String birthdate;
    //MASCULINO o FEMENINO
    private String gender;
    private String foto;
    private String mailRepresentative;
    //private List<NetworksDto> networks = new ArrayList<NetworksDto>();
    private List<RoleDto> roleList = new ArrayList<RoleDto>();
    private List<CurriculumDto> curriculum = new ArrayList<CurriculumDto>();

    public UserDto() { }

    public boolean addCurriculum(CurriculumDto experience) {
        return curriculum.add(experience);
    }

    @Override
    public String toString(){
        return "{ \n" +
                " 'id':" + id + ",\n" +
                " 'identification':'" + identification + "',\n" +
                " 'firstName':'" + firstName + "',\n" +
                " 'secondName':'" + secondName + "',\n" +
                " 'lastName':'" + lastName + "',\n" +
                " 'secondLastName':'" + secondLastName + "',\n" +
                " 'mail':'" + mail + "',\n" +
                " 'status':'" + status + "',\n" +
                " 'password':'**************'\n" +
                " 'gender':'" + gender + "',\n" +
                " 'birthdate':'" + birthdate + "',\n" +
               // " 'curriculum':" + (curriculum.size()>0?curriculum.toString():"{}") + " \n" +
                "}";
    }


}
