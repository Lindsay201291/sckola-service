package com.posma.sckola.app.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * Created by Francis Ries on 20/04/2017.
 */
@Getter
@Setter
public class MatterDto implements Serializable {

    private Long id;
    private String name;
    private String description;
    private String objective;
    private String especificObjective;


    public MatterDto() {}

    public MatterDto(String name, String description) {
        this.name = name;
        this.description = description;
    }
}
