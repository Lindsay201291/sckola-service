package com.posma.sckola.app.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * Created by Francis Ries on 18/04/2017.
 */
@Getter
@Setter
public class RequestAssociateDto implements Serializable {

    private Boolean associate;

    public RequestAssociateDto(){}

    public RequestAssociateDto(boolean associate){
        this.associate = associate;
    }
}
