package com.posma.sckola.app.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
public class InvitationDto implements Serializable {

    private Long id;

    private String name;

    private String mail;

    private String phone;

    private String ocupation;

    private List<ParticipationDto> participation;

    private String institution;

    private String other;

    public InvitationDto(){}
}
