package com.posma.sckola.app.dto;

import com.posma.sckola.app.persistence.entity.NotificationType;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * Created by Posma-dev on 17/08/2017.
 */
@Getter
@Setter
public class DashboardNotificationDto {

    private long id;
    private String from;
    private String text;
    private String image;
    private String username;
    private CommunityDto community;
    private MatterDto matter;
    private SectionDto section;
    private EvaluationDto evaluation;
    private boolean active;
    private NotificationType type;
    private String date;

    public DashboardNotificationDto() {
    }

    public DashboardNotificationDto(long id, String from, String text, String image, String username, CommunityDto community, MatterDto matter, NotificationType type) {
        this.id = id;
        this.from = from;
        this.text = text;
        this.image = image;
        this.username = username;
        this.community = community;
        this.matter = matter;
        this.type = type;
    }
}
