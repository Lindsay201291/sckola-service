package com.posma.sckola.app.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class TaskDto implements Serializable {

    private Long id;

    private String name;

    private String completionDate;

    private String creationDate;

    private String adjunt;

    private Integer weight;

    private Long evaluationId;

    private String description;

    private List<MatterCommunitySectionDto> matterCommunitySectionList = new ArrayList<MatterCommunitySectionDto>();


    public boolean addMatterCommunitySection(MatterCommunitySectionDto matterCommunitySectionDto){return matterCommunitySectionList.add(matterCommunitySectionDto);}
}
