package com.posma.sckola.app.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * Created by Ricardo on 19/05/2017.
 */
@Getter
@Setter
public class ImageDto implements Serializable {

    private String direccion;

    private String nombre;

    public ImageDto(){}
}
