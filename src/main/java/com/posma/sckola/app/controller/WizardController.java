package com.posma.sckola.app.controller;

import com.posma.sckola.app.business.WizardBF;
import com.posma.sckola.app.dto.MessageDto;
import com.posma.sckola.app.dto.WizardDto;
import com.posma.sckola.app.util.SystemMessage;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;


/**
 * Created by Francis on 29/05/2017.
 */
@RestController
@RequestMapping("/skola/wizard")
public class WizardController {

    static final Logger logger = Logger.getLogger(WizardController.class);

    static final String MessageUserApproveFail = "No fue posible procesar la solicitud";
    static final String MessageUserApproveSuccess= "Solicitud procesada";

    @Autowired
    WizardBF wizardBF;
    
    @Autowired
    SystemMessage systemMessage;

    /**
     * Servicio que permite actualizar el paso por donde va el usuario
     * @return wizardDto
     * @throws IOException
     */
    @RequestMapping(value = "/user/{userId}",method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<?> get(@PathVariable Long userId) throws IOException {

        if(userId == null ){
            MessageDto messageDto = new MessageDto();
            messageDto.setSuccess(false);
            messageDto.setErrorCode("SK-002");
            messageDto.setMessage(systemMessage.getMessage("SK-002"));
            return new ResponseEntity<>(messageDto, HttpStatus.BAD_REQUEST);
        }

        MessageDto messageDto = wizardBF.get(userId);
        if(messageDto.getSuccess())
            return ResponseEntity.ok(messageDto);
        return new ResponseEntity<>(messageDto, HttpStatus.CONFLICT);
    }


    /**
     * Servicio que permite crear una entrada al wizard
     * @return wizardDto
     * @throws IOException
     */
    /*
    @RequestMapping(method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<?> save(@RequestBody WizardDto wizardDto) throws IOException {

        if(wizardDto == null || wizardDto.getUserId() == null || wizardDto.getStepName() == null ){
            MessageDto messageDto = new MessageDto();
            messageDto.setSuccess(false);
            messageDto.setErrorCode("SK-002");
            messageDto.setMessage(systemMessage.getMessage("SK-002"));
            return new ResponseEntity<>(messageDto, HttpStatus.BAD_REQUEST);
        }

        MessageDto messageDto = wizardBF.save(wizardDto);
        if(messageDto.getSuccess())
            return ResponseEntity.ok(messageDto);

        return new ResponseEntity<>(messageDto, HttpStatus.CONFLICT);
    }
    */


    /**
     * Servicio que permite actualizar el paso por donde va el usuario
     * @return wizardDto
     * @throws IOException
     */
    @RequestMapping(method = RequestMethod.PUT)
    @ResponseBody
    public ResponseEntity<?> userUpdate(@RequestBody WizardDto wizardDto) throws IOException {

        if(wizardDto == null || wizardDto.getUserId() == null  ){
            MessageDto messageDto = new MessageDto();
            messageDto.setSuccess(false);
            messageDto.setErrorCode("SK-002");
            messageDto.setMessage(systemMessage.getMessage("SK-002"));
            return new ResponseEntity<>(messageDto, HttpStatus.BAD_REQUEST);
        }

        MessageDto messageDto = wizardBF.update(wizardDto);
        if(messageDto.getSuccess())
            return ResponseEntity.ok(messageDto);
        return new ResponseEntity<>(messageDto, HttpStatus.CONFLICT);
    }


    /**
     * Servicio que permite actualizar el paso por donde va el usuario
     * @return wizardDto
     * @throws IOException
     */
    @RequestMapping(value="/finish",method = RequestMethod.PUT)
    @ResponseBody
    public ResponseEntity<?> finish(@RequestBody WizardDto wizardDto) throws IOException {

        if(wizardDto == null || wizardDto.getUserId() == null ){
            MessageDto messageDto = new MessageDto();
            messageDto.setSuccess(false);
            messageDto.setErrorCode("SK-002");
            messageDto.setMessage(systemMessage.getMessage("SK-002"));
            return new ResponseEntity<>(messageDto, HttpStatus.BAD_REQUEST);
        }

        MessageDto messageDto = wizardBF.finish(wizardDto);
        if(messageDto.getSuccess())
            return ResponseEntity.ok(messageDto);
        return new ResponseEntity<>(messageDto, HttpStatus.CONFLICT);
    }










        /**
         * Funcion de manejo de excepciones pertenecientes a la clase Exception
         * @param exc excepcion interceptada
         * @return una entidad respuesta, con el HttpStatus y el mensaje del error
         */
    @ExceptionHandler(Exception.class)
    public ResponseEntity<String> errorHandler(Exception exc) {
        logger.error(exc.getMessage(), exc);
        exc.printStackTrace();
        return new ResponseEntity<>(exc.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
    }


}
