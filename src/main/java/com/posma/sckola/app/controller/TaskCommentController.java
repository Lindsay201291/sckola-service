package com.posma.sckola.app.controller;

import com.posma.sckola.app.business.TaskCommentBF;
import com.posma.sckola.app.dto.MessageDto;
import com.posma.sckola.app.dto.TaskCommentDto;
import com.posma.sckola.app.dto.TaskDto;
import com.posma.sckola.app.util.SystemMessage;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@RestController
@RequestMapping("/skola/task/comment")
public class TaskCommentController {

    static final Logger logger = Logger.getLogger(TaskCommentController.class);


    @Autowired
    SystemMessage systemMessage;

    @Autowired
    TaskCommentBF taskCommentBF;


    @RequestMapping(method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<?> taskCommentCreate(@RequestBody TaskCommentDto taskCommentDto) throws IOException {

        MessageDto messageDto;
        if(taskCommentDto == null || taskCommentDto.getTask() == null|| taskCommentDto.getComment() == null || taskCommentDto.getUser() == null || taskCommentDto.getDate() == null){
            messageDto = new MessageDto();
            messageDto.setSuccess(false);
            messageDto.setErrorCode("SK-002");
            messageDto.setMessage(systemMessage.getMessage("SK-002"));
            return new ResponseEntity<>(messageDto, HttpStatus.BAD_REQUEST);
        }

        messageDto = taskCommentBF.createTaskComment(taskCommentDto);
        if(messageDto.getSuccess()){
            return ResponseEntity.ok(messageDto);
        }

        return new ResponseEntity<>(messageDto, HttpStatus.CONFLICT);
    }

    @RequestMapping(method = RequestMethod.PUT)
    @ResponseBody
    public ResponseEntity<?> taskCommentUpdate(@RequestBody TaskCommentDto taskCommentDto) throws IOException {

        MessageDto messageDto;
        if(taskCommentDto == null || taskCommentDto.getTask() == null|| taskCommentDto.getComment() == null || taskCommentDto.getUser() == null || taskCommentDto.getDate() == null){
            messageDto = new MessageDto();
            messageDto.setSuccess(false);
            messageDto.setErrorCode("SK-002");
            messageDto.setMessage(systemMessage.getMessage("SK-002"));
            return new ResponseEntity<>(messageDto, HttpStatus.BAD_REQUEST);
        }

        messageDto = taskCommentBF.updateTaskComment(taskCommentDto);
        if(messageDto.getSuccess()){
            return ResponseEntity.ok(messageDto);
        }

        return new ResponseEntity<>(messageDto, HttpStatus.CONFLICT);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public ResponseEntity<?> taskCommentDelete(@PathVariable("id") Long taskCommentId) throws IOException {

        MessageDto messageDto;
        if(taskCommentId == null){
            messageDto = new MessageDto();
            messageDto.setSuccess(false);
            messageDto.setErrorCode("SK-002");
            messageDto.setMessage(systemMessage.getMessage("SK-002"));
            return new ResponseEntity<>(messageDto, HttpStatus.BAD_REQUEST);
        }

        messageDto = taskCommentBF.deleteTaskComment(taskCommentId);
        if(messageDto.getSuccess()){
            return ResponseEntity.ok(messageDto);
        }

        return new ResponseEntity<>(messageDto, HttpStatus.CONFLICT);
    }

    @RequestMapping(value = "/task/{id}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<?> taskCommentGetForTask(@PathVariable("id") Long taskId) throws IOException {

        MessageDto messageDto;
        if(taskId == null){
            messageDto = new MessageDto();
            messageDto.setSuccess(false);
            messageDto.setErrorCode("SK-002");
            messageDto.setMessage(systemMessage.getMessage("SK-002"));
            return new ResponseEntity<>(messageDto, HttpStatus.BAD_REQUEST);
        }

        messageDto = taskCommentBF.getAllTaskCommentForTask(taskId);
        if(messageDto.getSuccess()){
            return ResponseEntity.ok(messageDto);
        }

        return new ResponseEntity<>(messageDto, HttpStatus.CONFLICT);
    }



    /**
     * Funcion de manejo de excepciones pertenecientes a la clase Exception
     * @param exc excepcion interceptada
     * @return una entidad respuesta, con el HttpStatus y el mensaje del error
     */
    @ExceptionHandler(Exception.class)
    public ResponseEntity<String> errorHandler(Exception exc) {
        logger.error(exc.getMessage(), exc);
        exc.printStackTrace();
        return new ResponseEntity<>(exc.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
    }


}
