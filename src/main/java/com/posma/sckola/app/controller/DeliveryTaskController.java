package com.posma.sckola.app.controller;

import com.posma.sckola.app.business.DeliveryTaskBF;
import com.posma.sckola.app.dto.DeliveryTaskDto;
import com.posma.sckola.app.dto.MessageDto;
import com.posma.sckola.app.util.SystemMessage;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@RestController
@RequestMapping("/skola/delivery_task")
public class DeliveryTaskController {

    static final Logger logger = Logger.getLogger(DeliveryTaskController.class);

    @Autowired
    SystemMessage systemMessage;

    @Autowired
    DeliveryTaskBF deliveryTaskBF;

    @RequestMapping(method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<?> deliveryTaskCreate(@RequestBody DeliveryTaskDto deliveryTaskDto) throws IOException {

        MessageDto messageDto;
        if(deliveryTaskDto == null || deliveryTaskDto.getUser() == null|| deliveryTaskDto.getAdjunt() == null || deliveryTaskDto.getCompletionDate() == null || deliveryTaskDto.getTask() == null){
            messageDto = new MessageDto();
            messageDto.setSuccess(false);
            messageDto.setErrorCode("SK-002");
            messageDto.setMessage(systemMessage.getMessage("SK-002"));
            return new ResponseEntity<>(messageDto, HttpStatus.BAD_REQUEST);
        }

        messageDto = deliveryTaskBF.createDeliveryTask(deliveryTaskDto);
        if(messageDto.getSuccess()){
            return ResponseEntity.ok(messageDto);
        }

        return new ResponseEntity<>(messageDto, HttpStatus.CONFLICT);
    }

    @RequestMapping(method = RequestMethod.PUT)
    @ResponseBody
    public ResponseEntity<?> deliveryTaskUpdate(@RequestBody DeliveryTaskDto deliveryTaskDto) throws IOException {

        MessageDto messageDto;
        if(deliveryTaskDto == null || deliveryTaskDto.getUser() == null|| deliveryTaskDto.getAdjunt() == null || deliveryTaskDto.getCompletionDate() == null || deliveryTaskDto.getTask() == null){
            messageDto = new MessageDto();
            messageDto.setSuccess(false);
            messageDto.setErrorCode("SK-002");
            messageDto.setMessage(systemMessage.getMessage("SK-002"));
            return new ResponseEntity<>(messageDto, HttpStatus.BAD_REQUEST);
        }

        messageDto = deliveryTaskBF.updateDeliveryTask(deliveryTaskDto);
        if(messageDto.getSuccess()){
            return ResponseEntity.ok(messageDto);
        }

        return new ResponseEntity<>(messageDto, HttpStatus.CONFLICT);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public ResponseEntity<?> deliveryTaskDelete(@PathVariable("id") Long deliveryTaskId) throws IOException {

        MessageDto messageDto;
        if(deliveryTaskId == null){
            messageDto = new MessageDto();
            messageDto.setSuccess(false);
            messageDto.setErrorCode("SK-002");
            messageDto.setMessage(systemMessage.getMessage("SK-002"));
            return new ResponseEntity<>(messageDto, HttpStatus.BAD_REQUEST);
        }

        messageDto = deliveryTaskBF.deleteDeliveryTask(deliveryTaskId);
        if(messageDto.getSuccess()){
            return ResponseEntity.ok(messageDto);
        }

        return new ResponseEntity<>(messageDto, HttpStatus.CONFLICT);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<?> deliveryTaskGet(@PathVariable("id") Long deliveryTaskId) throws IOException {

        MessageDto messageDto;
        if(deliveryTaskId == null){
            messageDto = new MessageDto();
            messageDto.setSuccess(false);
            messageDto.setErrorCode("SK-002");
            messageDto.setMessage(systemMessage.getMessage("SK-002"));
            return new ResponseEntity<>(messageDto, HttpStatus.BAD_REQUEST);
        }

        messageDto = deliveryTaskBF.getDeliveryTask(deliveryTaskId);
        if(messageDto.getSuccess()){
            return ResponseEntity.ok(messageDto);
        }

        return new ResponseEntity<>(messageDto, HttpStatus.CONFLICT);
    }

    @RequestMapping(value = "/task/{id}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<?> deliveryTaskGetAllForTask(@PathVariable("id") Long taskId) throws IOException {

        MessageDto messageDto;
        if(taskId == null){
            messageDto = new MessageDto();
            messageDto.setSuccess(false);
            messageDto.setErrorCode("SK-002");
            messageDto.setMessage(systemMessage.getMessage("SK-002"));
            return new ResponseEntity<>(messageDto, HttpStatus.BAD_REQUEST);
        }

        messageDto = deliveryTaskBF.getAllDeliveryTaskForTask(taskId);
        if(messageDto.getSuccess()){
            return ResponseEntity.ok(messageDto);
        }

        return new ResponseEntity<>(messageDto, HttpStatus.CONFLICT);
    }

    @RequestMapping(value = "/task/{idTask}/user/{idUser}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<?> deliveryTaskGetAllForTaskUser(@PathVariable("idTask") Long taskId, @PathVariable("idUser") Long userId) throws IOException {

        MessageDto messageDto;
        if(taskId == null && userId == null){
            messageDto = new MessageDto();
            messageDto.setSuccess(false);
            messageDto.setErrorCode("SK-002");
            messageDto.setMessage(systemMessage.getMessage("SK-002"));
            return new ResponseEntity<>(messageDto, HttpStatus.BAD_REQUEST);
        }

        messageDto = deliveryTaskBF.getAllDeliveryTaskForTaskUser(taskId, userId);
        if(messageDto.getSuccess()){
            return ResponseEntity.ok(messageDto);
        }

        return new ResponseEntity<>(messageDto, HttpStatus.CONFLICT);
    }
}
