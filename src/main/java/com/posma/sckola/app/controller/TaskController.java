package com.posma.sckola.app.controller;

import com.posma.sckola.app.business.TaskBF;
import com.posma.sckola.app.dto.MessageDto;
import com.posma.sckola.app.dto.TaskDto;
import com.posma.sckola.app.util.SystemMessage;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@RestController
@RequestMapping("/skola/task")
public class TaskController {

    static final Logger logger = Logger.getLogger(TaskController.class);

    @Autowired
    SystemMessage systemMessage;

    @Autowired
    TaskBF taskBF;

    @RequestMapping(method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<?> taskCreate(@RequestBody TaskDto taskDto) throws IOException {

        MessageDto messageDto;
        if(taskDto == null || taskDto.getName() == null|| taskDto.getCompletionDate() == null || taskDto.getCreationDate() == null || taskDto.getMatterCommunitySectionList() == null){
            messageDto = new MessageDto();
            messageDto.setSuccess(false);
            messageDto.setErrorCode("SK-002");
            messageDto.setMessage(systemMessage.getMessage("SK-002"));
            return new ResponseEntity<>(messageDto, HttpStatus.BAD_REQUEST);
        }

        messageDto = taskBF.createTask(taskDto);
        if(messageDto.getSuccess()){
            return ResponseEntity.ok(messageDto);
        }

        return new ResponseEntity<>(messageDto, HttpStatus.CONFLICT);
    }

    @RequestMapping(method = RequestMethod.PUT)
    @ResponseBody
    public ResponseEntity<?> taskUpdate(@RequestBody TaskDto taskDto) throws IOException {

        MessageDto messageDto;
        if(taskDto == null || taskDto.getName() == null|| taskDto.getCompletionDate() == null || taskDto.getCreationDate() == null || taskDto.getMatterCommunitySectionList() == null){
            messageDto = new MessageDto();
            messageDto.setSuccess(false);
            messageDto.setErrorCode("SK-002");
            messageDto.setMessage(systemMessage.getMessage("SK-002"));
            return new ResponseEntity<>(messageDto, HttpStatus.BAD_REQUEST);
        }

        messageDto = taskBF.updateTask(taskDto);
        if(messageDto.getSuccess()){
            return ResponseEntity.ok(messageDto);
        }

        return new ResponseEntity<>(messageDto, HttpStatus.CONFLICT);
    }

    @RequestMapping(value = "/{id}",method = RequestMethod.DELETE)
    @ResponseBody
    public ResponseEntity<?> taskDelete(@PathVariable("id") Long taskId) throws IOException {

        MessageDto messageDto;
        if(taskId == null){
            messageDto = new MessageDto();
            messageDto.setSuccess(false);
            messageDto.setErrorCode("SK-002");
            messageDto.setMessage(systemMessage.getMessage("SK-002"));
            return new ResponseEntity<>(messageDto, HttpStatus.BAD_REQUEST);
        }

        messageDto = taskBF.deleteTask(taskId);
        if(messageDto.getSuccess()){
            return ResponseEntity.ok(messageDto);
        }

        return new ResponseEntity<>(messageDto, HttpStatus.CONFLICT);
    }

    @RequestMapping(value = "/{id}",method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<?> taskGet(@PathVariable("id") Long taskId) throws IOException {

        MessageDto messageDto;
        if(taskId == null){
            messageDto = new MessageDto();
            messageDto.setSuccess(false);
            messageDto.setErrorCode("SK-002");
            messageDto.setMessage(systemMessage.getMessage("SK-002"));
            return new ResponseEntity<>(messageDto, HttpStatus.BAD_REQUEST);
        }

        messageDto = taskBF.getTask(taskId);

        if(messageDto.getSuccess()){
            return ResponseEntity.ok(messageDto);
        }

        return new ResponseEntity<>(messageDto, HttpStatus.CONFLICT);
    }

    @RequestMapping(value = "/matterCommunitySection/{id}",method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<?> taskGetAllMatterCommunitySection(@PathVariable("id") Long matterCommunitySectionId) throws IOException {

        MessageDto messageDto;
        if(matterCommunitySectionId == null){
            messageDto = new MessageDto();
            messageDto.setSuccess(false);
            messageDto.setErrorCode("SK-002");
            messageDto.setMessage(systemMessage.getMessage("SK-002"));
            return new ResponseEntity<>(messageDto, HttpStatus.BAD_REQUEST);
        }

        messageDto = taskBF.getAllTaskForMatterCommunirySection(matterCommunitySectionId);

        if(messageDto.getSuccess()){
            return ResponseEntity.ok(messageDto);
        }

        return new ResponseEntity<>(messageDto, HttpStatus.CONFLICT);
    }
}
